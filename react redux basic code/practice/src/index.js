// npm dependencies
import React                     from 'react';
import ReactDOM                  from 'react-dom';
import { Provider }              from 'react-redux';

// project configuration
import * as serviceWorker        from './serviceWorker';

// component dependencies
import App                       from './App';

//store imports
import configureStore            from './Store/Utilities/configureStore';

//style imports
import './Assets/CSS/Base styles/basestyles.css';

const storeInstance = configureStore();

ReactDOM.render(
  <Provider store={storeInstance}>
    <App />
  </Provider>, document.getElementById('root'));

serviceWorker.unregister();