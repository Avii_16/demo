//npm dependencies
import { createStore, applyMiddleware, compose }       from 'redux'

//reducer imports
import rootReducer                                     from '../Reducers/rootReducer';

//service imports
//middleware imports goes here.....

/**
 * Configure the store with reducers and middlewares.
 */
export default function configureStore() {
  return createStore(
    rootReducer,
    compose(applyMiddleware(
    
    ))
  );
};
