import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';

@Injectable()
export class UpdateProfileService {
  url = environment.api_endpoint + '/vpas/api/';
  token: any = JSON.parse(localStorage.getItem('sessionData')).token;

  // default constructor
  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      // 'Authorization': this.token,
      'Authorization': 'Bearer ' + this.token,
      'Content-Type': 'application/json'
    })
  };

  // method to get profile data
  getProfileData() {
    return this.http.get<any>(this.url + 'loggedinadmin/0', this.httpoptions);
  }

  // method to get all time zones from local assets
  getAllTimeZones() {
    return this.http.get('../../../assets/constants/timeZoneList.json');
  }

  // method to update Administrator profile
  updateAdminProfile(data: any) {
    const body = {
      'bank': 'i18n/en_US',
      'roaming': 'false',
      'locale': 'en_US',
      'loggedinlevel': '2',
      'adminEmailAddress': data.adminEmailAddress,
      'adminPhoneNumber': data.adminPhoneNumber,
      'action': 'submit',
      'jobTitle': data.jobTitle,
      'supervisorName': data.supervisorName,
      'adminHintQuestion': data.adminHintQuestion,
      'adminHintAnswer1': data.adminHintAnswer1,
      'adminHintAnswer2': data.adminHintAnswer1,
      'recordsPerPage': data.recordsPerPage,
      'adminLocale': 'en_US',
      'localTimeZone': data.localTimeZone,
      'timeStampFormat': data.timeStampFormat,
      'startdate': data.startdate,
      'startDateOffset': data.startDateOffset,
      'privId': '',
      'level': '2'
    };
    return this.http.put<any>(this.url + 'globaladmin/profile', body, this.httpoptions);
  }
}
