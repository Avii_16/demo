import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class UploadSigningCertificateService {

  sessionData: any = JSON.parse(localStorage.getItem('sessionData'));
  token: any = this.sessionData.token;

  url: string = environment.api_endpoint + '/vpas/api/';
  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
      'Content-Type': 'application/json'
    })
  };
  getIssuers() {
    const body = {
      'task': 'display',
      'bank': 'i18n/en_US',
      'locale': 'en_US'
    };
    return this.http.post<any>(this.url + 'issuerranges', body, this.httpoptions);
  }

  // method to get range for the selected card user
  getRangeForSelectedIssuer(bankId: any) {

    const body = {
      'formContentType': 'multipart/form-data; boundary=----WebKitFormBoundaryjODMwaVTQ6N2oIdJ',
      'task': 'changeIssuer',
      'bank': 'i18n/en_US',
      'locale': 'en_US',
      'bankId': bankId,
      'authTokenPayLoad':
      {
        'authTokenFromReq': '-1739577157',
        'authTokensMap': {
          '1034373801': '338447693', '1228207067': '-1306981715', '-1589534758': '-1739577157', '-182548943': '-1256941635',
          '1199355329': '1473877433'
        },
        'formId': '-1589534758'
      }
    };
    // tslint:disable-next-line: max-line-length
    return this.http.post<any>(this.url + 'issuer/signingcertificate/upload', body, this.httpoptions);
  }

  uploadSigningCertificate(data, bankid, certificateFileInBytes) {
    if (data.rangeIds == '-1000') {
      data.rangeIds = [];
    } else {
      data.rangeIds = data.rangeIds;
    }
    const body = {
      'formContentType': 'multipart/form-data; boundary=----WebKitFormBoundaryjODMwaVTQ6N2oIdJ',
      'task': 'addOrUpdate',
      'bank': 'i18n/en_US',
      'locale': 'en_US',
     'authTokenPayLoad':
      {
        'authTokenFromReq': '-1739577157',
        'authTokensMap': {
          '1034373801': '338447693', '1228207067': '-1306981715', '-1589534758': '-1739577157', '-182548943': '-1256941635',
          '1199355329': '1473877433'
        },
        'formId': '-1589534758'
      },
      'bankId': bankid,
      'rangeIds': data.rangeIds,
      'certificateFileInBytes': certificateFileInBytes
    };
    // tslint:disable-next-line: max-line-length
    return this.http.post<any>(this.url + 'issuer/signingcertificate/upload', body, this.httpoptions);
  }

}
