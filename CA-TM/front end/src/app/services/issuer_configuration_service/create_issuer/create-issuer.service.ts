import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class CreateIssuerService {

  url = environment.api_endpoint + '/vpas/api/issuer/';
  getUrl: string = environment.api_endpoint + '/vpas/api/';
  sessionData: any = JSON.parse(localStorage.getItem('sessionData'));

  token: any = this.sessionData.token;
  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
      'Content-Type': 'application/json'
    })
  };


// common call to get all countries list
getAllCountries() {
  return this.http.get<any>(this.getUrl + 'countries/1', this.httpoptions);
}

// get list of all users
getAllUsers() {
  return this.http.get<any>(this.getUrl + 'admins/2', this.httpoptions);
}

  // method to create Issuer Administrator
  createIssuer(createIssuerData: any) {
    if (createIssuerData.enrollmentLocales == '') {
      createIssuerData.enrollmentLocales = [];
    } else {
      createIssuerData.enrollmentLocales = createIssuerData.enrollmentLocales;
    }

    if (createIssuerData.userIdEnabled === true) {
      createIssuerData.userIdEnabled = '1';
    } else {
      createIssuerData.userIdEnabled = '0';
    }

    if (createIssuerData.twoStepLoginEnabled === true) {
      createIssuerData.twoStepLoginEnabled = '1';
    } else {
      createIssuerData.twoStepLoginEnabled = '0';
    }

    if (createIssuerData.dualAuthRequiredAtCommit === true) {
      createIssuerData.dualAuthRequiredAtCommit = '1';
    } else {
      createIssuerData.dualAuthRequiredAtCommit = '0';
    }

    if (createIssuerData.collectAdminContactDetails === 0) {
      createIssuerData.twoFactorDeliveryChannelType = 0;
    }

    const body = {
      'nameDecrypted': 'global',
      'nameEncrypted': 'XHEC18tn55UAPzKtu9cV6Q==',
      'action': 'add',
      'privId': 'S6209',
      // 'bank': createIssuerData.bankName,
      'locale': 'i18n/en_US',
      'loggedinlevel': '2',
      'level': '',
      'slt_selAdmins': createIssuerData.slt_selAdmins,
      'bankName': createIssuerData.bankName,
      'issuerDisplayName': createIssuerData.issuerDisplayName,
      'uploadKey': createIssuerData.uploadKey,
      'retypeUploadKey': createIssuerData.retypeUploadKey,
      'uploadKeyHidden': createIssuerData.uploadKeyHidden,
      'cvvKeyA': createIssuerData.cvvKeyA,
      'cvvKeyB': createIssuerData.cvvKeyB,
      'processorName': createIssuerData.processorName,
      'subProcessorName': createIssuerData.subProcessorName,
      'processorData': createIssuerData.processorData,
      'processorInfo': createIssuerData.processorInfo,
      'CVVKEYIND': createIssuerData.CVVKEYIND,
      'doNotPromptBehavior': createIssuerData.DoNotPromptBehavior,
      'localTimeZone': createIssuerData.localTimeZone,
      'customerId': createIssuerData.customerId,
      'userEncoding': createIssuerData.userEncoding,
      'bankKeyModule': createIssuerData.bankKeyModule,
      'arKeyModule': createIssuerData.arKeyModule,
      'userIdEnabled': createIssuerData.userIdEnabled,
      'auth_token': '',
      'twoStepLoginEnabled': createIssuerData.twoStepLoginEnabled,
      'dualAuthRequiredAtCommit': createIssuerData.dualAuthRequiredAtCommit,
      // 'is2FAAuthenticationEnabled': '',
      'collectAdminContactDetails': createIssuerData.collectAdminContactDetails,
      'twoFactorDeliveryChannelType': createIssuerData.twoFactorDeliveryChannelType,
      'twoFactorAuthenticationType': '0',
      'AUTH_TOKEN_VALIDATED': true,
      'encryptionKey': createIssuerData.encryptionKey,
      'bankDir': createIssuerData.bankDirName,
      'country': createIssuerData.country,
      'localeID': createIssuerData.localeID,
      'countryname': '',
      'enrollmentLocales': createIssuerData.enrollmentLocales,
      'adminLoggedInBank': 'i18n/en_US',
      'contextPath': '/vpas',
      'requestURI': '/vpas/admin/AddUpdateIssuer.jsp',
      'queryString': 'privId=S6222&bank=i18n/en_US&locale=en_US&loggedinlevel=2',
    };
    return this.http.post<any>(this.url, body, this.httpoptions);
  }
}
