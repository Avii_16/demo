import { TestBed } from '@angular/core/testing';

import { CreateIssuerService } from './create-issuer.service';

describe('CreateIssuerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreateIssuerService = TestBed.get(CreateIssuerService);
    expect(service).toBeTruthy();
  });
});
