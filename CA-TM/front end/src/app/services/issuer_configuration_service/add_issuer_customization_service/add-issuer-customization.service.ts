import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import Utils from '../../../utils';

@Injectable()
export class AddIssuerCustomizationService {
  sessionData: any = JSON.parse(localStorage.getItem('sessionData'));
  token: any = this.sessionData.token;
  url = environment.api_endpoint + '/vpas/api/issuer/';

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {}

  getIssuers() {
    return this.http.get<any>(environment.api_endpoint + '/vpas/api/banks', this.httpoptions);
  }

  getRangeGroup(bankId) {
    const localRangeGroupParams = {'bankId': bankId, 'locale': 'en_US', 'bank': 'i18n%2Fen_US'};
    const queryString = Utils.encodeQueryData(localRangeGroupParams);
    return this.http.get(this.url + 'rangegroup' + '?' + queryString, this.httpoptions);
  }

  getCardRanges(bankId, rangeGroupId) {
    const localCardRangeParams = {'bankId': bankId, 'rangeGroup': rangeGroupId, 'locale': 'en_US', 'bank': 'i18n%2Fen_US'};
    const queryString = Utils.encodeQueryData(localCardRangeParams);
    return this.http.get(this.url + 'cardrange' + '?' + queryString, this.httpoptions );
  }

  getDeviceLocale(bankId) {
    const localDeviceLocaleParams = {'bankId': bankId, 'locale': 'en_US', 'bank': 'i18n%2Fen_US'};
    const queryString = Utils.encodeQueryData(localDeviceLocaleParams);
    return this.http.get(this.url + 'devicelocale' + '?' + queryString, this.httpoptions );
  }

  getUpdateType(bankId) {
    const localUpdateTypeParams = {'bankId': bankId, 'updateTypeId': '2', 'locale': 'en_US', 'bank': 'i18n%2Fen_US'};
    const queryString = Utils.encodeQueryData(localUpdateTypeParams);
    return this.http.get(this.url + 'updatetype' + '?' + queryString, this.httpoptions  );
  }

  getFolderName(bankId, updateTypeId, deviceLocale, rangeId) {
    const localFolderParams = {'bankId': bankId, 'updateTypeId': updateTypeId, 'deviceLocale': deviceLocale, 'rangeId': rangeId};
    const queryString = Utils.encodeQueryData(localFolderParams);
    return this.http.get(this.url + 'useragentandfolder' + '?' + queryString, this.httpoptions );
  }

  addIssuerCustomizations(data) {
    if (data.UpgradedUI == false) {
      data.UpgradedUI = '0';
    } else {
      data.UpgradedUI = '1';
    }
    const body = {
      'action': 'add',
      'bankName': data.bankName,
      'bank': 'i18n/en_US',
      'locale': 'en_US',
      'authorizedBanksBitMap': 'f39/f39/f39/f39/f394AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=',
      'adminLoggedInBank': 'i18n/en_US',
      'rangeGroup': data.configId,
      'userAgent': data.userAgent,
      'deviceLocale': data.text_locale,
      'folder': data.folder,
      'acsURL': '2',
      'rangeID': data.rangeID,
      'updateType': data.updateType,
      'upgradedUI': data.UpgradedUI
    };
    return this.http.post<any>(this.url + 'customizeissuer', body, this.httpoptions);
  }
}
