import { TestBed } from '@angular/core/testing';

import { AddIssuerCustomizationService } from './add-issuer-customization.service';

describe('AddIssuerCustomizationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddIssuerCustomizationService = TestBed.get(AddIssuerCustomizationService);
    expect(service).toBeTruthy();
  });
});
