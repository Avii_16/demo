import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable()
export class UpdateFiInformationService {
  url = environment.api_endpoint + '/vpas/api/';
  sessionData: any = JSON.parse(localStorage.getItem('sessionData'));
  token: any = this.sessionData.token;
  constructor(private http: HttpClient) { }
  httpoptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.token
    })
  };

  getIssuers() {
    const body = {
      'task': 'display',
      'bank': 'i18n/en_US',
      'locale': 'en_US',
    };
    return this.http.post<any>(environment.api_endpoint + 'issuerranges', body, this.httpoptions);
  }

  getBankList() {
    const body = {
      'bank': 'i18n/en_US',
      'locale': 'en_US',
      'loggedinlevel': '2',
      'userAction': 'getbanks',
      'ADMINLOGGEDINBANK': 'i18n/en_US',
      'privid': 'S6215',
      'requestURI': '/vpas/admin/redirect.jsp',
      'privilegeId': '',
      'queryString': 'privId=S6222&bank=i18n/en_US&locale=en_US&loggedinlevel=2',
      'contextPath': '/vpas',
      'priviledgeId': '',
      'action': '',
      'reason': '',
      'remarks': '',
      'level': '',
      'authorizedBanksBitMap': 'f39/f39/f39/f39/fwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA='
    };
    return this.http.post<any>(this.url + 'fi/getbanks', body, this.httpoptions);

  }

  getCardRangeForIssuer(issuerId) {

    const body = {
      'bank': 'i18n/en_US',
      'locale': 'en_US',
      'loggedinlevel': '2',
      'userAction': 'getrange',
      'ADMINLOGGEDINBANK': 'i18n/en_US',
      'bankName': issuerId,
      'privid': 'S6215',
      'cardRangeName': '',
      'fiBusinessID': '38109501',
      'fiBIN': '381095',
      'cardType': '1',
      'panLength': '16',
      'beginRange': '0000000012345678',
      'endRange': '1234567890123456',
      'termPolicyVersion1': '1',
      'termPolicyVersion2': '0',
      'mobileEnabled': '1',
      'acsURL1': 'https://kumji02-L24980.lvn.broadcom.net/acspage/cap',
      'acsURL2': '',
      'acsURL3': '',
      'acsURL4': '',
      'acsURL5': '',
      'CAPURLParameters': '',
      'defaultToIssuer': '',
      'disablePwdExpiry': '',
      'chPwdExpiryDuration': '',
      'authMethod': null,
      'authSelect': null,
      'authFallback': null,
      'maxAuthTries': '1',
      'maxAuthTriesForFYP': '0',
      'authPriority': '',
      'nStrikesAcrossSessions': '1',
      'eAccess': null,
      'pluginURL': '',
      'pluginName': '',
      'hsmVariant': '',
      'pluginVersion': '',
      'ahsReceiptURL': 'http://localhost:56',
      'disablerange': null,
      'isVELogging': '0',
      'EnableAttemptsAfterMaxDeclines': null,
      'backupattempts': null,
      'actualEnrollOption': '7',
      'uploadFilePath_0': '',
      'actualMaxDecline': '5',
      'actualMaxWelcome': '0',
      'mcKeyID': '0',
      'mcKeyAlias': '',
      'cvvKeyA': '453215554444AGMJ',
      'cvvKeyB': 'gabcdef123456789',
      'CVVKEYIND': '01',
      'cardRangeGroup': '0',
      'signingKeyModule': 'S/W',
      'authResultKeyModule': 'S/W',
      'chipKeyModule': '',
      'aAVAlgorithm': '-1',
      'arcotOtpEnabled': null,
      'amdsProviderName': 'dost',
      'amdsFromNumber': '01',
      'amdsFromEmail': '',
      'amdsAPINAME': '',
      'amdsProfileEmail': '',
      'amdsProfilePassword': '',
      'amdsCustomerID': 'Custom1',
      'enableSCA': null,
      'trustedBeneficiary': null,
      'passwordChangeVelocity': '0',
      'prevVELogValue': '',
      'signingCertFile': '',
      'signingCertFile1': '',
      'uploadFileName_0': '',
      'receiptURL': '',
      'velogging': '',
      'uploadFileName_1': '',
      'requestURI': '/vpas/admin/redirect.jsp',
      'privilegeId': '',
      'queryString': 'privId=S6222&bank=i18n/en_US&locale=en_US&loggedinlevel=2',
      'cardRange': '',
      'contextPath': '/vpas',
      'priviledgeId': '',
      'actionUrl': '',
      'action': '',
      'reason': '',
      'remarks': '',
      'level': '',
      'cc_number': '',
      'adminName': 'XHEC18tn55UAPzKtu9cV6Q==',
      'backURL': '',
      'strShowBank': '',
      // tslint:disable-next-line: max-line-length
      'acl_decrypted': 'S9200:S9201:S8350:S9202:S9203:S6210:S6211:IDS.ACCT.D:S6450:S7023:IDS.TDSTOKDETOK:S3904:S6214:S6215:S6212:S7026:S9204:S6218:S9601:S9205:S6216:S6217:IDS.LINK.W:S9312:S9313:S6442:S7011:S8188:S12154:S13001:S12155:S13002:S6209:S8909:S12153:IDS.TOKDETOK:S6204:S6201:S8985:S6202:S6207:S6208:S6205:S6206:S6470:S8492:S6232:S6233:S6230:S6990:S6231:S12143:S12144:FM.ENROLL:IDS.ENCDEC:S6994:S6999:S3403:S3402:S9900:S6997:S9851:S8400:S6221:S6101:S6222:S3471:S6220:IDS.ACCT.W:FM.AUTH:IDS.ACCT.R:S11200:S6225:S6501:S6226:S6984:S6102:S6223:S6983:S6103:S6224:S6982:S6229:S6989:S6988:S8923:S6227:S6228:S9882:S9240:S84921:S9881:S12115:FM.RISK:S6418:S3549:S6419:S6412:S9923:S8557:S6413:S6410:S6411:S6416:S6417:S6414:S8559:S9992:S9112:S8541:S9111:S6409:S6408:S8943:S6401:S6402:S3495:S6405:S9997:S6406:S6403:S6404:S9300:IDS.FACT.R:S6310:S6431:IDS.FACT.W:S12101:S6314:S6311:S6432:S6433:S6438:S6439:S6315:S6436:S6316:S6437:S9134:S9135:S6420:IRIS.IDSCONFIG.TOOL:S6308:S6429:S6309:S6302:S8327:S6303:S6421:S6301:S6427:S6428:IDS.FACT.D:S9810:S6304:S6305',
      'brandingURL1': 'BankLogo.gif',
      'brandingURL2': 'visalog.gif',
      'authTokenFromReq': '-1739577157',
      'authTokensMap': {
        '1034373801': '338447693', '1228207067': '-1306981715', '-1589534758': '-1739577157', '-182548943': '-1256941635',
        '1199355329': '1473877433'
      },
      'formId': '-1589534758',
      'authorizedBanksBitMap': 'f39/f39/f39/f39/fwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA='
    };
    return this.http.post<any>(this.url + 'fi/getranges', body, this.httpoptions);
  }

  getIssuerDetailsFromRange(bankId, cardRange) {
    const body = {
      'bank': 'i18n/en_US',
      'locale': 'en_US',
      'loggedinlevel': '2',
      'userAction': 'get',
      'ADMINLOGGEDINBANK': 'i18n/en_US',
      'bankName': bankId,
      'privid': 'S6215',
      'cardRangeName': '',
      'fiBusinessID': '38109501',
      'fiBIN': '381095',
      'cardType': '1',
      'panLength': '16',
      'beginRange': '0000000012345678',
      'endRange': '1234567890123456',
      'termPolicyVersion1': '1',
      'termPolicyVersion2': '0',
      'mobileEnabled': '1',
      'acsURL1': 'https://kumji02-L24980.lvn.broadcom.net/acspage/cap',
      'acsURL2': '',
      'acsURL3': '',
      'acsURL4': '',
      'acsURL5': '',
      'CAPURLParameters': '',
      'defaultToIssuer': '',
      'disablePwdExpiry': '',
      'chPwdExpiryDuration': '',
      'authMethod': null,
      'authSelect': null,
      'authFallback': null,
      'maxAuthTries': '1',
      'maxAuthTriesForFYP': '0',
      'authPriority': '',
      'nStrikesAcrossSessions': '1',
      'eAccess': null,
      'pluginURL': '',
      'pluginName': '',
      'hsmVariant': '',
      'pluginVersion': '',
      'ahsReceiptURL': 'http://localhost:56',
      'disablerange': null,
      'isVELogging': '0',
      'EnableAttemptsAfterMaxDeclines': null,
      'backupattempts': null,
      'actualEnrollOption': '7',
      'uploadFilePath_0': '',
      'actualMaxDecline': '5',
      'actualMaxWelcome': '0',
      'mcKeyID': '0',
      'mcKeyAlias': '',
      'cvvKeyA': '453215554444AGMJ',
      'cvvKeyB': 'gabcdef123456789',
      'CVVKEYIND': '01',
      'cardRangeGroup': '0',
      'signingKeyModule': 'S/W',
      'authResultKeyModule': 'S/W',
      'chipKeyModule': '',
      'aAVAlgorithm': '-1',
      'arcotOtpEnabled': null,
      'amdsProviderName': 'dost',
      'amdsFromNumber': '01',
      'amdsFromEmail': '',
      'amdsAPINAME': '',
      'amdsProfileEmail': '',
      'amdsProfilePassword': '',
      'amdsCustomerID': 'Custom1',
      'enableSCA': null,
      'trustedBeneficiary': null,
      'passwordChangeVelocity': '0',
      'prevVELogValue': '',
      'signingCertFile': '',
      'signingCertFile1': '',
      'uploadFileName_0': '',
      'receiptURL': '',
      'velogging': '',
      'uploadFileName_1': '',
      'requestURI': '/vpas/admin/redirect.jsp',
      'privilegeId': '',
      'queryString': 'privId=S6222&bank=i18n/en_US&locale=en_US&loggedinlevel=2',
      'cardRange': cardRange,
      'contextPath': '/vpas',
      'priviledgeId': '',
      'actionUrl': '',
      'action': '',
      'reason': '',
      'remarks': '',
      'level': '',
      'cc_number': '',
      'backURL': '',
      'strShowBank': '',
      'brandingURL1': 'BankLogo.gif',
      'brandingURL2': 'visalog.gif',
      'authTokenFromReq': '-1739577157',
      'authTokensMap': {
        '1034373801': '338447693', '1228207067': '-1306981715', '-1589534758': '-1739577157', '-182548943': '-1256941635',
        '1199355329': '1473877433'
      },
      'formId': '-1589534758',
      'authorizedBanksBitMap': 'f39/f39/f39/f39/fwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA='
    };
    return this.http.post<any>(this.url + 'fi/getranges', body, this.httpoptions);
  }


  updateFIinformation(data, fileData) {
    {
      if (data.disablerange == true) {
        data.disablerange = '1';
      } else {
        data.disablerange = '0';
      }
    }
    {
      if (data.velogging == false) {
        data.velogging = '0';
      } else {
        data.velogging = '1';
      }
    }
    {
      if (data.nStrikesAcrossSessions == false) {
        data.nStrikesAcrossSessions = '0';
      } else {
        data.nStrikesAcrossSessions = '1';
      }

    }
    const body = {
      'bank': 'i18n/en_US',
      'locale': 'en_US',
      'loggedinlevel': '2',
      'userAction': 'update',
      'ADMINLOGGEDINBANK': 'i18n/en_US',
      'bankName': data.bankName,
      'privid': 'S6215',
      'cardRangeName': data.CardRangeName,
      'fiBusinessID': data.fiBusinessID,
      'fiBIN': data.fiBIN,
      'cardType': data.cardType,
      'panLength': data.panLength,
      'beginRange': data.beginRange,
      'endRange': data.endRange,
      'termPolicyVersion1': data.termPolicyVersion1,
      'termPolicyVersion2': data.termPolicyVersion2,
      'mobileEnabled': data.mobileEnabled,
      'acsURL1': data.acsURL1,
      'maxAuthTries': data.maxAuthTries,
      'maxAuthTriesForFYP': data.maxAuthTriesForFYP,
      'authPriority': data.authPriority,
      'nStrikesAcrossSessions': data.nStrikesAcrossSessions,
      'ahsReceiptURL': data.ahsReceiptURL,
      'disablerange': data.disablerange,
      'isVELogging': '0',
      'actualEnrollOption': data.ActualEnrollOption,
      'actualMaxDecline': data.ActualMaxDecline,
      'actualMaxWelcome': data.ActualMaxWelcome,
      'mcKeyID': data.mcKeyID,
      'mcKeyAlias': '',
      'cardRangeGroup': data.cardRangeGroup,
      'signingKeyModule': data.signingKeyModule,
      'authResultKeyModule': data.authResultKeyModule,
      'aAVAlgorithm': data.AAVAlgorithm,
      'amdsProviderName': data.amdsProviderName,
      'amdsFromNumber': data.amdsFromNumber,
      'amdsCustomerID': data.amdsCustomerID,
      'passwordChangeVelocity': data.passwordChangeVelocity,
      'requestURI': '/vpas/admin/redirect.jsp',
      'privilegeId': '',
      'queryString': 'privId=S6222&bank=i18n/en_US&locale=en_US&loggedinlevel=2',
      'cardRange': data.cardRange,
      'contextPath': '/vpas',
      'priviledgeId': '',
      'brandingURL1': data.brandingURL1,
      'brandingURL2': data.brandingURL2,
      'authTokenFromReq': '-1739577157',
      'authTokensMap': {
        '1034373801': '338447693', '1228207067': '-1306981715', '-1589534758': '-1739577157', '-182548943': '-1256941635',
        '1199355329': '1473877433'
      },
      'formId': '-1589534758',
      // tslint:disable-next-line: max-line-length
      'binaryFileMap': fileData,
      'authorizedBanksBitMap': 'f39/f39/f39/f39/fwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=',
      'uploadFilePath_0': 'C:\\Users\\rakaluvai\\Downloads\\signing_cert.p7b',
      'pluginURL': data.pluginURL

    };
    console.log(body);
    return this.http.put<any>(this.url + 'fi/updatefi', body, this.httpoptions);
  }

}
