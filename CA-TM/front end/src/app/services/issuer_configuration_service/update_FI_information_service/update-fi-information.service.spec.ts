import { TestBed } from '@angular/core/testing';

import { UpdateFiInformationService } from './update-fi-information.service';

describe('UpdateFiInformationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UpdateFiInformationService = TestBed.get(UpdateFiInformationService);
    expect(service).toBeTruthy();
  });
});
