import { TestBed } from '@angular/core/testing';

import { ConfigureIssuerParametersService } from './configure-issuer-parameters.service';

describe('ConfigureIssuerParametersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigureIssuerParametersService = TestBed.get(ConfigureIssuerParametersService);
    expect(service).toBeTruthy();
  });
});
