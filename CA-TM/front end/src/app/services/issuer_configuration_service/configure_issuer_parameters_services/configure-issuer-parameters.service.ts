import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable()
export class ConfigureIssuerParametersService {

  sessionData: any = JSON.parse(localStorage.getItem('sessionData'));
  token: any = this.sessionData.token;
  loggedInAdmin: string = this.sessionData.adminName;
  url = environment.api_endpoint + '/vpas/api/';
  constructor(private http: HttpClient) {

  }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
      'Content-Type': 'application/json'
    })
  };

  getAllIssuers() {
    return this.http.get<any>(this.url + 'banks', this.httpoptions);
  }

  getIssuerParams(issuerData) {
    const body = {
      'privId': 'S6305',
      'actualPage': 'EditIssuerParameters.jsp',
      'actionUrl': 'EditIssuerParameters.jsp',
      'bank': 'i18n/en_US',
      'level': '',
      'locale': 'en_US',
      'loggedinlevel': '2',
      'auth': '1',
      'adminName': this.loggedInAdmin,
      'adminLoggedInBank': 'i18n/en_US',
      // tslint:disable-next-line: max-line-length
      'acl_decrypted': 'S9200:S9201:S8350:S9202:S9203:S6210:S6211:IDS.ACCT.D:S6450:S7023:IDS.TDSTOKDETOK:S3904:S6214:S6215:S6212:S7026:S9204:S6218:S9601:S9205:S6216:S6217:IDS.LINK.W:S9312:S9313:S6442:S7011:S8188:S12154:S13001:S12155:S13002:S6209:S8909:S12153:IDS.TOKDETOK:S6204:S6201:S8985:S6202:S6207:S6208:S6205:S6206:S6470:S8492:S6232:S6233:S6230:S6990:S6231:S12143:S12144:FM.ENROLL:IDS.ENCDEC:S6994:S6999:S3403:S3402:S9900:S6997:S9851:S8400:S6221:S6101:S6222:S3471:S6220:IDS.ACCT.W:FM.AUTH:IDS.ACCT.R:S11200:S6225:S6501:S6226:S6984:S6102:S6223:S6983:S6103:S6224:S6982:S6229:S6989:S6988:S8923:S6227:S6228:S9882:S9240:S84921:S9881:S12115:FM.RISK:S6418:S3549:S6419:S6412:S9923:S8557:S6413:S6410:S6411:S6416:S6417:S6414:S8559:S9992:S9112:S8541:S9111:S6409:S6408:S8943:S6401:S6402:S3495:S6405:S9997:S6406:S6403:S6404:S9300:IDS.FACT.R:S6310:S6431:IDS.FACT.W:S12101:S6314:S6311:S6432:S6433:S6438:S6439:S6315:S6436:S6316:S6437:S9134:S9135:S6420:IRIS.IDSCONFIG.TOOL:S6308:S6429:S6309:S6302:S8327:S6303:S6421:S6301:S6427:S6428:IDS.FACT.D:S9810:S6304:S6305',
      'nameDecrypted': 'GLOBAL',
      'nameEncrypted': this.loggedInAdmin,
      // tslint:disable-next-line: max-line-length
      'authorizedBanksBitMap': 'f39/f39/f39/f39/fwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=',
      'contextPath': '/vpas',
      'requestURI': '/vpas/admin/EditIssuerParameters.jsp',
      'queryString': 'bank=i18n/en_US&locale=en_US&loggedinlevel=2&actionUrl=EditIssuerParameters.jsp&privId=S6305',
      'secondaryBank': issuerData
    };
    console.log(body);
    return this.http.post<any>(this.url + 'issuer/parameters', body, this.httpoptions);
  }

  submitEditParams(editedData) {
    console.log(editedData);

    if (editedData.caseGroupingForPriority == true) {
      editedData.caseGroupingForPriority = '1';
    } else {
      editedData.caseGroupingForPriority = '0';
    }

    if (editedData.SupportUpgradedCAPUI == false) {
      editedData.SupportUpgradedCAPUI = '0';
    } else {
      editedData.SupportUpgradedCAPUI = '1';
    }

    if (editedData.sendTempPasswordToCH == false) {
      editedData.sendTempPasswordToCH = '0';
    } else {
      editedData.sendTempPasswordToCH = '1';
    }

    if (editedData.EnableAbridgedADS == false) {
      editedData.EnableAbridgedADS = '0';
    } else {
      editedData.EnableAbridgedADS = '1';
    }

    if (editedData.simultaneousCHLogin == false) {
      editedData.simultaneousCHLogin = '0';
    } else {
      editedData.simultaneousCHLogin = '1';
    }

    if (editedData.enrollmentOfLockedCard == false) {
      editedData.enrollmentOfLockedCard = '0';
    } else {
      editedData.enrollmentOfLockedCard = '1';
    }

    if (editedData.enableRiskFort == false) {
      editedData.enableRiskFort = null;
    } else {
      editedData.enableRiskFort = '1';
    }

    if (editedData.RFIntermediatePage == false) {
      editedData.RFIntermediatePage = '0';
    } else {
      editedData.RFIntermediatePage = '1';
    }

    if (editedData.ENABLEARGUS == false) {
      editedData.ENABLEARGUS = '0';
    } else {
      editedData.ENABLEARGUS = '1';
    }

    if (editedData.CollectMCDDNA == false) {
      editedData.CollectMCDDNA = '0';
    } else {
      editedData.CollectMCDDNA = '1';
    }

    if (editedData.VELOG == false) {
      editedData.VELOG = '0';
    } else {
      editedData.VELOG = '1';
    }

    if (editedData.LogVEReqPAReqTime == false) {
      editedData.LogVEReqPAReqTime = '0';
    } else {
      editedData.LogVEReqPAReqTime = '1';
    }

    if (editedData.LogPreviousTxnDetails == false) {
      editedData.LogPreviousTxnDetails = '0';
    } else {
      editedData.LogPreviousTxnDetails = '1';
    }

    if (editedData.LogRFStatus == false) {
      editedData.LogRFStatus = '0';
    } else {
      editedData.LogRFStatus = '1';
    }

    if (editedData.EnableCaseAutoAssignment == false) {
      editedData.EnableCaseAutoAssignment = '0';
    } else {
      editedData.EnableCaseAutoAssignment = '1';
    }

    if (editedData.haWithBackupServers == false) {
      editedData.haWithBackupServers = '0';
    } else {
      editedData.haWithBackupServers = '1';
    }

    if (editedData.maxTransactionsWithoutAOTP == false) {
      editedData.maxTransactionsWithoutAOTP = '0';
    } else {
      editedData.maxTransactionsWithoutAOTP = '1';
    }

    if (editedData.suppressCHContactDetailsInput == false) {
      editedData.suppressCHContactDetailsInput = '0';
    } else {
      editedData.suppressCHContactDetailsInput = '1';
    }

    if (editedData.enableAbridgedFyp == true) {
      editedData.enableAbridgedFyp = '1';
    } else {
      editedData.enableAbridgedFyp = '0';
    }

    if (editedData.uaCheckEnabled == false) {
      editedData.uaCheckEnabled = '0';
    } else {
      editedData.uaCheckEnabled = '1';
    }

    if (editedData.pwdUsagePolicy == false) {
      editedData.pwdUsagePolicy = '0';
    } else {
      editedData.pwdUsagePolicy = '1';
    }

    if (editedData.autoAssignmentForPriority == true) {
      editedData.autoAssignmentForPriority = '1';
    } else {
      editedData.autoAssignmentForPriority = '0';
    }

    if (editedData.maxNonAOTPTxns == true) {
      editedData.maxNonAOTPTxns = '1';
    } else {
      editedData.maxNonAOTPTxns = '0';
    }

    if (editedData.includeClosedCases == true) {
      editedData.includeClosedCases = '1';
    } else {
      editedData.includeClosedCases = '0';
    }

    const body = {
        'bank': 'i18n/en_US',
        // tslint:disable-next-line: radix
        'bankId' : parseInt(editedData.bankName),
        'locale': 'en_US',
        'loggedinlevel': '2',
        'passPhrase': editedData.passPhrase,
        'retypeDUPassPhrase': editedData.retypeDUPassPhrase,
        'adminLoggedInBank': 'i18n/en_US',
        'formattedDate': editedData.formattedDate,
        'dateOrder': editedData.dateOrder.toString(),
        'timeStampFormat': editedData.timeStampFormat,
        'recordsPerPage': editedData.recordsPerPage.toString(),
        'symbolLink': editedData.symbolLink.toString(),
        'symbolDisplay': editedData.symbolDisplay.toString(),
        'dateFormat': editedData.dateFormat,
        'dateSeparator': editedData.dateSeparator,
        'pwdUsagePolicy': editedData.pwdUsagePolicy,
        'tempPwdDuration': editedData.tempPwdDuration.toString(),
        'chPwdExpiryDuration': '0',
        'supportUpgradedCAPUI': editedData.SupportUpgradedCAPUI,
        'enableAbridgedADS': editedData.EnableAbridgedADS,
        'enrollmentLockingPolicy': editedData.enrollmentLockingPolicy.toString(),
        'maxValForLastPwdList': editedData.maxValForLastPwdList.toString(),
        'promptNPasswordCharacters': editedData.promptNPasswordCharacters.toString(),
        'enableRiskFort': editedData.enableRiskFort,
        // 'enableRiskFort' : null,
        'rfIntermediatePage': '1',
        'collectMCDDNA': editedData.CollectMCDDNA,
        'cookieType': editedData.cookieType.toString(),
        'logVEReqPAReqTime': editedData.LogVEReqPAReqTime,
        'simultaneousCHLogin': editedData.simultaneousCHLogin,
        'enrollmentOfLockedCard': editedData.enrollmentOfLockedCard,
        'arcotOtpEnabled': editedData.arcotOtpEnabled.toString(),
        'authMinderOrgName': editedData.authMinderOrgName,
        'arcotOtpImagesRelativePath': editedData.arcotOtpImagesRelativePath,
        'arcotOTPProfileName': editedData.arcotOTPProfileName,
        'arcotEMVAccountType': editedData.arcotEMVAccountType,
        'maxTransactionsWithoutAOTP': editedData.maxTransactionsWithoutAOTP,
        'maxNonAOTPTxns': editedData.maxNonAOTPTxns,
        'calloutAuthMethod': editedData.calloutAuthMethod,
        'contextPath': '/vpas',
        'pareqWaitTimeBuffer': editedData.pareqWaitTimeBuffer.toString(),
        'uaCheckEnabled': editedData.uaCheckEnabled,
        'passwordStorageAlgorithm': editedData.passwordStorageAlgorithm.toString(),
        'authminderFailOverOptions': editedData.authminderFailOverOptions.toString(),
        'displayOptionalInfo': editedData.displayOptionalInfo.toString(),
        'maxChangePwdTries': editedData.maxChangePwdTries.toString(),
        'chPwdExpiry': '0',
        'velog': editedData.VELOG,
        'logPreviousTxnDetails': editedData.LogPreviousTxnDetails,
        'logRFStatus': editedData.LogRFStatus,
        'enableCaseAutoAssignment': editedData.EnableCaseAutoAssignment,
        'haWithBackupServers': editedData.haWithBackupServers,
        'enableAbridgedFyp': editedData.enableAbridgedFyp,
        'brandRFIntermediatePage': editedData.BrandRFIntermediatePage.toString(),
        'enableargus': editedData.ENABLEARGUS,
        'raDownHandleAction': editedData.raDownHandleAction.toString(),
        'autoAssignmentForPriority': editedData.autoAssignmentForPriority,
        'caseGroupingForPriority': editedData.caseGroupingForPriority,
        'includeClosedCases': editedData.includeClosedCases,
        'caseGroupingForPriorityInCM': '0',
        'emailPIIType': editedData.emailPIIType,
        'pamPIIType': editedData.pamPIIType,
        'issuerAnsPIIType': editedData.issuerAnsPIIType,
        'amdsCustomerID': editedData.amdsCustomerID,
        'amdsProviderName': editedData.amdsProviderName,
        'amdsFromNumber': editedData.amdsFromNumber,
        'amdsFromEmail': editedData.amdsProfileEmail,
        'amdsAPINAME': editedData.amdsAPINAME,
        'amdsProfileEmail': editedData.amdsProfileEmail,
        'amdsProfilePassword': editedData.amdsProfilePassword,
        'suppressCHContactDetailsInput': editedData.suppressCHContactDetailsInput,
        'sendTempPasswordToCH': editedData.sendTempPasswordToCH,
        'enableSCA': editedData.ENABLESCA,
        'trustedBeneficiary': editedData.TRUSTEDBENEFICIARY
      };
    console.log(body);
    return this.http.put<any>(this.url + 'issuer/parameters', body, this.httpoptions);
  }
}

