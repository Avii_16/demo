import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable()
export class UpdateIssuerService {

  sessionData: any = JSON.parse(localStorage.getItem('sessionData'));
  token: any = this.sessionData.token;
  url = environment.api_endpoint + '/vpas/api/';

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
      'Content-Type': 'application/json'
    })
  };
  constructor(private http: HttpClient) { }

  getAllIssuers() {
    console.log(this.token);

    return this.http.get<any>(this.url + 'banks', this.httpoptions);
  }
  // get list of all users
  getAllUsers() {
    return this.http.get<any>(this.url + 'admins/2', this.httpoptions);
  }

  getIssuerDetails(bankId) {
    return this.http.get<any>(this.url + 'issuer/' + bankId, this.httpoptions);
  }

  updateIssuers(data) {

    if (data.userIdEnabled === true) {
      data.userIdEnabled = '1';
    } else {
      data.userIdEnabled = '0';
    }

    if (data.twoStepLoginEnabled === true) {
      data.twoStepLoginEnabled = '1';
    } else {
      data.twoStepLoginEnabled = '0';
    }

    if (data.dualAuthRequiredAtCommit === true) {
      data.dualAuthRequiredAtCommit = '1';
    } else {
      data.dualAuthRequiredAtCommit = '0';
    }

    if (data.collectAdminContactDetails === 0) {
      data.twoFactorDeliveryChannelType = 0;
    }
    const body = {

      'nameDecrypted': 'GLOBAL',
      'nameEncrypted': 'XHEC18tn55UAPzKtu9cV6Q==',
      'action': 'update',
      'uploadKeyHidden': '',
      'privId': 'S6209',
      'locale': 'i18n/en_US',
      'loggedinlevel': '2',
      'slt_selAdmins': data.slt_selAdmins,
      'bankName': data.bankName,
      'issuerDisplayName': data.issuerDisplayName,
      'uploadKey': data.uploadKey,
      'retypeUploadKey': data.retypeUploadKey,
      'cvvKeyA': data.cvvKeyA,
      'cvvKeyB': data.cvvKeyB,
      'processorName': data.processorName,
      'subProcessorName': data.subProcessorName,
      'processorData': data.processorData,
      'processorInfo': data.processorInfo,
      'CVVKEYIND': data.CVVKEYIND,
      'doNotPromptBehavior': data.DoNotPromptBehavior,
      'localTimeZone': data.localTimeZone,
      'customerId': data.customerId,
      'userEncoding': data.userEncoding,
      'bankKeyModule': data.bankKeyModule,
      'arKeyModule': data.arKeyModule,
      'userIdEnabled': data.userIdEnabled,
      'twoStepLoginEnabled': data.twoStepLoginEnabled,
      'dualAuthRequiredAtCommit': data.dualAuthRequiredAtCommit,
      'is2FAAuthenticationEnabled': '',
      'collectAdminContactDetails': data.collectAdminContactDetails,
      'twoFactorDeliveryChannelType': data.twoFactorDeliveryChannelType,
      'twoFactorAuthenticationType': '0',
      'AUTH_TOKEN_VALIDATED': true,
      'encryptionKey': data.encryptionKey,
      'bankDir': data.bankDirName,
      'country': data.country,
      'localeID': data.localeID,
      'enrollmentLocales': data.enrollmentLocales,
      'adminLoggedInBank': 'i18n/en_US',
      'contextPath': '/vpas',
      'requestURI': '/vpas/admin/AddUpdateIssuer.jsp',
      'queryString': 'privId=S6222&bank=i18n/en_US&locale=en_US&loggedinlevel=2',
      'authorizedBanksBitMap': 'f39/f39/f39/f39/f39wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA='

    };
    console.log(body);
    return this.http.put<any>(this.url + 'issuer/', body, this.httpoptions);
  }

}
