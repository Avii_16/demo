import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class EnableCardRangesService {

  sessionData: any = JSON.parse(localStorage.getItem('sessionData'));
  token: any = this.sessionData.token;

  url = environment.api_endpoint + '/vpas/api/';

  // default constructor
  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
      'Content-Type': 'application/json'
    })
  };

  // method to fet all Issuer Administrators
  getIssuers() {
    
    const body = {
      'task': 'display',
      'bank': 'i18n/en_US',
      'locale': 'en_US'
    };

    return this.http.post<any>(this.url + 'issuerranges', body, this.httpoptions);
  }

  // method to get Range for an Issuer
  getRangeForSelectedIssuer(bankId: any) {

    const body = {
      'task': 'changeIssuer',
      'selectedBankId': bankId,
      'bank': 'i18n/en_US',
      'locale': 'en_US',
      'bankId': bankId

    };
    return this.http.post<any>(this.url + 'rangesbasedonissuer', body, this.httpoptions);

  }
  enableCardRange(data, bankId) {
    if (data.rangeIds == ""){
      data.rangeIds = [];
    } else {
      data.rangeIds = data.rangeIds;
    }

    const body = {
      'task': 'enableCardRanges',
      'selectedBankId': bankId,
      'rangeIds': data.rangeIds,
      'bank': 'i18n/en_US',
      'locale': 'en_US',
      'bankId': bankId
    };
    return this.http.post<any>(this.url + 'cardranges', body, this.httpoptions);
  }
}
