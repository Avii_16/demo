// Angular Imports
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

// environment imports
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthServiceService {

  // default constructor
  constructor(private http: HttpClient) {
  }
  body: any;
  url: string = environment.api_endpoint + '/vpas/api/';
  masterurl: string = environment.api_endpoint + 'api/admin/master/signin';

  // method to login admin
  doLogin(credentials) {
    credentials.roaming = 'false';
    credentials.key = 'masterKey';
    return this.http.post<any>(this.url + 'auth/login', credentials, {
      params: {
        'bank': 'i18n/en_US'
      }
    });
  }

  // method to login admin
  doIssuerLogin(credentials, bank) {
    credentials.locale = 'en_US';
    credentials.roaming = 'false';
    credentials.key = 'masterKey';
    return this.http.post<any>(this.url + 'auth/login', credentials, {
      params: {
        'bank': bank
      }
    });
  }

  // method to login master admin
  doMasterLogin(credentials) {
    return this.http.post<any>(this.masterurl, credentials);
  }

  // method to change password
  dochangePassword(data) {
    const token = JSON.parse(localStorage.getItem('sessionData')).token;
    const httpoptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      })
    };
    if (localStorage.getItem('tempPasswordDuration') === '-3') {
      this.body = {
        'bank': 'i18n/en_US',
        'locale': 'en_US',
        'password': data.password,
        'retypeNewpassword': data.retypeNewpassword,
        'oldPassword': data.oldPassword,
        'attempts': '0',
        'loggedInAdminName': localStorage.getItem('username')
      };
    } else {
      this.body = {
        'bank': 'i18n/en_US',
        'locale': 'en_US',
        'password': data.password,
        'retypeNewpassword': data.retypeNewpassword,
        'attempts': '0',
        'loggedInAdminName': localStorage.getItem('username')
      };
    }
    console.log(this.body);
    
    return this.http.put<any>(this.url + 'admin/passwordchange', this.body, httpoptions);
  }

  // common method to fetch locales
  getLocales() {
    const token = JSON.parse(localStorage.getItem('sessionData')).token;
    const httpoptions = {
      headers: new HttpHeaders({
        'Authorization': token,
        'Content-Type': 'application/json'
      })
    };

    return this.http.get<any>(this.url + 'admin/locales', httpoptions);
  }
}

