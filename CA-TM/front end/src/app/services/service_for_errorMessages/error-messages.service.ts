import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ErrorMessagesService {

  // default constructor
  constructor(private http: HttpClient) { }

  public getJSON(): Observable<any> {
    return this.http.get('../assets/Errors/ErrorMessages.json');
  }
  public getNavbarDetails() {
    return this.http.get('../../../assets/navbardata/navbar.json');
  }
}
