import { TestBed } from '@angular/core/testing';

import { UpdateGlobalAdminPrivilegesService } from './update-global-admin-privileges.service';

describe('UpdateGlobalAdminPrivilegesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UpdateGlobalAdminPrivilegesService = TestBed.get(UpdateGlobalAdminPrivilegesService);
    expect(service).toBeTruthy();
  });
});
