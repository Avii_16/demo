import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class UpdateGlobalAdminPrivilegesService {
  url: string = environment.api_endpoint + '/vpas/api/';
  token: any = JSON.parse(localStorage.getItem('sessionData')).token;

  // default constructor
  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
      'Content-Type': 'application/json'
    })
  };

  // call to get all users
  getallUsers() {
    return this.http.get<any>(this.url + 'admins/2', this.httpoptions);
  }

  // call to get all banks
  getallbanks() {
    return this.http.get<any>(this.url + 'banks', this.httpoptions);
  }
  // getting all privileges
  getAllPrivileges() {
    // let body = {
    // "adminName": this.sessionData.adminName
    // }
    return this.http.get<any>(this.url + 'globaladmin/' + 'privileges/', this.httpoptions);
  }
  // call to get admin details
  getAdminDetails(userId) {

    const body = {
      'locale': 'en_US',
      'action': 'changeAdmin',
      'adminIdClear': userId,
      'level': '2',
      'bank': 'i18n/en_US',
      'adminBankId': '0'
    };
    return this.http.put<any>(this.url + 'admin/privileges/', body, this.httpoptions);
  }

  // call to get all update global admin privileges
  updateGLobalAdminPrivileges(adminData: any) {
    let processerName;
    if (adminData.processorName == null) {
       processerName = '-1000';
    } else {
      processerName = adminData.processorName;
    }

    let subProcesserName;
    if (adminData.subProcesserName == null) {
      subProcesserName = '-1000';
    } else {
      subProcesserName = adminData.subProcesserName;
    }
    let maxValForLastPwdList;
    if (adminData.maxValForLastPwdList == null) {
      maxValForLastPwdList = '0';
    } else {
      maxValForLastPwdList = adminData.maxValForLastPwdList.toString();
    }

    const body = {
      'locale': 'en_US',
      'action': 'saveAdmin',
      'adminIdClear': adminData.userEncoding,
      'adminBankId': '0',
      'firstNameClear': adminData.firstName,
      'middleNameClear': adminData.middleName,
      'lastNameClear': adminData.lastName,
      'description': adminData.description,
      'selectedBanks': adminData.slt_selIssuers,
      'blockkSimultaneousLogin': adminData.blksimultlogin,
      'updateProfileAtLogin': adminData.updateProfileAtLogin,
      'maxValForLastPwdList': maxValForLastPwdList,
      'passwordType': true,
      'subProcessorName': subProcesserName,
      'processorName': processerName,
      'twoFactorAuthenticationType': 1,
      // tslint:disable-next-line: radix
      'twoFactorDeliveryChannelType': parseInt(adminData.twoFactorDeliveryChannelType),
      'twoFactorAuthenticationEnabled': true,
      // tslint:disable-next-line: radix
      'collectAdminContactDetails': parseInt(adminData.collectAdminContactDetails),
      'passwordTypeChanged': true,
      'level': '2',
      'pwdNeverExpires': adminData.pwdNeverExpires,
      'privileges': adminData.privs,
      'bank': 'i18n/en_US',
      'templateIdClear': adminData.templateIdClear
    };
    return this.http.put<any>(this.url + 'admin/privileges', body, this.httpoptions);
  }

}
