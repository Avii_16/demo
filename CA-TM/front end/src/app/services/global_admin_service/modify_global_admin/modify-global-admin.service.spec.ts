import { TestBed } from '@angular/core/testing';

import { ModifyGlobalAdminService } from './modify-global-admin.service';

describe('ModifyGlobalAdminServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModifyGlobalAdminService = TestBed.get(ModifyGlobalAdminService);
    expect(service).toBeTruthy();
  });
});
