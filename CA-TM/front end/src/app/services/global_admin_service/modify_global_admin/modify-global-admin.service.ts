import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class ModifyGlobalAdminService {

  url: string = environment.api_endpoint + '/vpas/api/';
  masterUrl: string = environment.api_endpoint + '/vpas/api/admins/2';

  token: any = JSON.parse(localStorage.getItem('sessionData')).token;

  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
      'Content-Type': 'application/json'
    })
  };

  // method to get all Admins
  getAllAdmins() {
    return this.http.get<any>(this.masterUrl, this.httpoptions);
  }

  // method to get admin details
  getAdminDetails(adminName: any) {
    return this.http.get<any>(this.url + 'admin/' + adminName + '/' + 0 + '/status', this.httpoptions);
  }

  // method to modify global admin
  modifyGlobalAdmin(data: any) {
    const body = {
      'selectedAdminBank': '0',
      'selectedAdminName': data.selectedAdminName,
      'remark': data.remark,
      'level': '2',
      'action': 'updateUser',
      'status': data.status,
      'locale': 'en_US',
      'bank': 'i18n/en_US',
      'adminForUpdate': data.selectedAdminName
    };
    return this.http.put<any>(this.url + 'admin/status', body, this.httpoptions);
  }
}
