import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class GlobalAdminService {

  url: string = environment.api_endpoint + '/vpas/api/globaladmin/';
  getUrl: string = environment.api_endpoint + '/vpas/api/';
  sessionData: any = JSON.parse(localStorage.getItem('sessionData'));
  token: any = this.sessionData.token;

  // default constructor
  constructor(private http: HttpClient) { }
  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
      'Content-Type': 'application/json'
    })
  };

  // get list of all users
  getAllUsers() {
    return this.http.get<any>(this.getUrl + 'admins/2', this.httpoptions);
  }

  // common call to get all countries list
  getAllCountries() {
    return this.http.get<any>(this.getUrl + 'countries/2', this.httpoptions);
  }

  // common call to get all issuers list
  getAllIssuers() {
    return this.http.get<any>(this.getUrl + 'banks', this.httpoptions);
  }

  // common call to get all privileges
  getAllPrivileges() {
    // let body = {
    // "adminName": this.sessionData.adminName
    // }
    return this.http.get<any>(this.url + 'privileges/', this.httpoptions);
  }

  // common call to get list of banks
  getBanklist(template) {
    return this.http.get<any>(environment.api_endpoint + 'vpas/api/banks/' + template, this.httpoptions);
  }

  // common call to get a list of all templates
  getTemplates() {
    const body = {
      'level': 2
    };
    return this.http.post<any>(environment.api_endpoint + 'vpas/api/admin/authtemplates', body, this.httpoptions);
  }

  // call to create a Global Admin
  createGlobalAdmin(data: any) {

    if (data.changePasswordAtFirstLogin === true) {
      data.changePasswordAtFirstLogin = '1';
    } else {
      data.changePasswordAtFirstLogin = '0';
    }

    if (data.pwdNeverExpires === true) {
      data.pwdNeverExpires = '1';
    } else {
      data.pwdNeverExpires = '0';
    }

    if (data.blkSimultLogin === true) {
      data.blockksimultlogin = '1';
    } else {
      data.blockksimultlogin = '0';
    }

    if (data.updateProfileAtLogin === true) {
      data.updateProfileAtLogin = '1';
    } else {
      data.updateProfileAtLogin = '0';
    }

    if (data.collectAdminContactDetails === 0) {
      data.twoFactorDeliveryChannelType = 4;
    }

    const body = {
      'adminUserId': data.userid,
      'firstName': data.firstName,
      'lastName': data.lastName,
      'level': '2',
      'middleName': data.middleName,
      'desciption': data.description,
      'maxValForLastPwdList': data.maxValForLastPwdList,
      'pasword': data.password,
      'rePassword': data.rePassword,
      'changePasswordAtFirstLogin': data.changePasswordAtFirstLogin,
      'pwdNeverExpires': data.pwdNeverExpires,
      'blksimultlogin': data.blockksimultlogin,
      'updateProfileAtLogin': data.updateProfileAtLogin,
      'collectAdminContactDetails': data.collectAdminContactDetails.toString(),
      'twoFactorDeliveryChannelType': data.twoFactorDeliveryChannelType,
      'subProcessorName': data.subProcessorName,
      'selectedAdminBank': '0',
      'selectedTemplateName': data.selectedTemplateName,
      'countries': data.slt_selCountries,
      'bankList': data.slt_selIssuers,
      'successMsg': '0',
      'error': '0',
      'secAdminName': 'global',
      'secAdminName_decrypted': 'global',
      'multiControl': 'false',
      // 'listOfCountries': '',
      'errorPwdPolicy': 'nilesh',
      'blkSimultLogin': data.blockksimultlogin,
      'is2FAAuthenticationEnabled': '0',
      'processorName': data.processorName,
      'twoFactorDelChnlType1': '0',
      'twoFactorAuthenticationType': '1',
      'deliveryChannel': '0',
      'privs': data.privs,
      'bank': 'i18n/en_US',
      'action': 'newUser'
    };
    {
    if (data.slt_selCountries.length == 0) {
       delete body['countries'];
     }
    }
    {
      if (data.slt_selIssuers.length == 0) {
        delete body['bankList'];
      }
    }
    {
      if (data.privs.length == 0) {
        delete body['privs'];
      }
    }
    return this.http.post<any>(this.url, body, this.httpoptions);
  }

  // call to create a global admin with template
  createGlobalAdminTemplate(data: any) {
    if (data.pwdNeverExpires === true) {
      data.pwdNeverExpires = '1';
    } else {
      data.pwdNeverExpires = '0';
    }
    const body = {
      'adminUserId': data.userid,
      'firstName': data.firstName,
      'lastName': data.lastName,
      'level': 2,
      'middleName': data.middleName,
      'desciption': data.description,
      'pwdNeverExpires': data.pwdNeverExpires,
      'selectedAdminBank': '0',
      'selectedTemplateName': data.selectedTemplateName,
      'successMsg': 'nilesh',
      'error': 'nilesh',
      'secAdminName': 'global',
      'secAdminName_decrypted': 'global',
      'multiControl': 'false',
      'errorPwdPolicy': 'nilesh',
      'bankList': data.slt_selIssuers,
      'bank': 'i18n/en_US',
      'action': 'newUser'
    };
    return this.http.post<any>(this.url, body, this.httpoptions);
  }
}
