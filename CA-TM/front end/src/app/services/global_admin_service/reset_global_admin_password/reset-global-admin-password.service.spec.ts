import { TestBed } from '@angular/core/testing';

import { ResetGlobalAdminPasswordService } from './reset-global-admin-password.service';

describe('ResetGlobalAdminPasswordService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResetGlobalAdminPasswordService = TestBed.get(ResetGlobalAdminPasswordService);
    expect(service).toBeTruthy();
  });
});
