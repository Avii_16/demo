import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class ResetGlobalAdminPasswordService {

  bankUrl: string = environment.api_endpoint + '/vpas/api/admins/2';
  resetUrl: string = environment.api_endpoint + '/vpas/api/admin/passwordreset';

  token: any = JSON.parse(localStorage.getItem('sessionData')).token;

  // default constructor
  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
    })
  };

  // call to get all admins
  getAllAdmins() {
    return this.http.get<any>(this.bankUrl, this.httpoptions);
  }

  // call to reset global admin
  resetGlobalAdmin(data: any, adminname) {
    const body = {
      'loggedInUserid': 'global',
      'selectedBank': '0',
      'selectedAdmin': adminname,
      'password': data.newPassword,
      'remarks': data.remark,
      'level': 2,
      'action': 'updateUser',
      'reCheckPassword': data.retypeNewpassword,
      'changePwdAtLogin': '0',
      'locale': 'en_US',
      'loggedInBank': 'i18n/en_US',
      'emailCheck': 'on',
      'phoneCheck': 'on'
    };
    return this.http.put<any>(this.resetUrl, body, this.httpoptions);
  }
}
