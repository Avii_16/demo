import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class ConfigureIssuerAdminPrivilegesService {

  privilegeUrl = environment.api_endpoint + '/vpas/api/issueradmin/privileges';
  updatePrivilegesUrl = environment.api_endpoint + '/vpas/api/issueradmin/privilegeconfigure';

  token: any = JSON.parse(localStorage.getItem('sessionData')).token;

  // default constructor
  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
    })
  };

  // call to get all issuer privileges
  getAllIssuerPrivilegeData() {
    return this.http.get<any>(this.privilegeUrl, this.httpoptions);
  }

  // call to get all update privileges
  updatePrivileges(privIds: any, privType: any, privNumberOfAdmins: any, privEnable: any) {
    const privilegeType = [];
    for (let i = 0; i < privType.length; i++) {
      privilegeType.push(privType[i].toString());
    }

   const body = {
      'level': '3',
      'action': 'updatePrivileges',
      'privType': privilegeType,
      'privIds': privIds,
      'privNumberOfAdmins': privNumberOfAdmins,
      'privEnable': privEnable,
      'locale': 'en_US',
      'bankId': '0'
    };
    return this.http.put<any>(this.privilegeUrl, body , this.httpoptions);
  }
}
