import { TestBed } from '@angular/core/testing';

import { ConfigureIssuerAdminPrivilegesService } from './configure-issuer-admin-privileges.service';

describe('ConfigureIssuerAdminPrivilegesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigureIssuerAdminPrivilegesService = TestBed.get(ConfigureIssuerAdminPrivilegesService);
    expect(service).toBeTruthy();
  });
});
