import { TestBed } from '@angular/core/testing';

import { CreateIssuerAdminService } from './create-issuer-admin.service';

describe('CreateIssuerAdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreateIssuerAdminService = TestBed.get(CreateIssuerAdminService);
    expect(service).toBeTruthy();
  });
});
