import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class CreateIssuerAdminService {

  url = environment.api_endpoint + '/vpas/api/issueradmin/';
  sessionData: any = JSON.parse(localStorage.getItem('sessionData'));

  // default constructor
  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.sessionData.token,
    })
  };
  getAdminContactDetails(issuer) {
    return this.http.get<any>(this.url + 'deliverychannel/' + issuer, this.httpoptions);
  }

  // method to get issuer privilege list
  getIssuerPrivilegeList() {
    return this.http.get<any>(this.url + 'privileges/', this.httpoptions);
  }


  // method to create an issuer administrator account
  createIssuer(formData: any) {

    if (formData.changePasswordAtFirstLogin === true) {
      formData.changePasswordAtFirstLogin = '1';
    } else {
      formData.changePasswordAtFirstLogin = '0';
    }

    if (formData.pwdNeverExpires === true) {
      formData.pwdNeverExpires = '1';
    } else {
      formData.pwdNeverExpires = '0';
    }

    if (formData.blkSimultLogin === true) {
      formData.blockksimultlogin = '1';
    } else {
      formData.blockksimultlogin = '0';
    }

    if (formData.updateProfileAtLogin === true) {
      formData.updateProfileAtLogin = '1';
    } else {
      formData.updateProfileAtLogin = '0';
    }
    if (formData.issuer == 0) {
      formData.issuer = '';
    }
    const body = {
      'adminUserId': formData.userid,
      'firstName': formData.firstName,
      'lastName': formData.lastName,
      'level': 3,
      'middleName': formData.middleName,
      'desciption': formData.description,
      'maxValForLastPwdList': formData.maxValForLastPwdList,
      'pasword': formData.password,
      'rePassword': formData.rePassword,
      'changePasswordAtFirstLogin': formData.changePasswordAtFirstLogin,
      'pwdNeverExpires': formData.pwdNeverExpires,
      'updateProfileAtLogin': formData.updateProfileAtLogin,
      'collectAdminContactDetails': formData.collectAdminContactDetails,
      'twoFactorDeliveryChannelType': formData.twoFactorDeliveryChannelType,
      'countries': null,
      // 'subProcessorName':null,
      'selectedAdminBank': formData.issuer,
      'selectedTemplateName': formData.Admins,
      'secAdminName': 'global',
      'secAdminName_decrypted': 'global',
      'multiControl': 'false',
      'blkSimultLogin': formData.blockksimultlogin,
      // 'blksimultLogin':formData.blockksimultlogin,
      'is2FAAuthenticationEnabled': '0',
      'twoFactorDelChnlType1': '0',
      'twoFactorAuthenticationType': formData.twoFactorAuthenticationType,
      'deliveryChannel': '0',
      'privs': formData.privs,
      'bank': 'i18n/en_US',
      'action': 'newUser'
    };
    if (formData.privs.length == 0) {
      delete body['privs'];
    }
    return this.http.post<any>(this.url, body, this.httpoptions);
  }
}
