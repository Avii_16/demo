import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class ResetIssuerAdminPasswordService {
  userUrl = environment.api_endpoint + '/vpas/api/issueradmin/';
  bankUrl = environment.api_endpoint + '/vpas/api/banks';
  resetUrl = environment.api_endpoint + '/vpas/api/admin/passwordreset';
  fetchUrl = environment.api_endpoint + '/vpas/api/issueradmin/';
  sessionData: any = JSON.parse(localStorage.getItem('sessionData'));
  token: any = this.sessionData.token;

  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.token
    })
  };


  getAllAdmins() {
    return this.http.get<any>(this.bankUrl, this.httpoptions);
  }
  // get list of all users
  getAllUsers(data: any) {
    return this.http.get<any>(this.userUrl + data, this.httpoptions);
  }

  // reset Issuer Administrator
  resetIssuerAdmin(data: any) {
    const body = {
      'loggedInAdminName': this.sessionData.adminName,
      'loggedInUserid': 'global',
      'selectedBank': data.issuer,
      'selectedAdmin': data.Admins,
      'password': data.newPassword,
      'remarks': data.remark,
      'level': 3,
      'action': 'updateUser',
      'reCheckPassword': data.retypeNewpassword,
      'changePwdAtLogin': 'true',
      'emailCheck': 'on',
      'phoneCheck': 'on',
      'locale': 'en_US',
      'loggedInBank': 'i18n/en_US',
      'adminNameForUpdate': 'fSNBKDmUNKojkN8dAFBKkr7G6AD4rLSq'
    };
    return this.http.put<any>(this.resetUrl, body, this.httpoptions);
  }
}
