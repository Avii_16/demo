import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class ModifyIssuerAdminService {

  url = environment.api_endpoint + '/vpas/api/';
  bankUrl = environment.api_endpoint + '/vpas/api/banks';

  token: any = JSON.parse(localStorage.getItem('sessionData')).token;

  // default constructor
  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
      'Content-Type': 'application/json'
    })
  };

  // method to get list of Admins
  getAllAdmins() {
    return this.http.get<any>(this.bankUrl, this.httpoptions);
  }

  // method to get all Users
  getAllUsers(data: any) {
    return this.http.get<any>(this.url + 'issueradmin/' + data, this.httpoptions);
  }

  // method to get administrator details
  getAdminDetails(adminData: any) {
    return this.http.get<any>(this.url + 'admin/' + adminData.adminName + '/' + adminData.bankId + '/status', this.httpoptions);
  }

  // method to modify global Administrator account
  modifyGlobalAdmin(data) {
    const body = {
      'selectedAdminName': data.Admins,
      'selectedAdminBank': data.issuer,
      'remark': data.remark,
      'level': '3',
      'action': 'updateUser',
      'status': data.status,
      'locale': 'en_US',
      'bank': 'i18n/en_US',
      'adminForUpdate': data.Admins
    };
    return this.http.put<any>(this.url + 'admin/status', body, this.httpoptions);
  }
}
