import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class UpdateIssuerAdminPrivilegesService {

  url = environment.api_endpoint + '/vpas/api/';
  token: any = JSON.parse(localStorage.getItem('sessionData')).token;

  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.token,
    })
  };

  // method to list all banks
  fetchAllBanks() {
    return this.http.get<any>(this.url + 'banks', this.httpoptions);
  }

  // method to list all admins
  fetchAllAdmins(data) {
    console.log(data);

    return this.http.get<any>(this.url + 'issueradmin/' + data, this.httpoptions);
  }

  // method to fetch admin details
  fetchAdminDetails(data: any) {
    const body = {
      'locale': 'en_US',
      'action': 'changeAdmin',
      'adminIdClear': data.selectedAdminName,
      'adminBankId': data.selectedBankId,
      'level': '3',
      'bank': 'i18n/en_US'
    };
    return this.http.put<any>(this.url + 'admin/privileges/', body, this.httpoptions);
  }

  // method to update issuer admin
  updateIssuerAdmin(data: any, bankId: any, adminName: any) {
    if (data.maxValForLastPwdList == 'None') {
      data.maxValForLastPwdList = '0';
    } else {
      data.maxValForLastPwdList = data.maxValForLastPwdList;
    }
    const body = {
      'locale': 'en_US',
      'action': 'saveAdmin',
      'adminIdClear': adminName,
      'adminBankId': bankId,
      'firstNameClear': data.firstNameClear,
      'middleNameClear': data.middleNameClear,
      'lastNameClear': data.lastNameClear,
      'description': data.description,
      'selectedBanks': ['23'],
      'blockkSimultaneousLogin': data.blockkSimultaneousLogin,
      'updateProfileAtLogin': data.updateProfileAtLogin,
      'maxValForLastPwdList': data.maxValForLastPwdList,
      'passwordType': true,
      'subProcessorName': '-1000',
      'processorName': '-1000',
      'twoFactorAuthenticationType': 1,
      // tslint:disable-next-line: radix
      'twoFactorDeliveryChannelType': parseInt(data.twoFactorDeliveryChannelType),
      'twoFactorAuthenticationEnabled': true,
      // tslint:disable-next-line: radix
      'collectAdminContactDetails': parseInt(data.collectAdminContactDetails),
      'passwordTypeChanged': true,
      'level': '3',
      'pwdNeverExpires': data.pwdNeverExpires,
      'bank': 'i18n/en_US',
      'privileges': data.privs,
      'templateIdClear': data.templateIdClear
    };
    return this.http.put<any>(this.url + 'admin/privileges/', body, this.httpoptions);
  }
}
