import { TestBed } from '@angular/core/testing';

import { ConfigureIssuerAdminPolicyService } from './configure-issuer-admin-policy.service';

describe('ConfigureIssuerAdminPolicyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigureIssuerAdminPolicyService = TestBed.get(ConfigureIssuerAdminPolicyService);
    expect(service).toBeTruthy();
  });
});
