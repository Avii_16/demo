import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class ConfigureIssuerAdminPolicyService {

  url = environment.api_endpoint + '/vpas/api/issueradmin/policy';
  bankListUrl = environment.api_endpoint + '/vpas/api/banks';
  sessionData: any = JSON.parse(localStorage.getItem('sessionData'));

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.sessionData.token
    })
  };

  // default constructor
  constructor(private http: HttpClient) { }

  // call to configure issuer admin
  configureIssuer(data: any) {
    let allowPasswordReset = '0';
    if (data.AllowPasswordReset == true) {
      allowPasswordReset = 'on';
    }
    const body = {
      'loggedInAdminName': this.sessionData.adminName,
      'locale': 'en_US',
      'loggedInBank': 'i18n/en_US',
      // 'adminNameForUpdate': data.issuer,
      // 'adminNameForUpdate': 'fSNBKDmUNKojkN8dAFBKkr7G6AD4rLSq',
      'level': '3',
      'selectedBank': data.issuer,
      'action': 'submit',
      'maxTriesPerSession': data.MaxTriesPerSession,
      'maxTriesAcrossSession': data.MaxTriesAcrossSession,
      'maxLength': data.MaxLength,
      'minLength': data.MinLength,
      'minAlphabet': data.MinAlphabet,
      'minNumeric': data.MinNumeric,
      'minSpecialCharacters': data.MinSpecialCharacters,
      'minLowerCaseCharacters': data.MinLowerCaseCharacters,
      'minUpperCaseCharacters': data.MinUpperCaseCharacters,
      'passwordRenewalFrequency': data.PasswordRenewalFrequency,
      'maxInactivityPeriod': data.MaxInactivityPeriod,
      'allowPasswordReset': allowPasswordReset,
      'passwordStorageAlgorithm': data.passwordStorageAlgorithm,
      'passwordChangeVelocity': data.passwordChangeVelocity,
      'loginSessionTimeout': data.loginSessionTimeout,
      'collectAdminContactDetails': data.collectAdminContactDetails
    };
    return this.http.put<any>(this.url, body, this.httpoptions);
  }

  // call to get all issuers list
  getAllIssuers() {
    return this.http.get<any>(this.bankListUrl, this.httpoptions);
  }

  fetchPolicyDetails(bankId) {
    return this.http.get<any>(this.url + '/' + 3 + '/' + bankId , this.httpoptions);
  }
}
