import { Component, OnInit } from '@angular/core';
import { ErrorMessagesService } from '../../../services/service_for_errorMessages/error-messages.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  expanded = true;
  navData: any = [];

  constructor(private service: ErrorMessagesService, private router: Router) {
    // override the route reuse strategy
    this.router.routeReuseStrategy.shouldReuseRoute = function () { return false; };
  }

  ngOnInit() {
    this.service.getNavbarDetails().subscribe((data) => {
      this.navData = data;
    });
  }

  imageChange(value) {
    const URL = value.src;
    const newURL = URL.replace(/^[a-z]{4,5}\:\/{2}[a-z]{1,}\:[0-9]{1,4}.(.*)/, '$1'); // http or https
    if (newURL == 'assets/minus.gif') {
      value.src = '../../assets/plus.gif';
    } else {
      value.src = '../../assets/minus.gif';
    }
  }
}
