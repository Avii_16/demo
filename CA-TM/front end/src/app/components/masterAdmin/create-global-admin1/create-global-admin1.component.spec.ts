import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGlobalAdmin1Component } from './create-global-admin1.component';

describe('CreateGlobalAdmin1Component', () => {
  let component: CreateGlobalAdmin1Component;
  let fixture: ComponentFixture<CreateGlobalAdmin1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateGlobalAdmin1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGlobalAdmin1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
