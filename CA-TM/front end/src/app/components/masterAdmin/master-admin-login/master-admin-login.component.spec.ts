import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterAdminLoginComponent } from './master-admin-login.component';

describe('MasterAdminLoginComponent', () => {
  let component: MasterAdminLoginComponent;
  let fixture: ComponentFixture<MasterAdminLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterAdminLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterAdminLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
