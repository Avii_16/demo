import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServiceService } from '../../../services/auth_service/auth-service.service';
import { ErrorMessagesService } from '../../../services/service_for_errorMessages/error-messages.service';
@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})
export class LoginpageComponent implements OnInit {
  loginForm: FormGroup;
  errorMessage: string;
  showPopup = true;
  sessionData: any;
  errorData: any = [];
  constructor(private formBuilder: FormBuilder, private service: AuthServiceService, private routes: Router, private errorService: ErrorMessagesService) {
    this.loginForm = this.formBuilder.group({
      adminName: [''],
      password: ['']
    });
  }
  ngOnInit() {
    this.errorService.getJSON().subscribe((data) => {
      this.errorData = data;
    });
  }

  doLogin() {
    localStorage.setItem('password', this.loginForm.get('password').value);
    localStorage.setItem('username', this.loginForm.get('adminName').value);
    this.service.doLogin(this.loginForm.value).subscribe((response) => {
      if (response.statusCode == 0) {
        // for (let i = 0; i < this.errorData.length; i++) {
        //   if (this.errorData[i].errorCode == response.errorList[0].errorMsg) {
        //     this.errorMessage = this.errorData[i].errorMessage;
        //     break;
        //   }
        // }
      } else if (response.statusCode == 1) {
        this.sessionData = response;
        localStorage.setItem('token', response.token);
        localStorage.setItem('entered', 'true');
        localStorage.setItem('name', response.firstName + '  ' + response.middleName + '  ' + response.lastName);
        localStorage.setItem('lastLogOnTime', response.lastLogOnTime);
        localStorage.setItem('path', window.location.href);
        localStorage.setItem('sessionData', JSON.stringify(this.sessionData));
        window.location.href = '/index';
      } else if (response.statusCode == 2) {
        this.sessionData = response;
        localStorage.setItem('token', response.token);
        localStorage.setItem('entered', 'true');
        localStorage.setItem('firstlogin', 'true');
        localStorage.setItem('lastLogOnTime', response.lastLogOnTime);
        localStorage.setItem('path', window.location.href);
        localStorage.setItem('name', response.firstName + '  ' + response.middleName + '  ' + response.lastName);
        localStorage.setItem('sessionData', JSON.stringify(this.sessionData));
        window.location.href = '/update_profile/first_login';
      } else if (response.statusCode == 3) {
        this.sessionData = response;
        localStorage.setItem('token', response.token);
        localStorage.setItem('entered', 'true');
        localStorage.setItem('lastLogOnTime', response.lastLogOnTime);
        localStorage.setItem('name', response.firstName + '  ' + response.middleName + '  ' + response.lastName);
        localStorage.setItem('path', window.location.href);
        localStorage.setItem('sessionData', JSON.stringify(this.sessionData));
        window.location.href = '/index';
      } else if (response.statusCode == 4) {
        this.sessionData = response;
        localStorage.setItem('token', response.token);
        localStorage.setItem('entered', 'true');
        localStorage.setItem('tempPasswordDuration', response.tempPasswordDuration);
        localStorage.setItem('lastLogOnTime', response.lastLogOnTime);
        localStorage.setItem('name', response.firstName + '  ' + response.middleName + '  ' + response.lastName);
        localStorage.setItem('path', window.location.href);
        localStorage.setItem('firstlogin', 'true');
        localStorage.setItem('sessionData', JSON.stringify(this.sessionData));
        window.location.href = '/change_password';
      }
    },
    (error) => {
      console.log(error);
      for (let i = 0; i < this.errorData.length; i++) {
        if (this.errorData[i].errorCode == error.error.errorList[0].errorMsg) {
          this.errorMessage = this.errorData[i].errorMessage;
          this.loginForm.reset({adminName: '', password: ''}, { onlySelf: true });
        }
      }
      // this.errorMessage = error.error.errorList[0].errorMsg;
    });
  }
  hidePopup() {
    this.showPopup = false;
  }
}
