import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ConfigureIssuerAdminPrivilegesService } from '../../../../services/issuer_admin_configuration_service/configure_issuer_admin_privileges/configure-issuer-admin-privileges.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-configure-issuer-admin-previleges',
  templateUrl: './configure-issuer-admin-previleges.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class ConfigureIssuerAdminPrevilegesComponent implements OnInit {
  configureIssuerAdminPrivilegesForm: FormGroup;
  privileges: any = [];
  privIds: any = [];
  privType: any = [];
  menuLocation: string;
  privIdsNegative: any = [];
  privTypeNegative: any = [];
  privEnable: any = [];
  privNumberOfAdmins: any = [];
  errorMessage: string;
  privsEnableType: any = [];
  errordata: any = [];
  constructor(private service: ConfigureIssuerAdminPrivilegesService, private router: Router, private formbuilder: FormBuilder) {
    this.configureIssuerAdminPrivilegesForm = this.formbuilder.group({
      privEnable: [''],
      privNumberOfAdmins: ['']
    });
  }

  ngOnInit() {
    this.fetchAllIssuerAdminData();
  }

  doCancel() {
    this.router.navigate(['/index']);
  }

  fetchAllIssuerAdminData() {
    // tslint:disable-next-line: max-line-length
    const array = ['System Configurations', 'Issuer Reports', 'Issuer Configurations', 'Global Admin Configurations', 'Issuer Admin Configurations', 'Admin Configurations', 'Cardholder Configurations', 'Callout Configurations', 'Enrollment Process Configurations', 'Registration Reports', 'Transaction Reports', 'Admin Reports', 'Cardholder Reports', 'Upload Configurations', 'Template Configurations', '', 'Case Management', '', '', '', '', 'External Requests', 'Tools'];
    this.service.getAllIssuerPrivilegeData().subscribe((response) => {
      this.privileges = response.response;
      for (let index = 0; index < this.privileges.length; index++) {
        const indexvalue = this.privileges[index].type;
        this.privileges[index].menuid = array[indexvalue - 1];
      }
      this.privilegeCheck();
      this.DualAdminCheck();
      for (let y = 0; y < this.privileges.length; y++) {
        if (this.privileges[y].type < 1) {
          this.configureIssuerAdminPrivilegesForm.addControl(y.toString(), new FormControl(false));
        } else {
          this.configureIssuerAdminPrivilegesForm.addControl(y.toString(), new FormControl(true));
        }
      }

      for (let y = 0; y < this.privileges.length; y++) {

        if (this.privileges[y].numberOfAdmins >= 2) {
          this.configureIssuerAdminPrivilegesForm.addControl(this.privileges[y].privilegeId, new FormControl(true));
        } else {
          this.configureIssuerAdminPrivilegesForm.addControl(this.privileges[y].privilegeId, new FormControl(false));
        }
      }
    });


  }

  privilegeCheck() {
    for (let index = 0; index < this.privileges.length; index++) {
      if (this.privileges[index].type < 1) {
        this.privileges[index].checked = false;
      } else {
        this.privileges[index].checked = true;
      }
    }
  }

  DualAdminCheck() {
    for (let index = 0; index < this.privileges.length; index++) {
      if (this.privileges[index].numberOfAdmins == 2) {
        this.privileges[index].dualChecked = true;
      } else {
        this.privileges[index].dualChecked = false;
      }
    }
  }


  configureIssuerAdminPrivileges() {
    this.privIds.length = 0;
    this.privType.length = 0;
    this.privEnable.length = 0;
    this.privNumberOfAdmins.length = 0;
    /////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * all privids
     */
    for (let index = 0; index < this.privileges.length; index++) {
      this.privIds.push(this.privileges[index].privilegeId);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////
    for (let i = 0; i < this.privileges.length; i++) {
      if (this.configureIssuerAdminPrivilegesForm.get(i.toString()).value == true) {
        if (this.privileges[i].type > 0) {
          this.privType.push(this.privileges[i].type);
        } else {
          const privtype = (-1 * this.privileges[i].type);
          this.privType.push(privtype);
        }
      } else {
        if (this.privileges[i].type < 0) {
          this.privType.push(this.privileges[i].type);
        } else {
          const privtype = (-1 * this.privileges[i].type);
          this.privType.push(privtype);
        }
      }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  dual controller
     */
    for (let i = 0; i < this.privileges.length; i++) {
      if (this.configureIssuerAdminPrivilegesForm.get(this.privileges[i].privilegeId).value == true) {
        let flag = 0;

          for (let y = 0; y < this.privEnable.length; y++) {
            if (this.privileges[i].privilegeId == this.privEnable[y]) {
              flag = 1;
            }
          }
          if (flag == 0) {
            this.privNumberOfAdmins.push(this.privileges[i].privilegeId);
          }

      }
      console.log(this.privNumberOfAdmins.length);
    }
    for (let i = 0; i < this.privileges.length; i++) {
      if (this.configureIssuerAdminPrivilegesForm.get(i.toString()).value == true) {
        this.privEnable.push(this.privileges[i].privilegeId);
      }
    }
    this.service.updatePrivileges(this.privIds, this.privType, this.privNumberOfAdmins, this.privEnable).subscribe((data) => {
      if (data) {
        this.errorMessage = data.response;
      } else if (data.statusCode == -1) {
        this.errorMessage = 'Error Saving Details';
      }
    },
      (error) => {
        console.log(error.errorList);
        this.errordata = error.errorList;
        console.log(this.errordata);
        this.errorMessage = this.errordata[0].errorMsg;
        console.log(this.errorMessage);

      }
    );
  }
}
