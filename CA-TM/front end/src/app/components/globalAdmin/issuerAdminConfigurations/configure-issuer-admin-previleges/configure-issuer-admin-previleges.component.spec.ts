import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureIssuerAdminPrevilegesComponent } from './configure-issuer-admin-previleges.component';

describe('ConfigureIssuerAdminPrevilegesComponent', () => {
  let component: ConfigureIssuerAdminPrevilegesComponent;
  let fixture: ComponentFixture<ConfigureIssuerAdminPrevilegesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureIssuerAdminPrevilegesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureIssuerAdminPrevilegesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
