import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfigureIssuerAdminPolicyService } from '../../../../services/issuer_admin_configuration_service/configure_issuer_admin_policy/configure-issuer-admin-policy.service';
import { ErrorMessagesService } from '../../../../services/service_for_errorMessages/error-messages.service';

@Component({
  selector: 'app-configure-issuer-admin-policy',
  templateUrl: './configure-issuer-admin-policy.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class ConfigureIssuerAdminPolicyComponent implements OnInit {

  issuerId = false;
  configureIssuerAdminForm: FormGroup;
  errorMessage: string;
  issuersList: any;
  dropDownNumbers: any = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25'];
  errorData: any = [];
  checked: boolean;
  constructor(private errorService: ErrorMessagesService, private formBuilder: FormBuilder, private router: Router , private service: ConfigureIssuerAdminPolicyService ) {
    this.errorService.getJSON().subscribe((data) => {
      this.errorData = data;
 });

    this.configureIssuerAdminForm = this.formBuilder.group({
      issuer: [''],
      MaxTriesPerSession: [''],
      MaxTriesAcrossSession: [''],
      MinLength: [''],
      MaxLength: [''],
      MinNumeric: [''],
      MinAlphabet: [''],
      MinSpecialCharacters: [''],
      MinUpperCaseCharacters: [''],
      MinLowerCaseCharacters: [''],
      PasswordRenewalFrequency: [''],
      MaxInactivityPeriod: [''],
      AllowPasswordReset: [''],
      loginSessionTimeout: [''],
      passwordStorageAlgorithm: [''],
      passwordChangeVelocity: [''],
      collectAdminContactDetails: [''],
      twoFactorDeliveryChannelType: ['']
    });
    this.configureIssuerAdminForm.controls['passwordStorageAlgorithm'].setValue('0', { onlySelf: true });
    this.configureIssuerAdminForm.controls['collectAdminContactDetails'].setValue('0', { onlySelf: true });
    this.configureIssuerAdminForm.controls['twoFactorDeliveryChannelType'].setValue('0', { onlySelf: true });
  }

  ngOnInit() {
    this.fetchAllIssuers();
  }

  changeIssuer(value: any) {
    if (value == 0) {
      this.issuerId = false;
      this.errorMessage = 'Invalid Issuer selected';
    } else {
      this.errorMessage = '';
      this.issuerId = true;
      this.getPolicydetails();
    }
  }

  doCancel() {
    this.router.navigate(['/index']);
  }

  fetchAllIssuers() {
    this.service.getAllIssuers().subscribe((issuers) => {
      this.issuersList = issuers.response;
      console.log(this.issuersList);
    });
  }
  getPolicydetails() {
    this.service.fetchPolicyDetails(this.configureIssuerAdminForm.get('issuer').value).subscribe((data) => {
      console.log(data);
      this.configureIssuerAdminForm.controls['MaxTriesPerSession'].setValue(data.response.MaxTriesPerSession.toString(), { onlySelf: true });
      this.configureIssuerAdminForm.controls['MaxTriesAcrossSession'].setValue(data.response.MaxTriesAcrossSession, { onlySelf: true });
      this.configureIssuerAdminForm.controls['MinLength'].setValue(data.response.MinLength, { onlySelf: true });
      this.configureIssuerAdminForm.controls['MaxLength'].setValue(data.response.MaxLength, { onlySelf: true });
      this.configureIssuerAdminForm.controls['MinNumeric'].setValue(data.response.MinNumeric.toString(), { onlySelf: true });
      this.configureIssuerAdminForm.controls['MinAlphabet'].setValue(data.response.MinAlphabet, { onlySelf: true });
      this.configureIssuerAdminForm.controls['MinSpecialCharacters'].setValue(data.response.MinSpecialCharacters, { onlySelf: true });
      this.configureIssuerAdminForm.controls['MinUpperCaseCharacters'].setValue(data.response.MinUpperCaseCharacters, { onlySelf: true });
      this.configureIssuerAdminForm.controls['MinLowerCaseCharacters'].setValue(data.response.MinLowerCaseCharacters, { onlySelf: true });
      this.configureIssuerAdminForm.controls['collectAdminContactDetails'].setValue(data.response.collectAdminContactDetails, { onlySelf: true });
      this.configureIssuerAdminForm.controls['twoFactorDeliveryChannelType'].setValue(data.response.twoFactorDeliveryChannelType, { onlySelf: true });
      {
        if (data.response.passwordStorageAlgorithm == 'SHA1') {
          data.response.passwordStorageAlgorithm = '0';
        } else if (data.response.passwordStorageAlgorithm == 'SHA2') {
          data.response.passwordStorageAlgorithm = '1';
        }
      }
      this.configureIssuerAdminForm.controls['passwordStorageAlgorithm'].setValue(data.response.passwordStorageAlgorithm, { onlySelf: true });
      this.configureIssuerAdminForm.patchValue({

        PasswordRenewalFrequency: data.response.PasswordRenewalFrequency/86400000,
        MaxInactivityPeriod: data.response.MaxInactivityPeriod/86400000,
        loginSessionTimeout: data.response.loginSessionTimeout,
        passwordChangeVelocity: data.response.passwordChangeVelocity,
      });

      if (data.response.AllowPasswordReset == 0) {
        this.checked = false;
      } else {
        this.checked = true;
      }
    });
  }

  configureIssuer() {
    const confirm = window.confirm('Password Policy is weaker than default password policy. Do you want to continue ?');
    if (confirm == true) {
    this.service.configureIssuer(this.configureIssuerAdminForm.value).subscribe((response) => {
      if (response) {
        this.errorMessage = response.message ;
      } else if (response.statusCode == -1) {
        this.errorMessage = response.response;
      } else if (response.response == 'Invalid User !!!') {
        alert('Session Expired');
        window.location.href = '/';
      } else {
        this.errorMessage = 'One or more input was an invalid number';
      }
    }
    , (error) => {
            console.log(error);
            this.errorMessage = error.error.errorList[0].errorMsg;
          }

    );
  }
}

}
