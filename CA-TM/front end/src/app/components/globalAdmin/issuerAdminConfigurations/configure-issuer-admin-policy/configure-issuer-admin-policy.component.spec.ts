import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureIssuerAdminPolicyComponent } from './configure-issuer-admin-policy.component';

describe('ConfigureIssuerAdminPolicyComponent', () => {
  let component: ConfigureIssuerAdminPolicyComponent;
  let fixture: ComponentFixture<ConfigureIssuerAdminPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureIssuerAdminPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureIssuerAdminPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
