import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ResetIssuerAdminPasswordService } from '../../../../services/issuer_admin_configuration_service/reset_issuer_admin_password/reset-issuer-admin-password.service';
import { ErrorMessagesService } from '../../../../services/service_for_errorMessages/error-messages.service';

@Component({
  selector: 'app-reset-issuer-admin-password',
  templateUrl: './reset-issuer-admin-password.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class ResetIssuerAdminPasswordComponent implements OnInit {
  resetIssuerAdminPassword: FormGroup;
  adminSelection = false;
  admindetails = false;
  admins: any;
  users: any;
  bankcode: any;
  errorMessage: string;
  errorData: any;
  issuerdata: any = [];
  collectDeatils : boolean;
  showHeader = true;
  // tslint:disable-next-line: no-inferrable-types
  description: string = 'Select a User ID and click ';
  statusInactive = true;
  showContactMessage = false;

  constructor(private formbuilder: FormBuilder, private router: Router, private service: ResetIssuerAdminPasswordService, private errorService: ErrorMessagesService) {

    this.errorService.getJSON().subscribe((data) => {
      this.errorData = data;
    });

    this.resetIssuerAdminPassword = this.formbuilder.group({
      newPassword: [''],
      retypeNewpassword: [''],
      remark: [''],
      issuer: [''],
      Admins: [''],
      changePwdAtLogin: [''],
      emailCheck: [''],
      emailAddr: [''],
      phoneCheck: [''],
      phoneNum: ['']
    });

    this.resetIssuerAdminPassword.controls['Admins'].setValue('1',
      { onlySelf: true });
    this.resetIssuerAdminPassword.controls['issuer'].setValue('0',
      { onlySelf: true });
  }

  ngOnInit() {
    this.fetchAllAdmins();
  }

  onChangeIssuer(val: any) {
    if (val == 0) {
      this.adminSelection = false;
      this.admindetails = false;
      this.errorMessage = 'Invalid admin selected.';
    } else {
      this.adminSelection = true;
    }
    this.bankcode = this.resetIssuerAdminPassword.get('issuer').value;
    this.fetchAllUsers(this.bankcode);
  }

  onChangeAdmins(val: any) {
    if (val == 0) {
      this.admindetails = false;
      this.errorMessage = 'Invalid admin selected.';
    } else {
      const selectedAdmin = this.resetIssuerAdminPassword.get('Admins').value;
      this.resetIssuerAdminPassword.controls['Admins'].setValue(selectedAdmin, { onlySelf: true });
      for (let i = 0; i < this.users.length; i++) {
        if (selectedAdmin == this.users[i].name) {
          if (this.users[i].status == 2) {
            this.statusInactive = false;
          } else if (this.users[i].resetType == 0 && this.users[i].collectAdminDetails == 0) {
            this.admindetails = true;
            this.collectDeatils = false;
          } 
          else {
            this.admindetails = true;
            this.collectDeatils = true;
          }
        } 
        else {
          this.admindetails = true;
        }
      }
      this.admindetails = true;
    }
  }

  doCancel() {
    this.router.navigate(['/index']);
  }

  fetchAllAdmins() {
    this.service.getAllAdmins().subscribe((data) => {
      this.admins = data.response;
    });
  }
  fetchAllUsers(bankid) {
    this.service.getAllUsers(bankid).subscribe((data) => {
      this.users = data.response;
      if (this.users.length == 0) {
          this.errorMessage = 'No Admin(s) found for this admin';
        this.description = 'Select a User ID and click ';
        this.adminSelection = false;
        this.admindetails =  false;
      } else {
        this.adminSelection = true;
        this.errorMessage = ' ';
        this.issuerdata = data.response;
        this.description = 'Select a User ID, reset the password and click ' ;
      }
    });
  }
  ResetIssuerAdminPassword() {
    if (this.collectDeatils == true) {
      this.admindetails = false;
      this.collectDeatils = false;
      this.showContactMessage = true;
      this.showHeader = false;
    } else {
    this.service.resetIssuerAdmin(this.resetIssuerAdminPassword.value).subscribe((data) => {
      if (data.response.status != 1) {
        // this.showPage = true;
        // this.message = "You can not reset the Administrator's password because status is inactive";
      }
      if (data.statusCode == 1) {
        this.errorMessage = 'Reset Admin Password Successful';
      }
    }, (error) => {
      console.log(error);
      this.errorMessage = error.error.errorList[0].errorMsg;
    }
    );
  }
}
}
