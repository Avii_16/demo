import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetIssuerAdminPasswordComponent } from './reset-issuer-admin-password.component';

describe('ResetIssuerAdminPasswordComponent', () => {
  let component: ResetIssuerAdminPasswordComponent;
  let fixture: ComponentFixture<ResetIssuerAdminPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetIssuerAdminPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetIssuerAdminPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
