import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ModifyIssuerAdminService } from '../../../../services/issuer_admin_configuration_service/modify_issuer_admin/modify-issuer-admin.service';
import { ErrorMessagesService } from '../../../../services/service_for_errorMessages/error-messages.service';

@Component({
  selector: 'app-modify-issuer-admin-account',
  templateUrl: './modify-issuer-admin-account.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class ModifyIssuerAdminAccountComponent implements OnInit {
  modifyIssuerForm: FormGroup;
  adminId = false;
  issuerId = false;
  admins: any;
  users: any;
  bankcode: any;
  errorMessage: any;
  adminData: any;
  issuerdata: any = [];
  errorData: any = [];
  constructor(private errorService: ErrorMessagesService, private formbuilder: FormBuilder, private router: Router, private service: ModifyIssuerAdminService) {
    this.errorService.getJSON().subscribe((data) => {
      this.errorData = data;
    });

    this.modifyIssuerForm = this.formbuilder.group({
      issuer: [''],
      Admins: [''],
      remark: [''],
      status: ['']
    });
    this.modifyIssuerForm.controls['issuer'].setValue('0', { onlySelf: true });
  }

  ngOnInit() {
    this.fetchAllAdmins();
  }

  changeIssuer(value: any) {
    if (value == '0') {
      this.issuerId = false;
      this.errorMessage = 'No Issuer found for this Admin';
      this.modifyIssuerForm.controls['Admins'].setValue('0', { onlySelf: true });
      this.modifyIssuerForm.controls['status'].setValue('', { onlySelf: true });
      this.modifyIssuerForm.get('remark').patchValue('');
    } else {
      this.issuerId = true;
      this.modifyIssuerForm.controls['Admins'].setValue('0', { onlySelf: true });
      this.errorMessage = '';
    }
    this.bankcode = this.modifyIssuerForm.get('issuer').value;
    this.fetchAllUsers(this.bankcode);
  }
  changeAdmin(value: any) {
    if (value == 0) {
      this.adminId = false;
    } else {
      this.adminId = true;
      this.fetchAdminDetails();
    }
  }

  doCancel() {
    this.router.navigate(['/index']);
  }

  fetchAllAdmins() {
    this.service.getAllAdmins().subscribe((data) => {
      this.admins = data.response;
    });
  }

  fetchAllUsers(bankid) {
    this.service.getAllUsers(bankid).subscribe((data) => {
      this.users = data.response;
      if (this.users.length == 0) {
        this.errorMessage = 'No Admin(s) found for this admin';
        this.issuerId = false;
      } else {
        this.issuerId = true;
        this.issuerdata = data.response;
        this.errorMessage = ' ';
      }
    });
  }

  fetchAdminDetails() {
    const body = {
      adminName: this.modifyIssuerForm.get('Admins').value,
      bankId: this.modifyIssuerForm.get('issuer').value
    };
    this.service.getAdminDetails(body).subscribe((data) => {
      console.log(data);
      if (data.statusCode == 1) {
        this.adminData = data.response;
        if (this.adminData.status == 0) {
          this.modifyIssuerForm.controls['status'].setValue('2', { onlySelf: true });
        } else {
          this.modifyIssuerForm.controls['status'].setValue(this.adminData.status.toString(), { onlySelf: true });
          this.modifyIssuerForm.patchValue({
            remark: this.adminData.remarks
          });
        }
      }
    });
  }

  deleteConfirmation() {
    if (this.modifyIssuerForm.get('status').value == '3') {
      const message = 'Are you sure do you want to remove ' + this.modifyIssuerForm.get('Admins').value;
      const confirm = window.confirm(message);
      if (confirm == true) {
        this.modifyIssuerAdmin();
      }
    } else {
      this.modifyIssuerAdmin();
    }
  }

  modifyIssuerAdmin() {
    this.service.modifyGlobalAdmin(this.modifyIssuerForm.value).subscribe((data) => {
      this.errorMessage = data.message;
    }, (error) => {
      for (let i = 0; i < this.errorData.length; i++) {
        if (this.errorData[i].errorCode == error.error.errorList[0].errorMsg) {
          this.errorMessage = this.errorData[i].errorMessage;
          break;
        }
      }
    }

    );
  }
}
