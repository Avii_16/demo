import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { UpdateIssuerAdminPrivilegesService } from '../../../../services/issuer_admin_configuration_service/update_issuer_admin_privileges/update-issuer-admin-privileges.service';
import { ErrorMessagesService } from '../../../../services/service_for_errorMessages/error-messages.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-issuer-admin-previleges',
  templateUrl: './update-issuer-admin-previleges.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class UpdateIssuerAdminPrevilegesComponent implements OnInit {
  updateIssuerAdminPrivilegesForm: FormGroup;
  issuerSelected = false;
  adminSelected = false;
  banks: any;
  bankId: any;
  admins: any;
  adminName: any;
  adminData: any;
  privilegesData: any = [];
  privilegeChecked: any;
  isPrivilege: boolean;
  checkAllPrivileges: boolean;
  privilegesSize: any;
  privilegesCheck: any = [];
  checkedprivileges: any = [];
  checked: any;
  issuerPrivileges: any = [];
  privilegesList: any = [];
  matchedPrivileges: boolean;
  errorData: any;
  errorMessage: string;
  _values2 = [];
  adminId = false;
  templateId: boolean;

  _values1 = [
    { id: 0, name: 'Do not collect' },
    { id: 1, name: 'Only Email Address' },
    { id: 2, name: 'Only Phone Number' },
    { id: 3, name: 'Both Email Address and Phone Number' }
  ];

  constructor(private formbuilder: FormBuilder, private router: Router, private service: UpdateIssuerAdminPrivilegesService, private errorService: ErrorMessagesService ) {
    this.errorService.getJSON().subscribe((data) => {
      this.errorData = data;
    });

    this.updateIssuerAdminPrivilegesForm = this.formbuilder.group({
      issuer: [''],
      Admins: [''],

      firstNameClear: [''],
      middleNameClear: [''],
      lastNameClear: [''],
      description: [''],
      pwdNeverExpires: [''],
      templateIdClear: [''],
      blockkSimultaneousLogin: [''],
      twoFactorDeliveryChannelType: [''],
      updateProfileAtLogin: [''],
      maxValForLastPwdList: [''],
      collectAdminContactDetails: [''],
      privs: ['']
    });
    this.updateIssuerAdminPrivilegesForm.controls['issuer'].setValue('None', { onlySelf: true });
    this.updateIssuerAdminPrivilegesForm.controls['Admins'].setValue('None', { onlySelf: true });
  }

  ngOnInit() {
    this.getAllBanks();
  }

  doCancel() {
    this.router.navigate(['/index']);
  }

  changeTemplate(value: any) {
    if (value == 0) {
      this.templateId = false;
    } else {
      this.templateId = true;
    }
  }

  showDeliveryChannel(val: any) {
    const obj = this._values1[val];
    if (obj.id == 0) {
      this.adminId = false;
    } else {
      this.adminId = true;
    }
  }

  firstDropDownChanged(val: any) {
    const obj = this._values1[val];
    if (!obj) { return; }
    if (obj.id == 1) {
      this._values2 = [
        { id: 2, name: 'EMAIL' }
      ];
    } else if (obj.id == 2) {
      this._values2 = [
        { id: 1, name: 'SMS' },
        { id: 3, name: 'VOICE' }
      ];
    } else if (obj.id == 3) {
      this._values2 = [
        { id: 4, name: 'EMAIL and SMS' },
        { id: 1, name: 'SMS' },
        { id: 2, name: 'EMAIL' },
        { id: 3, name: 'VOICE' },
        { id: 5, name: 'EMAIL AND VOICE' }
      ];
    } else {
      this._values2 = [];
    }
  }



  getAllBanks() {
    this.service.fetchAllBanks().subscribe((data) => {
      this.banks = data.response;
    });
  }
  onChangeIssuer(value: any) {
    this.updateIssuerAdminPrivilegesForm.controls['Admins'].setValue('None', { onlySelf: true });
    this.adminSelected = false;
    this.issuerSelected = false;
    if (value == 0) {
      this.issuerSelected = false;
    } else {
      this.issuerSelected = true;
    }
    this.bankId = this.updateIssuerAdminPrivilegesForm.get('issuer').value;
    this.getAllAdmins(this.bankId);
  }
  getAllAdmins(bankId) {
    this.service.fetchAllAdmins(bankId).subscribe((data) => {
      if (data.statusCode == 1) {
        this.admins = data.response;
      } else {
        this.adminSelected = false;
        this.issuerSelected = false;
      }
    });
  }
  onChangeAdmins(value: any) {
    if (value == 0) {
      this.adminSelected = false;
    } else {
      this.adminSelected = true;
    }
    this.adminName = this.updateIssuerAdminPrivilegesForm.get('Admins').value;
    this.getAdminDetails();
  }

  allPrivileges(event) {
    const checked = event.target.checked;
    const privileges = {};
    for (let index = 0; index < this.privilegesData.length; index++) {
      this.privilegesData[index].checked1 = checked;
    }
    for (let i = 0; i < this.privilegesSize; i++) {
      if (this.updateIssuerAdminPrivilegesForm.get('privs').value == true) {
        const property = this.privilegesData[i].privilegeId;
        privileges[property] = true;
      } else {
        const property = this.privilegesData[i].privilegeId;
        privileges[property] = false;
      }
    }
    this.updateIssuerAdminPrivilegesForm.patchValue(privileges);
  }

  changePrivilegesByCategory(event) {
    if (event.target.name == 'privileges') {
      this.isPrivilege = true;
    }
    if (this.isPrivilege && this.checkAllPrivileges) {
      event.target.checked = true;
    }
  }

  getAdminDetails() {
    const adminDetails = {
      selectedBankId: this.bankId,
      selectedAdminName: this.adminName
    };

    this.service.fetchAdminDetails(adminDetails).subscribe((data) => {
      if (data.statusCode == 2) {
        this.adminData = data;
        for (let i = 0; i < data.loggedInAdminsPrivileges.length; i++) {
          if ( data.loggedInAdminsPrivileges[i].type > 0 ) {
            this.privilegesData.push(data.loggedInAdminsPrivileges[i]);
          }
        }
        console.log(this.privilegesData);
        
        this.privilegesSize = this.privilegesData.length;
        this.issuerPrivileges = data.selectedAdmin.privilegeIDList;
        const checkedPrivilegesList = [];
        if (this.privilegesSize > 0) {
          for (let remove = 0; remove < this.privilegesSize; remove++) {
            this.updateIssuerAdminPrivilegesForm.removeControl(this.privilegesData[remove].privilegeId);
          }
        }
        for (let j = 0; j < this.privilegesData.length; j++) {
          let flag = 0;
          for (let i = 0; i < this.issuerPrivileges.length; i++) {
            if (this.privilegesData[j].privilegeId == this.issuerPrivileges[i]) {
              this.privilegesData[j].checked1 = true;
              flag = 1;
            }
          }
          if (flag == 0) {
            this.privilegesData[j].checked1 = false;
          }
        }

        // updataedPrivilegesList1= this.getUnique(updataedPrivilegesList1);



        for (let i = 0; i < this.privilegesSize; i++) {
          this.updateIssuerAdminPrivilegesForm.addControl(this.privilegesData[i].privilegeId.toString(), new FormControl(this.privilegesData[i].checked1));
        }
        for (let i = 0; i < this.privilegesSize; i++) {
          this.privilegesData[i].checked = this.privilegesCheck[i];
        }
        this.updateIssuerAdminPrivilegesForm.patchValue({
          pwdNeverExpires: this.adminData.adminInput.pwdNeverExpires,
          firstNameClear: this.adminData.adminInput.firstNameClear,
          middleNameClear: this.adminData.adminInput.middleNameClear,
          lastNameClear: this.adminData.adminInput.lastNameClear,
          description: this.adminData.adminInput.description,
          maxValForLastPwdList: this.adminData.adminInput.maxValForLastPwdList,
          blockkSimultaneousLogin: this.adminData.adminInput.blockkSimultaneousLogin,
          updateProfileAtLogin: this.adminData.adminInput.updateProfileAtLogin,
          collectAdminContactDetails:this.adminData.adminInput.collectAdminContactDetails,
          twoFactorDeliveryChannelType: this.adminData.adminInput.twoFactorDeliveryChannelType
        });
        this.updateIssuerAdminPrivilegesForm.controls['maxValForLastPwdList'].setValue('None', { onlySelf: true });

        if (this.adminData.adminInput.templateIdClear == null) {
          this.updateIssuerAdminPrivilegesForm.controls['templateIdClear'].setValue('-1000', { onlySelf: true });
        } else {
          this.updateIssuerAdminPrivilegesForm.controls['templateIdClear'].setValue(this.adminData.adminInput.templateIdClear, { onlySelf: true });
        }
        if (this.adminData.adminInput.collectAdminContactDetails != 0) {
          this.updateIssuerAdminPrivilegesForm.controls['collectAdminContactDetails'].setValue(this.adminData.adminInput.collectAdminContactDetails, { onlySelf: true });
          this.adminId = true;
          this.firstDropDownChanged(this.adminData.adminInput.collectAdminContactDetails);
          this.updateIssuerAdminPrivilegesForm.controls['twoFactorDeliveryChannelType'].setValue(this.adminData.adminInput.twoFactorDeliveryChannelType, { onlySelf: true });
        } else {
          this.updateIssuerAdminPrivilegesForm.controls['collectAdminContactDetails'].setValue(0, { onlySelf: true });
          this.adminId = false;
          this.templateId = false;
        }
        // this.updateIssuerAdminPrivilegesForm.controls['collectAdminContactDetails'].setValue(this.adminData.adminInput.collectAdminContactDetails, { onlySelf: true });

      } else {
        this.errorMessage = 'invalid Admin Selected.';
        this.adminSelected = false;
      }
    });
  }

  getUnique(array) {
    const uniqueArray = [];
    // Loop through array values
    for (let i = 0; i < array.length; i++) {
      if (uniqueArray.indexOf(array[i].privilegeId) === -1) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  }

  updateIssuerAdminPrivileges() {

    const updatedPrivileges = [];
    if (this.updateIssuerAdminPrivilegesForm.get('privs').value == false) {
      for (let i = 0; i < this.privilegesSize; i++) {
        if (this.updateIssuerAdminPrivilegesForm.get(this.privilegesData[i].privilegeId).value == true) {
          updatedPrivileges.push(this.privilegesData[i].privilegeId);
        }
      }
    } else {
      for (let i = 0; i < this.privilegesSize; i++) {
        updatedPrivileges.push(this.privilegesData[i].privilegeId);
      }
    }


    this.updateIssuerAdminPrivilegesForm.patchValue({
      privs: updatedPrivileges
    });


    this.service.updateIssuerAdmin(this.updateIssuerAdminPrivilegesForm.value, this.bankId, this.adminName).subscribe((data) => {
      console.log(this.updateIssuerAdminPrivilegesForm.get('collectAdminContactDetails').value);
      console.log(this.updateIssuerAdminPrivilegesForm.get('twoFactorDeliveryChannelType').value);
      if (data.statusCode == 2) {
        this.errorMessage = data.response;
      }
    }, (error) => {
      this.errorMessage = error.error.errorList[0].errorMsg;
      console.log(this.errorMessage);
    });
  }
}
