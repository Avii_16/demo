import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateIssuerAdminPrevilegesComponent } from './update-issuer-admin-previleges.component';

describe('UpdateIssuerAdminPrevilegesComponent', () => {
  let component: UpdateIssuerAdminPrevilegesComponent;
  let fixture: ComponentFixture<UpdateIssuerAdminPrevilegesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateIssuerAdminPrevilegesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateIssuerAdminPrevilegesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
