import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalAdminService } from '../../../../services/global_admin_service/create_global_admin/global-admin.service';
import { CreateIssuerAdminService } from '../../../../services/issuer_admin_configuration_service/create_issuer_admin/create-issuer-admin.service';
import { ErrorMessagesService } from '../../../../services/service_for_errorMessages/error-messages.service';

@Component({
  selector: 'app-create-issuer-admin',
  templateUrl: './create-issuer-admin.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class CreateIssuerAdminComponent implements OnInit {
  bankid: any;
  issuerId = false;
  templateId = false;
  default: any = 0;
  defaultcollectAdminContactDetails: any ;
  twoFactorDeliveryChannelType: any;
  privileges: any = [];
  isPrivilege = false;
  checkAllPrivileges = false;
  checked = true;
  errorMessage: string;
  issuers: any;
  numbers: any = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13'];
  createIssuerAdminForm: FormGroup;
  errorData: any = [];
  hideTemplates = false;
  passwordMessage = false;
  issuer: any;
  admindetails: any;
  showDeliveryChannel = false;
  deliveryChannel: any;
  contactId: any;
  deliveryId: any;
  constructor(private errorService: ErrorMessagesService, private formBuilder: FormBuilder, private router: Router, private service: GlobalAdminService, private createIssuerService: CreateIssuerAdminService) {
    this.errorService.getJSON().subscribe((data) => {
      this.errorData = data;
    });
    this.createIssuerAdminForm =
      this.formBuilder.group({
        Admins: [''],
        issuer: [''],
        userid: [''],
        firstName: [''],
        middleName: [''],
        lastName: [''],
        description: [''],
        password: [''],
        rePassword: [''],
        changePasswordAtFirstLogin: [''],
        pwdNeverExpires: [''],
        blksimultlogin: [''],
        updateProfileAtLogin: [''],
        twoFactorAuthenticationType: [''],
        maxValForLastPwdList: [''],
        collectAdminContactDetails: [''],
        privs: [''],
        twoFactorDeliveryChannelType:['']
      });
    this.createIssuerAdminForm.controls['Admins'].setValue('None', { onlySelf: true });
    this.createIssuerAdminForm.controls['maxValForLastPwdList'].setValue('0', { onlySelf: true });
  }
  ngOnInit() {
    this.fetchIssuers();
    this.fetchIssuerPrevileges();
    for (let i = 0; i < 143; i++) {
      this.createIssuerAdminForm.addControl(i.toString(), new FormControl(''));
    }
  }

  changeIssuer(value: any) {
    this.errorMessage = '';
    this.passwordMessage = false;
    if (value == 0) {
      this.hideTemplates = true;
      this.issuerId = false;
      this.errorMessage = 'Invalid Issuer selected';
    } else {
      this.issuerId = true;
      this.errorMessage = '';
      // this.createIssuerAdminForm.get('collectAdminContactDetails').disable();
    }
    this.issuer = this.createIssuerAdminForm.get('issuer').value;
    this.fetchadminContactDetails(this.issuer);
  }

  fetchadminContactDetails(issuer) {
    this.createIssuerService.getAdminContactDetails(issuer).subscribe((data) => {
      this.contactId = data.response.ContactDetailsPolicy;
      // this.defaultcollectAdminContactDetails = this.contactId;
      this.admindetails = this.getAdminDetails(this.contactId);
      this.createIssuerAdminForm.controls['collectAdminContactDetails'].setValue(this.contactId, { onlySelf: true });
      console.log(this.contactId, this.admindetails);
      if (this.contactId == 0) {
        this.showDeliveryChannel = false;
      } else {
        this.showDeliveryChannel = true;
        this.deliveryId = data.response.DeliveryChannel;
        // this.twoFactorDeliveryChannelType = this.deliveryId;
        this.deliveryChannel = this.getDeliveryChannelDetails(this.contactId, this.deliveryId);
        console.log( this.deliveryId,this.deliveryChannel);
        this.createIssuerAdminForm.controls['twoFactorDeliveryChannelType'].setValue(this.deliveryId, { onlySelf: true });
      }
    });
  }

  getAdminDetails(adminId) {
    let adminName;
    const adminDetails = [
      { id: 0, name: 'Do not collect' },
      { id: 1, name: 'Only Email Address' },
      { id: 2, name: 'Only Phone Number' },
      { id: 3, name: 'Both Email Address and Phone Number' }
    ];
    for (let i = 0; i < adminDetails.length; i++) {
      if (adminId == adminDetails[i].id) {
        adminName = adminDetails[i].name;
      }
    }
    return adminName;
  }

  getDeliveryChannelDetails(adminId, contactId) {
    let deliveryName;
    if (adminId == 1) {
      const deliveryDetails = [
        { id: 2, name: 'EMAIL' }
      ];
      if (contactId == 2) {
        deliveryName = deliveryDetails[0].name;
      }
    } else if ( adminId == 2) {
      const deliveryDetails = [
        { id: 1, name: 'SMS' },
        { id: 3, name: 'VOICE'}
      ];
      for (let i = 0; i < deliveryDetails.length; i++) {
        if (contactId == deliveryDetails[i].id) {
          deliveryName = deliveryDetails[i].name;
        }
      }
    } else if ( adminId == 3) {
      const deliveryDetails = [
        { id: 4, name: 'EMAIL and SMS' },
        { id: 1, name: 'SMS' },
        { id: 2, name: 'EMAIL' },
        { id: 3, name: 'VOICE' },
        { id: 5, name: 'EMAIL AND VOICE' }
      ];
      for (let i = 0; i < deliveryDetails.length; i++) {
        if (contactId == deliveryDetails[i].id) {
          deliveryName = deliveryDetails[i].name;
        }
      }
    }
    return deliveryName;
  }

  ChangeTemplate(value: any) {
    if (value == 0) {
      this.templateId = false;
    } else {
      this.templateId = true;
    }
  }

  allPrivileges(event) {
    const checked = event.target.checked;
    this.checked = checked;
  }

  changePrivilegesByCategory(event) {

    if (event.target.name == 'privileges') {
      this.isPrivilege = true;
    }
    if (this.isPrivilege && this.checkAllPrivileges) {
      event.target.checked = true;
    }
  }

  fetchIssuers() {
    this.service.getAllIssuers().subscribe((response) => {
      console.log(response);
      this.issuers = response.response;
    });
  }

  doCancel() {
    this.router.navigate(['/index']);
  }

  fetchIssuerPrevileges() {
    this.createIssuerService.getIssuerPrivilegeList().subscribe((data) => {
      console.log(data);
      for (let index = 0; index < data.response.length ; index++) {
        if (data.response[index].type > 0) {
          this.privileges.push(data.response[index]);
        }
      }
      console.log(this.privileges);
    });
  }

  createIssuer() {

    let privilegesList = [];
    for (let i = 0; i < this.privileges.length; i++) {
      const y = i.toString();

      if (this.createIssuerAdminForm.get(y).value == true) {
        privilegesList.push(this.privileges[i].privilegeId);
      }
    }

    if (this.createIssuerAdminForm.get('privs').value == true) {
      privilegesList.length = 0;
      privilegesList = this.privileges;
    }
    if (this.createIssuerAdminForm.get('privs').touched == false) {
      for (let i = 0; i < this.privileges.length; i++) {
        privilegesList.push(this.privileges[i].privilegeId);
      }
    }


    this.createIssuerAdminForm.patchValue({
      privs: privilegesList,
    });
    this.createIssuerService.createIssuer(this.createIssuerAdminForm.value).subscribe((response) => {
      this.passwordMessage = false;
      if (response.statusCode == 1) {
        this.errorMessage = 'Created Admin successfully';
      }
    }, (error) => {
      if (error.error.response == null) {
        this.passwordMessage = false;
        this.errorMessage = error.error.errorList[0].errorMsg;
      } else if (error.error.response != null) {
        this.passwordMessage = true;
        this.errorMessage = error.error.response;
      }
    }
    );
  }
}
