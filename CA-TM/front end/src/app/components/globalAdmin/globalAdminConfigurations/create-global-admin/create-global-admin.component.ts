import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { GlobalAdminService } from '../../../../services/global_admin_service/create_global_admin/global-admin.service';
import { ErrorMessagesService } from '../../../../services/service_for_errorMessages/error-messages.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-global-admin',
  templateUrl: './create-global-admin.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})

export class CreateGlobalAdminComponent implements OnInit {
  createGlobalAdminForm: FormGroup;
  passwordMessage = false;
  $: any;
  selectedCountries: any = [];
  selectedRemoveCountries: any = [];
  selectedIssuers: any = [];
  selectedRemoveIssuers: any = [];
  templateId = false;
  countries: any;
  issuers: any;
  selectedbanks: any = [];
  countrycode: any = [];
  issuerCode: any = [];
  adminId = false;
  _values2 = [];
  templateDefaultValue: any = 1;
  privileges: any = [];
  isPrivilege = false;
  checkAllPrivileges = false;
  checked1 = true;
  checked2 = true;
  errorMessage: string;
  errorData: any;
  globalPrivileges: any = [];
  issuerPrivileges: any = [];
  templates: any = [];
  defaultValue: boolean = false;
  defaultValue1: boolean = false;
  _values1 = [
    { id: 0, name: 'Do not collect' },
    { id: 1, name: 'Only Email Address' },
    { id: 2, name: 'Only Phone Number' },
    { id: 3, name: 'Both Email Address and Phone Number' }
  ];

  constructor(private formBuilder: FormBuilder, private service:
    GlobalAdminService, private errorService: ErrorMessagesService, private router: Router) {

    this.errorService.getJSON().subscribe((data) => {
      this.errorData = data;
    });

    this.createGlobalAdminForm = this.formBuilder.group({
      userid: [''],
      firstName: [''],
      middleName: [''],
      lastName: [''],
      description: [''],
      Admins: ['',],
      password: [''],
      rePassword: [''],
      changePasswordAtFirstLogin: [''],
      pwdNeverExpires: [''],
      blksimultlogin: [''],
      updateProfileAtLogin: [''],
      maxValForLastPwdList: [''],
      collectAdminContactDetails: [''],
      twoFactorDeliveryChannelType: [''],
      twoFactorAuthenticationType: [''],
      processorName: [''],
      subProcessorName: [''],
      slt_Countries: [''],
      slt_selCountries: [''],
      slt_Issuers: [''],
      slt_selIssuers: [''],
      privs: [''],
      selectedTemplateName: [''],
      issuerPrivs: [''],
      globalPrivs: ['']

    });
    this.createGlobalAdminForm.controls['Admins'].setValue('None', {
      onlySelf: true
    });
    this.createGlobalAdminForm.controls['collectAdminContactDetails'].setValue(0,
      { onlySelf: true });
    this.createGlobalAdminForm.controls['processorName'].setValue('none',
      { onlySelf: true });
    this.createGlobalAdminForm.controls['subProcessorName'].setValue('none',
      { onlySelf: true });
    this.createGlobalAdminForm.controls['maxValForLastPwdList'].setValue('0',
      { onlySelf: true });
    this.createGlobalAdminForm.controls['selectedTemplateName'].setValue('None',
      { onlySelf: true });

  }

  ngOnInit() {
    // if (performance.navigation.type == 1){
    //   window.location.href = "/index";
    // }
    this.getAllCountries();
    this.getAllIssuers();
    this.fetchAllPrivileges();
    this.getTemplates();
  }

  doCancel() {
    this.router.navigate(['/index']);
  }

  firstDropDownChanged(val: any) {
    const obj = this._values1[val];
    if (!obj) { return; }
    if (obj.id == 1) {
      this._values2 = [
        { id: 2, name: 'EMAIL' }
      ];
      this.defaultValue = true;
    } else if (obj.id == 2) {
      this._values2 = [
        { id: 1, name: 'SMS' },
        { id: 3, name: 'VOICE' }
      ];
    } else if (obj.id == 3) {
      this._values2 = [
        { id: 4, name: 'EMAIL and SMS' },
        { id: 1, name: 'SMS' },
        { id: 2, name: 'EMAIL' },
        { id: 3, name: 'VOICE' },
        { id: 5, name: 'EMAIL AND VOICE' }
      ];
      this.defaultValue1 = true;
    } else {
      this._values2 = [];
    }
  }

  getAllCountries() {
    this.service.getAllCountries().subscribe((data: any) => {
      this.countries = data.response;
    });
  }

  getAllIssuers() {
    this.service.getAllIssuers().subscribe((data: any) => {
      this.issuers = data.response;
      console.log(this.issuers);
      const bankList = [];
      for (let i = 0; i < data.response.length; i++) {
        bankList.push(data.response[i].bankCode);
      }
      this.createGlobalAdminForm.patchValue({
        slt_Issuers: bankList,
      });
    });
  }

  fetchAllPrivileges() {
    this.service.getAllPrivileges().subscribe((data: any) => {
      console.log(data);
      this.privileges = data.response;
      this.globalPrivileges = data.response.globalPrivList;
      this.issuerPrivileges = data.response.issuerPrivList;
      console.log(data.response.globalPrivList.length);
      console.log(data.response.issuerPrivList);
      for (let i = 0; i < data.response.globalPrivList.length; i++) {
        this.createGlobalAdminForm.addControl(data.response.globalPrivList[i].privilegeId,
          new FormControl(true));
      }
      for (let y = 0; y < data.response.issuerPrivList.length; y++) {
        this.createGlobalAdminForm.addControl(y.toString(), new FormControl(true));
      }
    });
  }

  getTemplates() {
    this.service.getTemplates().subscribe((data: any) => {
      this.templates = data.response;
    });
  }

  allIssuerPrivileges(event) {
    const checked = event.target.checked;
    this.checked2 = checked;
    const Issuerprivileges = {};
    for (let i = 0; i < this.issuerPrivileges.length; i++) {
      if (this.createGlobalAdminForm.get('issuerPrivs').value == true) {
        Issuerprivileges[i.toString()] = true;
      } else {
        Issuerprivileges[i.toString()] = false;
      }
    }
    this.createGlobalAdminForm.patchValue(Issuerprivileges);
  }

  allglobalPrivileges(event) {
    const checked = event.target.checked;
    this.checked1 = checked;
    const globalPrivileges = {};
    for (let i = 0; i < this.globalPrivileges.length; i++) {
      if (this.createGlobalAdminForm.get('globalPrivs').value == true) {
        let property;
        property = this.globalPrivileges[i].privilegeId;
        globalPrivileges[property] = true;
      } else {
        let property;
        property = this.globalPrivileges[i].privilegeId;
        globalPrivileges[property] = false;
      }
    }
    this.createGlobalAdminForm.patchValue(globalPrivileges);
  }

  getUnique(array) {
    const uniqueArray = [];
    for (let i = 0; i < array.length; i++) {
      if (uniqueArray.indexOf(array[i]) == -1) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  }

  addCountries() {
    if (this.createGlobalAdminForm.get('slt_Countries').value.length == 0) {
      alert('Select atleast one Country from List Of Countries');
    }
    for (let i = 0; i <
      this.createGlobalAdminForm.get('slt_Countries').value.length; i++) {
      this.selectedCountries.push(this.createGlobalAdminForm.get('slt_Countries').value[i]);
    }
    this.selectedCountries = this.getUnique(this.selectedCountries);
  }


  removeCountries() {
    if (this.createGlobalAdminForm.get('slt_selCountries').value.length == 0) {
      alert('Select atleast one Country from Selected Countries');
    }
    this.selectedRemoveCountries.length = 0;
    for (let i = 0; i <
      this.createGlobalAdminForm.get('slt_selCountries').value.length; i++) {
      this.selectedRemoveCountries.push(this.createGlobalAdminForm.get('slt_selCountries').value[i]);
    }
    for (let j = 0; j < this.selectedRemoveCountries.length; j++) {
      this.selectedCountries.splice($.inArray(this.selectedRemoveCountries[j],
        this.selectedCountries), 1);
    }
  }

  addIssuers() {
    if (this.createGlobalAdminForm.get('slt_Issuers').value.length == 0) {
      alert('Select atleast one Issuer from List Of Issuers');
    }
    for (let i = 0; i <
      this.createGlobalAdminForm.get('slt_Issuers').value.length; i++) {
      this.selectedIssuers.push(this.createGlobalAdminForm.get('slt_Issuers').value[i]);
    }
    this.selectedIssuers = this.getUnique(this.selectedIssuers);
  }

  removeIssuers() {
    if (this.createGlobalAdminForm.get('slt_selIssuers').value.length == 0) {
      alert('Select atleast one Issuer from Selected Of Issuers');
    }
    this.selectedRemoveIssuers.length = 0;
    for (let i = 0; i <
      this.createGlobalAdminForm.get('slt_selIssuers').value.length; i++) {
      this.selectedRemoveIssuers.push(this.createGlobalAdminForm.get('slt_selIssuers').value[i]);
    }
    for (let j = 0; j < this.selectedRemoveIssuers.length; j++) {
      this.selectedIssuers.splice($.inArray(this.selectedRemoveIssuers[j],
        this.selectedIssuers), 1);
    }
  }


  onChangeTemplate(value: any) {
    if (value == 0) {
      this.templateId = false;
      this.getbankList(this.createGlobalAdminForm.get('blksimultlogin').value);
      this.createGlobalAdminForm.get('blksimultlogin').enable();
      this.createGlobalAdminForm.get('updateProfileAtLogin').enable();
    } else {
      this.templateId = true;
      this.getbankList(this.createGlobalAdminForm.get('selectedTemplateName').value);
      this.createGlobalAdminForm.get('pwdNeverExpires').patchValue('');
      this.createGlobalAdminForm.get('blksimultlogin').patchValue('');
      this.createGlobalAdminForm.get('updateProfileAtLogin').patchValue('');
      this.createGlobalAdminForm.get('blksimultlogin').disable();
      this.createGlobalAdminForm.get('updateProfileAtLogin').disable();
    }
  }
  getbankList(selectedTemplate) {
    const bankList = [];
    this.service.getBanklist(selectedTemplate).subscribe((data) => {
      console.log(data.response);
      for (let i = 0; i < data.response.length; i++) {
        bankList.push(data.response[i].bankCode);
      }
      this.createGlobalAdminForm.patchValue({
        slt_selIssuers: bankList,
      });
    });
  }
  changeAdmin(value: any) {
    const obj = this._values1[value];
    if (obj.id == 0) {
      this.adminId = false;
    } else {
      this.adminId = true;
      if (this.defaultValue == true) {
        this.createGlobalAdminForm.controls['twoFactorDeliveryChannelType'].setValue(2,
          { onlySelf: true });
          this.defaultValue = false;
      } else if (this.defaultValue1 == true) {
        this.createGlobalAdminForm.controls['twoFactorDeliveryChannelType'].setValue(4,
          { onlySelf: true });
          this.defaultValue1 = false;
      } else {
        this.createGlobalAdminForm.controls['twoFactorDeliveryChannelType'].setValue(1,
          { onlySelf: true });
      }
    }
  }


  createGlobalAdmin() {
    const privilegesList = [];
    this.issuerCode.length = 0;
    this.countrycode.length = 0;
    for (let i = 0; i < this.selectedCountries.length; i++) {
      for (let j = 0; j < this.countries.length; j++) {
        if (this.countries[j].COUNTRYNAME == this.selectedCountries[i]) {
          this.countrycode.push(this.countries[j].COUNTRYCODE.toString());
        }
      }
    }

    for (let i = 0; i < this.selectedIssuers.length; i++) {
      for (let j = 0; j < this.issuers.length; j++) {
        if (this.issuers[j].bankName == this.selectedIssuers[i]) {
          this.issuerCode.push(this.issuers[j].bankCode.toString());
        }
      }
    }
    const issuercode = this.getUnique(this.issuerCode);
    const countryCode = this.getUnique(this.countrycode);
    for (let i = 0; i < this.issuerPrivileges.length; i++) {
      const y = i.toString();
      if (this.createGlobalAdminForm.get(y).value == true) {
        privilegesList.push(this.issuerPrivileges[i].privilegeId);
      }
    }
    for (let i = 0; i < this.globalPrivileges.length; i++) {
      if (this.createGlobalAdminForm.get(this.globalPrivileges[i].privilegeId).value
        == true) {
        privilegesList.push(this.globalPrivileges[i].privilegeId);
      }
    }
    if (this.templateId == false) {
      this.createGlobalAdminForm.patchValue({
        slt_selCountries: countryCode,
        slt_selIssuers: issuercode,
        privs: this.getUnique(privilegesList)
      });
    }
    console.log(this.createGlobalAdminForm.value);
    if (this.templateId == false) {
      this.service.createGlobalAdmin(this.createGlobalAdminForm.value).subscribe((data) => {
        this.passwordMessage = false;
        if (data.statusCode == 1) {
          this.errorMessage = data.response;
        }
      }, (error) => {
        if (error.error.response == null) {
          this.passwordMessage = false;
          this.errorMessage = error.error.errorList[0].errorMsg;
        } else if (error.error.response != null) {
          this.passwordMessage = true;
          this.errorMessage = error.error.response;
        }
      }
      );
    } else {
      this.service.createGlobalAdminTemplate(this.createGlobalAdminForm.value).subscribe((data) => {
        this.errorMessage = data.response;
      });
    }
  }
}
