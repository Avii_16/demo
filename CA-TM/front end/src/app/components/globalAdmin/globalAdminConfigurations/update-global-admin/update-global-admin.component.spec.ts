import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGlobalAdminComponent } from './update-global-admin.component';

describe('UpdateGlobalAdminComponent', () => {
  let component: UpdateGlobalAdminComponent;
  let fixture: ComponentFixture<UpdateGlobalAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateGlobalAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGlobalAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
