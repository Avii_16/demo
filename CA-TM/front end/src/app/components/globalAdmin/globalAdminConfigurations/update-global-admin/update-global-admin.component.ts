import { Component, OnInit } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as $ from 'jquery';
import { UpdateGlobalAdminPrivilegesService } from '../../../../services/global_admin_service/update_global_admin_privileges/update-global-admin-privileges.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-global-admin',
  templateUrl: './update-global-admin.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']

})
export class UpdateGlobalAdminComponent implements OnInit {

  globalAdminUpdateForm: FormGroup;
  $: any;
  showDetails = false;
  templateId = false;
  users: any;
  userdata: string;
  _values2 = [];
  banklist: any = [];
  adminId = false;
  loggedInAdminsPrivilegesLength = 0;
  adminDetails: any;
  loggedInPrivileges: any;
  adminPrivileges: any;
  privilegesSize: number;
  isPrivilege = false;
  checkAllPrivileges = false;
  selectedIssuers: any = [];
  selectedRemoveIssuers: any = [];
  issuerCode: any = [];
  show = false;
  errorMessage: any;
  templates: any = [];
  _values1 = [
    { id: 0, name: 'Do not collect' },
    { id: 1, name: 'Only Email Address' },
    { id: 2, name: 'Only Phone Number' },
    { id: 3, name: 'Both Email Address and Phone Number' }
  ];
  privileges: any;
  globalPrivileges: any;
  issuerPrivileges: any;
// tslint:disable-next-line: no-inferrable-types
  hide: boolean = false;

  constructor(private formBuilder: FormBuilder, private router: Router , private service: UpdateGlobalAdminPrivilegesService, private ref: ChangeDetectorRef) {
    this.globalAdminUpdateForm = this.formBuilder.group({
      userid: [''],
      firstName: [''],
      middleName: [''],
      lastName: [''],
      description: [''],
      templateIdClear: ['', ],
      password: [''],
      rePassword: [''],
      changePasswordAtFirstLogin: [''],
      pwdNeverExpires: [''],
      blksimultlogin: [''],
      updateProfileAtLogin: [''],
      maxValForLastPwdList: [''],
      collectAdminContactDetails: [''],
      twoFactorDeliveryChannelType: [''],
      processorName: [''],
      subProcessorName: [''],
      slt_Issuers: [''],
      slt_selIssuers: [''],
      userEncoding: [''],
      privs: [''],
      issuerPrivs: [''],
      globalPrivs : ['']

    });
    this.globalAdminUpdateForm.controls['userEncoding'].setValue('--', { onlySelf: true });
  }

  ngOnInit() {

    this.getAllUserIds();
    this.getallbanks();
    this.fetchAllPrivileges();
  }

  firstDropDownChanged(val: any) {
    const obj = this._values1[val];
    if (!obj) { return; }
    if (obj.id == 1) {
      this._values2 = [
        { id: '2', name: 'EMAIL' }
      ];
    } else if (obj.id == 2) {
      this._values2 = [
        { id: '1', name: 'SMS' },
        { id: '2', name: 'VOICE' }
      ];
    } else if (obj.id == 3) {
      this._values2 = [
        { id: '1', name: 'EMAIL and SMS' },
        { id: '2', name: 'SMS' },
        { id: '3', name: 'EMAIL' },
        { id: '4', name: 'VOICE' },
        { id: '5', name: 'EMAIL AND VOICE' }
      ];
    } else {
      this._values2 = [];
    }
  }

  getUnique(array) {
    const uniqueArray = [];
    for (let i = 0; i < array.length; i++) {
      if (uniqueArray.indexOf(array[i]) == -1) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  }

  addIssuers() {
  console.log(this.globalAdminUpdateForm.get('slt_Issuers').value);
    console.log(this.selectedIssuers);
    if (this.globalAdminUpdateForm.get('slt_Issuers').value.length == 0) {
      window.confirm('Select atleast one Issuer from List Of Issuers');
    }
    for (let i = 0; i < this.globalAdminUpdateForm.get('slt_Issuers').value.length; i++) {
      this.selectedIssuers.push(this.globalAdminUpdateForm.get('slt_Issuers').value[i]);
    }
    this.selectedIssuers = this.getUnique(this.selectedIssuers);
    this.globalAdminUpdateForm.patchValue({slt_Issuers: ''});
  }

  removeIssuers() {
    this.selectedRemoveIssuers.length = 0;
    if (this.globalAdminUpdateForm.get('slt_selIssuers').value.length == 0) {
      window.confirm('Select atleast one Issuer from List Of Issuers');
    }
    for (let i = 0; i < this.globalAdminUpdateForm.get('slt_selIssuers').value.length; i++) {
      this.selectedRemoveIssuers.push(this.globalAdminUpdateForm.get('slt_selIssuers').value[i]);
    }
    for (let j = 0; j < this.selectedRemoveIssuers.length; j++) {
      this.selectedIssuers.splice($.inArray(this.selectedRemoveIssuers[j], this.selectedIssuers), 1);
    }
    this.globalAdminUpdateForm.patchValue({slt_selIssuers: ''});
  }

  getAllUserIds() {

    this.service.getallUsers().subscribe((data: any) => {

      this.users = data.response;

    });
  }
  getallbanks() {

    this.service.getallbanks().subscribe((data: any) => {
      if (data.statusCode == 1) {
        this.banklist = data.response;
      }
      console.log(this.banklist);
    });
  }

  doCancel() {
    this.router.navigate(['/index']);
  }

  changeTemplate(value: any) {
    if (value == 0) {
      this.templateId = false;
      this.globalAdminUpdateForm.get('blksimultlogin').enable();
      this.globalAdminUpdateForm.get('updateProfileAtLogin').enable();
      this.globalAdminUpdateForm.get('slt_selIssuers').enable();
      this.globalAdminUpdateForm.get('slt_Issuers').enable();
      this.globalAdminUpdateForm.get('maxValForLastPwdList').enable();
      this.globalAdminUpdateForm.get('collectAdminContactDetails').enable();
      this.globalAdminUpdateForm.get('twoFactorDeliveryChannelType').enable();
      this.globalAdminUpdateForm.get('processorName').enable();
      this.globalAdminUpdateForm.get('subProcessorName').enable();
      for (let i = 0; i < this.globalPrivileges.length; i++) {
        this.globalAdminUpdateForm.get(this.globalPrivileges[i].privilegeId).enable();
     }
     for (let j = 0; j < this.issuerPrivileges.length; j++) {
      this.globalAdminUpdateForm.get(j.toString()).enable();
    }
    } else {
      this.templateId = true;
      this.globalAdminUpdateForm.get('blksimultlogin').disable();
      this.globalAdminUpdateForm.get('updateProfileAtLogin').disable();
      this.globalAdminUpdateForm.get('maxValForLastPwdList').disable();
      this.globalAdminUpdateForm.get('collectAdminContactDetails').disable();
      this.globalAdminUpdateForm.get('twoFactorDeliveryChannelType').disable();
      this.globalAdminUpdateForm.get('processorName').disable();
      this.globalAdminUpdateForm.get('subProcessorName').disable();
      for (let i = 0; i < this.globalPrivileges.length; i++) {
        this.globalAdminUpdateForm.get(this.globalPrivileges[i].privilegeId).disable();
     }
     for (let j = 0; j < this.issuerPrivileges.length; j++) {
      this.globalAdminUpdateForm.get(j.toString()).disable();
    }
    }

  }
  showDeliveryChannel(val: any) {
    const obj = this._values1[val];
    if (obj.id == 0) {
      this.adminId = false;
    } else {
      this.adminId = true;
    }
  }
  allIssuerPrivileges(event) {
    const checked = event.target.checked;
    for (let i = 0; i < this.issuerPrivileges.length; i++) {
        this.issuerPrivileges[i].checked = checked;
    }
    const Issuerprivileges = {};
    for (let i = 0; i < this.issuerPrivileges.length; i++) {
      if (this.globalAdminUpdateForm.get('issuerPrivs').value == true) {
        Issuerprivileges[i.toString()] = true;
      } else {
        Issuerprivileges[i.toString()] = false;
      }
    }
    this.globalAdminUpdateForm.patchValue(Issuerprivileges);
  }

  allglobalPrivileges(event) {
    const checked = event.target.checked;
    const globalPrivileges = {};
    for (let i = 0; i < this.globalPrivileges.length; i++) {
      this.globalPrivileges[i].checked = checked;
  }
    for (let i = 0; i < this.globalPrivileges.length; i++) {
      if (this.globalAdminUpdateForm.get('globalPrivs').value == true) {
        let property;
        property = this.globalPrivileges[i].privilegeId;
        globalPrivileges[property] = true;
      } else {
        let property;
        property = this.globalPrivileges[i].privilegeId;
        globalPrivileges[property] = false;
      }
    }
    this.globalAdminUpdateForm.patchValue(globalPrivileges);
  }
  changeAdmin(userId: any) {
    if (userId == 0) {
      this.showDetails = false;
      this.show = false;
    } else {
      this.showDetails = true;
      this.show = true;
      this.selectedIssuers.length = 0;
    let issuers = [];
    this.service.getAdminDetails(this.globalAdminUpdateForm.get('userEncoding').value).subscribe((adminDetails) => {
      if (adminDetails.statusCode == 2) {
        this.adminDetails = adminDetails;
        this.showDetails = true;
        if (this.adminDetails.adminInput.templateIdClear == null) {
        issuers = adminDetails.authBanksOfSelAdmin;
        {
          if (adminDetails.authBanksOfSelAdmin.length > 0) {
        for (let i = 0; i < adminDetails.authBanksOfSelAdmin.length; i++) {
          this.selectedIssuers.push(adminDetails.authBanksOfSelAdmin[i].label);
          }
        }
      }
    } else {
      if (adminDetails.authBanksOfSelTemplate.length > 0) {
        for (let i = 0; i < adminDetails.authBanksOfSelTemplate.length; i++) {
          this.selectedIssuers.push(adminDetails.authBanksOfSelTemplate[i].label);
          }
        }
    }
        this.globalAdminUpdateForm.patchValue({
          slt_selAdmins: adminDetails.adminInput.authBanksOfSelAdmin,
          firstName: adminDetails.adminInput.firstNameClear,
          middleName: adminDetails.adminInput.middleNameClear,
          lastName: adminDetails.adminInput.lastNameClear,
          description: adminDetails.adminInput.description,
          pwdNeverExpires: adminDetails.adminInput.pwdNeverExpires,
          blksimultlogin: adminDetails.adminInput.blockkSimultaneousLogin,
          divTwoFactorAuthTypeLabel: adminDetails.adminInput.twoFactorAuthenticationEnabled,
          maxValForLastPwdList: adminDetails.adminInput.maxValForLastPwdList,
          templateIdClear: adminDetails.adminInput.templateIdClear,
          processorName: adminDetails.adminInput.processorName,
          subProcessorName: adminDetails.adminInput.subProcessorName,
          updateProfileAtLogin: adminDetails.adminInput.updateProfileAtLogin,
          collectAdminContactDetails: adminDetails.adminInput.collectAdminContactDetails,
          twoFactorDeliveryChannelType: adminDetails.adminInput.twoFactorDeliveryChannelType
        });
        this.loggedInPrivileges = this.adminDetails.loggedInAdminsPrivileges;
        this.adminPrivileges = this.adminDetails.selectedAdmin.privilegeIDList;
        this.privilegesSize = this.adminDetails.loggedInAdminsPrivileges.length;

        for (let j = 0; j < this.loggedInPrivileges.length; j++) {
          let flag = 0;
          for (let i = 0; i < this.adminPrivileges.length; i++) {
            if (this.loggedInPrivileges[j].privilegeId == this.adminPrivileges[i]) {
              this.loggedInPrivileges[j].checked = true;
              flag = 1;
            }
          }
          if (flag == 0) {
            this.loggedInPrivileges[j].checked = false;
          }
        }
        this.addingPrivileges();
        this.settingDefaultFormValues(adminDetails);
      }
    });
  }
}
fetchAllPrivileges() {
  this.service.getAllPrivileges().subscribe((data: any) => {
    console.log(data);
    this.privileges = data.response;
    this.globalPrivileges = data.response.globalPrivList;
    this.issuerPrivileges = data.response.issuerPrivList;
    console.log(data.response.globalPrivList.length);
    console.log(data.response.issuerPrivList);
  });
}
  addingPrivileges() {
    for (let i = 0; i < this.globalPrivileges.length; i++) {
      let flag = 0;
      for (let j = 0; j < this.loggedInPrivileges.length; j++) {
        if (this.globalPrivileges[i].privilegeId == this.loggedInPrivileges[j].privilegeId) {
        this.globalAdminUpdateForm.addControl(this.globalPrivileges[i].privilegeId,
        new FormControl(this.loggedInPrivileges[j].checked));
        this.globalPrivileges[i].checked = this.loggedInPrivileges[j].checked ;
        flag = 1;
      }
    }
    if (flag == 0) {
      this.globalPrivileges[i].checked = false ;
      this.globalAdminUpdateForm.get(this.globalPrivileges[i].privilegeId).disable();
    }
  }
    for (let y = 0; y < this.issuerPrivileges.length; y++) {
      let flag = 0;
      for (let j = 0; j < this.loggedInPrivileges.length; j++) {
        if (this.issuerPrivileges[y].privilegeId == this.loggedInPrivileges[j].privilegeId) {
        this.globalAdminUpdateForm.addControl(y.toString(),
        new FormControl(this.loggedInPrivileges[j].checked));
        this.issuerPrivileges[y].checked = this.loggedInPrivileges[j].checked;
        flag = 1;
      }
    }
    if (flag == 0) {
      this.issuerPrivileges[y].checked = false ;
      this.globalAdminUpdateForm.get(y.toString()).disable();
    }
    }
    console.log(this.globalPrivileges);
    console.log(this.issuerPrivileges);
  }
  settingDefaultFormValues(adminDetails) {
    this.adminDetails = adminDetails;
    this.templates.length = 0;
    this.globalAdminUpdateForm.controls['templateIdClear'].setValue('-1000', { onlySelf: true });
    if ( this.adminDetails.adminInput.templateIdClear == null) {
      this.globalAdminUpdateForm.get('slt_selIssuers').enable();
      this.globalAdminUpdateForm.get('slt_Issuers').enable();
      this.hide = true;
      this.globalAdminUpdateForm.get('maxValForLastPwdList').enable();
      this.globalAdminUpdateForm.get('processorName').enable();
      this.globalAdminUpdateForm.get('subProcessorName').enable();
      this.globalAdminUpdateForm.get('collectAdminContactDetails').enable();
      for (let i = 0; i < this.globalPrivileges.length; i++) {
        this.globalAdminUpdateForm.get(this.globalPrivileges[i].privilegeId).enable();
     }
     for (let j = 0; j < this.issuerPrivileges.length; j++) {
      this.globalAdminUpdateForm.get(j.toString()).enable();
    }

    } else {
      this.templates.push(this.adminDetails.adminInput.templateIdClear);
      this.globalAdminUpdateForm.controls['templateIdClear'].setValue(this.adminDetails.adminInput.templateIdClear, { onlySelf: true });
      this.globalAdminUpdateForm.get('slt_Issuers').disable();
      this.globalAdminUpdateForm.get('maxValForLastPwdList').disable();
      this.globalAdminUpdateForm.get('processorName').disable();
      this.globalAdminUpdateForm.get('subProcessorName').disable();
      this.globalAdminUpdateForm.get('collectAdminContactDetails').disable();
      for (let i = 0; i < this.globalPrivileges.length; i++) {
        this.globalAdminUpdateForm.get(this.globalPrivileges[i].privilegeId).disable();
     }
     for (let j = 0; j < this.issuerPrivileges.length; j++) {
      this.globalAdminUpdateForm.get(j.toString()).disable();
    }
    if (this.adminDetails.adminInput.collectAdminContactDetails != 0) {
      this.globalAdminUpdateForm.controls['collectAdminContactDetails'].setValue(this.adminDetails.adminInput.collectAdminContactDetails, { onlySelf: true });
      this.adminId = true;
      this.firstDropDownChanged(this.adminDetails.adminInput.collectAdminContactDetails);
      this.globalAdminUpdateForm.controls['twoFactorDeliveryChannelType'].setValue(this.adminDetails.adminInput.twoFactorDeliveryChannelType, { onlySelf: true });
    } else {
      this.globalAdminUpdateForm.controls['collectAdminContactDetails'].setValue(0, { onlySelf: true });
      this.adminId = false;
      this.templateId = false;
    }
    this.globalAdminUpdateForm.controls['processorName'].setValue(this.adminDetails.adminInput.processorName, { onlySelf: true });
    this.globalAdminUpdateForm.controls['subProcessorName'].setValue(this.adminDetails.adminInput.subProcessorName, { onlySelf: true });
    this.globalAdminUpdateForm.controls['maxValForLastPwdList'].setValue(this.adminDetails.adminInput.maxValForLastPwdList, { onlySelf: true });

  }

  }

  updateGlobalAdmin() {
    console.log(this.globalAdminUpdateForm.value);
    const updatedPrivileges = [];
    for (let i = 0; i < this.selectedIssuers.length; i++) {
      for (let j = 0; j < this.banklist.length; j++) {
        if (this.banklist[j].bankName == this.selectedIssuers[i]) {
          this.issuerCode.push(this.banklist[j].bankCode.toString());
        }
      }
    }
    for (let i = 0; i < this.issuerPrivileges.length; i++) {
      const y = i.toString();
      if (this.globalAdminUpdateForm.get(y).value == true) {
        updatedPrivileges.push(this.issuerPrivileges[i].privilegeId);
      }
    }
    for (let i = 0; i < this.globalPrivileges.length; i++) {
      if (this.globalAdminUpdateForm.get(this.globalPrivileges[i].privilegeId).value == true) {
          updatedPrivileges.push(this.globalPrivileges[i].privilegeId);
      }
    }
    this.issuerCode = this.getUnique(this.issuerCode);

    this.globalAdminUpdateForm.patchValue({
      privs: updatedPrivileges,
      slt_selIssuers: this.issuerCode
    });
    console.log(this.globalAdminUpdateForm.value);
    this.service.updateGLobalAdminPrivileges(this.globalAdminUpdateForm.value).subscribe((data) => {
      if (data.statusCode == 2) {
        this.errorMessage = data.response;
      } else if (data.statusCode == -3) {
        this.errorMessage = data.response;
      }
    },
    (error) => {
      console.log(error);
      this.errorMessage = error.error.errorList[0].errorMsg;
      console.log(this.errorMessage);
    });
  }
}