import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResetGlobalAdminPasswordService } from '../../../../services/global_admin_service/reset_global_admin_password/reset-global-admin-password.service';
import { ErrorMessagesService } from '../../../../services/service_for_errorMessages/error-messages.service';

@Component({
  selector: 'app-reset-global-admin',
  templateUrl: './reset-global-admin.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})

export class ResetGlobalAdminComponent implements OnInit {
  showHeader = true;
  changeId = false;
  resetPasswordForm: FormGroup;
  admins: any = [];
  errorMessage: string;
  adminName: any;
  errorData: any;
  statusInactive = true;
  collectDeatils: boolean;
  showContactMessage: boolean;
  // tslint:disable-next-line: no-inferrable-types
  message: string = 'Enter twice the new password and a remark';
  // tslint:disable-next-line: no-inferrable-types
  description: string = 'Select a User ID and click ';

  constructor(private formbuilder: FormBuilder, private router: Router, private service: ResetGlobalAdminPasswordService, private errorService: ErrorMessagesService) {

    this.errorService.getJSON().subscribe((data) => {
      this.errorData = data;
    });

    this.resetPasswordForm = this.formbuilder.group({
      newPassword: [''],
      retypeNewpassword: [''],
      remark: [''],
      Admins: [''],
      changePwdAtLogin: [''],
      emailCheck: [''],
      emailAddr: [''],
      phoneCheck: [''],
      phoneNum: ['']
    });

    this.resetPasswordForm.controls['Admins'].setValue('0', { onlySelf: true });
  }

  ngOnInit() {
    this.fetchAllAdmins();
  }

   // sort all admins
   sortAdmins(admins) {
    this.admins =  admins.sort(function(a, b) {
      const x = a.name.toLowerCase();
      const y = b.name.toLowerCase();
      if (x < y) {return -1; }
      if (x > y) {return 1; }
      return 0;
    });
  }
  changeAdmin(value: any) {
    if (value == 0) {
      this.changeId = false;
      this.errorMessage = 'Invalid admin selected.';
      this.description = 'Select a User ID and click ';
      this.resetPasswordForm.get('newPassword').patchValue('');
      this.resetPasswordForm.get('retypeNewpassword').patchValue('');
      this.resetPasswordForm.get('remark').patchValue('');
    } else {
      this.description = 'Select a User ID, reset the password and click';
      this.errorMessage = ' ';
      const selectedAdmin = this.resetPasswordForm.get('Admins').value;
      this.resetPasswordForm.reset({ newPassword: '', retypeNewpassword: '', remark: '' }, { onlySelf: true });
      this.resetPasswordForm.controls['Admins'].setValue(selectedAdmin, { onlySelf: true });
      for (let i = 0; i < this.admins.length; i++) {
        if (selectedAdmin == this.admins[i].name) {
          if (this.admins[i].status == 2) {
            this.statusInactive = false;
          } else if (this.admins[i].resetType == 0 && this.admins[i].collectAdminDetails == 0) {
            this.changeId = true;
            this.collectDeatils = false;
          } else {
            this.changeId = true;
            this.collectDeatils = true;
          }
        } else {
          this.changeId = true;
        }
      }
      console.log(this.collectDeatils);
      console.log(this.changeId);
    }
    this.adminName = this.resetPasswordForm.get('Admins').value;
  }

  fetchAllAdmins() {
    this.service.getAllAdmins().subscribe((data) => {
      this.admins = data.response;
      console.log(this.admins);
      this.sortAdmins(this.admins);
    });
  }

  cancelOperation() {
    this.router.navigate(['/index']);
  }

  resetPassword() {
    if (this.collectDeatils == true) {
      this.changeId = false;
      this.collectDeatils = false;
      this.showContactMessage = true;
      this.showHeader = false;
    } else {
      let bankName: any;
      for (let i = 0; i < this.admins.length; i++) {
        if (this.adminName == this.admins[i].name) {
          bankName = this.admins[i].name;
        }
      }
      this.resetPasswordForm.patchValue({
        Admins: bankName
      });

      this.service.resetGlobalAdmin(this.resetPasswordForm.value, this.adminName).subscribe((data) => {
        this.resetPasswordForm.controls['Admins'].setValue(this.resetPasswordForm.get('Admins').value, { onlySelf: true });
        if (data.statusCode == 1) {
          this.errorMessage = 'Reset Admin Password successful';
        } else if (data.statusCode == -4) {
          this.errorMessage = 'Password Policy for this level is not satisfied';
        } else {
          for (let i = 0; i < this.errorData.length; i++) {
            if (this.errorData[i].errorCode == data.response) {
              this.errorMessage = this.errorData[i].errorMessage;
              break;
            } else {
              this.errorMessage = 'Two Passwords should match and can not be blank';
            }
          }
        }

      },
        (error) => {
          console.log(error);
          this.errorMessage = error.error.errorList[0].errorMsg;
          console.log(this.errorMessage);
        });
    }
  }
}