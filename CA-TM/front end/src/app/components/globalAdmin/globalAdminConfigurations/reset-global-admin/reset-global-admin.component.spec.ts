import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetGlobalAdminComponent } from './reset-global-admin.component';

describe('ResetGlobalAdminComponent', () => {
  let component: ResetGlobalAdminComponent;
  let fixture: ComponentFixture<ResetGlobalAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetGlobalAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetGlobalAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
