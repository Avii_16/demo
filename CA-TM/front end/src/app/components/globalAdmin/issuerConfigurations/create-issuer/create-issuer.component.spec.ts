import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateIssuerComponent } from './create-issuer.component';

describe('CreateIssuerComponent', () => {
  let component: CreateIssuerComponent;
  let fixture: ComponentFixture<CreateIssuerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateIssuerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateIssuerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
