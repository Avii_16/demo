import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as $ from 'jquery';
import { CreateIssuerService } from '../../../../services/issuer_configuration_service/create_issuer/create-issuer.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GlobalAdminService } from '../../../../services/global_admin_service/create_global_admin/global-admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-issuer',
  templateUrl: './create-issuer.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class CreateIssuerComponent implements OnInit {
  createIssuerForm: FormGroup;
  $: any;
  countries: any;
  issuers: any;
  locales: any;
  admins: any;
  _values2 = [];
  errorMessage: string;
  selectedAdmins: any = [];
  selectedRemoveAdmins: any = [];
  timeZones: any;
  adminId: boolean;
  users: any;

  _values1 = [
    { id: 0, name: 'Do not collect' },
    { id: 1, name: 'Only Email Address' },
    { id: 2, name: 'Only Phone Number' },
    { id: 3, name: 'Both Email Address and Phone Number' }
  ];

  constructor(private formBuilder: FormBuilder, private fetchDataService: GlobalAdminService,
    private service: CreateIssuerService, private http: HttpClient, private router: Router) {
    this.createIssuerForm = this.formBuilder.group({
      bankName: [''],
      country: [''],
      localeID: [''],
      localTimeZone: [''],
      customerId: [''],
      bankDirName: [''],
      issuerDisplayName: [''],
      encryptionKey: [''],
      uploadKey: [''],
      uploadKeyHidden: [''],
      retypeUploadKey: [''],
      userEncoding: [''],
      cvvKeyA: [''],
      cvvKeyB: [''],
      bankKeyModule: [''],
      arKeyModule: [''],
      processorName: [''],
      subProcessorName: [''],
      processorData: [''],
      processorInfo: [''],
      CVVKEYIND: [''],
      DoNotPromptBehavior: [''],
      enrollmentLocales: [''],
      userIdEnabled: [''],
      twoStepLoginEnabled: [''],
      dualAuthRequiredAtCommit: [''],
      collectAdminContactDetails: [''],
      slt_Admins: [''],
      slt_selAdmins: [''],
      twoFactorDeliveryChannelType: ['']
    });
    this.createIssuerForm.controls['country'].setValue('0', { onlySelf: true });
    this.createIssuerForm.controls['localTimeZone'].setValue('GMT-08:00', { onlySelf: true });
    this.createIssuerForm.controls['userEncoding'].setValue('ISO-8859-1', { onlySelf: true });
    this.createIssuerForm.controls['bankKeyModule'].setValue('S/W', { onlySelf: true });
    this.createIssuerForm.controls['arKeyModule'].setValue('S/W', { onlySelf: true });
    this.createIssuerForm.controls['CVVKEYIND'].setValue('00', { onlySelf: true });
    this.createIssuerForm.controls['DoNotPromptBehavior'].setValue('1', { onlySelf: true });
    this.createIssuerForm.controls['collectAdminContactDetails'].setValue('0', { onlySelf: true });
  }

  ngOnInit() {
    this.fetchAllTimeZones();
    this.fetchAllCountries();
    this.getAllUserIds();
    // this.fetchAllLocales();
    // this.fetchAllAdmins();
  }

  doCancel() {
    this.router.navigate(['/index']);
  }

  changeAdmin(value: any) {
    const obj = this._values1[value];
    if (obj.id == 0) {
      this.adminId = false;
    } else {
      this.adminId = true;
    }
  }

  firstDropDownChanged(val: any) {
    const obj = this._values1[val];
    if (!obj) { return; }
    if (obj.id == 1) {
      this._values2 = [
        { id: 2, name: 'EMAIL' }
      ];
      this.createIssuerForm.controls['twoFactorDeliveryChannelType'].setValue('2', { onlySelf: true });
    } else if (obj.id == 2) {
      this._values2 = [
        { id: 1, name: 'SMS' },
        { id: 3, name: 'VOICE' }
      ];
      this.createIssuerForm.controls['twoFactorDeliveryChannelType'].setValue('1', { onlySelf: true });
    } else if (obj.id == 3) {
      this._values2 = [
        { id: 4, name: 'EMAIL and SMS' },
        { id: 1, name: 'SMS' },
        { id: 2, name: 'EMAIL' },
        { id: 3, name: 'VOICE' },
        { id: 5, name: 'EMAIL AND VOICE' }
      ];
      this.createIssuerForm.controls['twoFactorDeliveryChannelType'].setValue('4', { onlySelf: true });
    } else {
      this._values2 = [];
    }
  }


  getUnique(array) {
    const uniqueArray = [];
    for (let i = 0; i < array.length; i++) {
      if (uniqueArray.indexOf(array[i]) === -1) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  }
  addAdmins() {
    for (let i = 0; i < this.createIssuerForm.get('slt_Admins').value.length; i++) {
      this.selectedAdmins.push(this.createIssuerForm.get('slt_Admins').value[i]);
    }
    this.selectedAdmins = this.getUnique(this.selectedAdmins);
  }

  removeAdmins() {
    this.selectedRemoveAdmins.length = 0;
    for (let i = 0; i < this.createIssuerForm.get('slt_selAdmins').value.length; i++) {
      this.selectedRemoveAdmins.push(this.createIssuerForm.get('slt_selAdmins').value[i]);
    }
    for (let j = 0; j < this.selectedRemoveAdmins.length; j++) {
      this.selectedAdmins.splice($.inArray(this.selectedRemoveAdmins[j], this.selectedAdmins), 1);
    }
  }

  fetchAllTimeZones() {
    this.getTimeZoneList().subscribe(data => {
      this.timeZones = data;
    });
  }

  public getTimeZoneList(): Observable<any> {
    return this.http.get('../assets/constants/timeZoneList.json');
  }


  fetchAllCountries() {

    this.service.getAllCountries().subscribe((data) => {
      this.countries = data.response;
      console.log(this.countries);
    });


    // debugger;
    // this.service.getAllCountries().subscribe((data) => {
    //   this.countries = data.response;
    //   console.log(this.countries);
    // });
  }

  getAllUserIds() {
    this.service.getAllUsers().subscribe((data: any) => {
      this.users = data.response;
      console.log(this.users);
    });
  }

  // fetchAllLocales(){
  //   this.service.getAllLocales().subscribe((localesData) => {
  //     this.locales = localesData.response;
  //   })
  // }

  // fetchAllAdmins(){
  //   this.service.getAllAdmins().subscribe((adminData) => {
  //     this.admins = adminData.response;
  //   })
  // }

  createIssuers() {

    this.createIssuerForm.patchValue({
      slt_selAdmins: this.selectedAdmins
    });

    this.service.createIssuer(this.createIssuerForm.value).subscribe((response) => {
      console.log(response);
      this.errorMessage = 'Issuer added successfully';
      // if(response.statusCode === 1){
      //   this.errorMessage = "Issuer "+ response.bankName +" Added"
      // }
      // else if(response.errorList.length > 0) {
      //   this.errorMessage = response.errorList[0].errorMsg
      // }
    },
      (error) => {
        console.log(error);
        this.errorMessage = error.error.errorList[0].errorMsg;
        console.log(this.errorMessage);
      }

    );
  }
}

