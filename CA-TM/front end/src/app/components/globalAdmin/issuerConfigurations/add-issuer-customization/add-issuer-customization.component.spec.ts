import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIssuerCustomizationComponent } from './add-issuer-customization.component';

describe('AddIssuerCustomizationComponent', () => {
  let component: AddIssuerCustomizationComponent;
  let fixture: ComponentFixture<AddIssuerCustomizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddIssuerCustomizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIssuerCustomizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
