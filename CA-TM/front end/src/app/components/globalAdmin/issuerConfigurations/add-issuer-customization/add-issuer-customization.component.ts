import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AddIssuerCustomizationService } from '../../../../services/issuer_configuration_service/add_issuer_customization_service/add-issuer-customization.service';
import { Router } from '@angular/router';
import Utils from './../../../../utils';

@Component({
  selector: 'app-add-issuer-customization',
  templateUrl: './add-issuer-customization.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class AddIssuerCustomizationComponent implements OnInit {
  addIssuerCustomizationForm: FormGroup;
  issuers: any;
  checked: boolean;
  rangeGroups: any = [];
  errorMessage: string;
  rangeIDs: any = [];
  locales: any = [];
  cardRanges: any = {};
  _cardRangesKeys: any = [];
  range: any;
  rangeGroup: any;
  cardRange: any;
  locale: any;
  deviceLocale: Object;
  updateType: Object;
  bankId: any;
  rangeGroupId: any;
  cardRangeId: any;
  localeId: any;
  rangeId: any;
  folderData: Object;
  userAgentData: any;
  updateTypeId: any;
  showDropdownData = false;
  _userAgentKey: string[];
  _folderData: string[];
  _folderDataKey: string[];
  showFolderData: boolean;
  userAgentId: any;
  folderMessage: any;
  enabled: boolean;
  userAgentDisable = false;
  displayCardRange = false;
  flag: number;
  defaultMessage: any;

  constructor(private service: AddIssuerCustomizationService, private router: Router, private formBuilder: FormBuilder) {
    this.addIssuerCustomizationForm = this.formBuilder.group({
      bankName: [''],
      configId: [''],
      rangeID: [''],
      text_locale: [''],
      updateType: [''],
      userAgent: [''],
      UpgradedUI: [''],
      folder: [''],
      acsURL: [''],
    });
    this.addIssuerCustomizationForm.controls['bankName'].setValue('0', { onlySelf: true });
    this.addIssuerCustomizationForm.controls['text_locale'].setValue('0', { onlySelf: true });
    this.addIssuerCustomizationForm.controls['updateType'].setValue('-1', { onlySelf: true });
    // this.addIssuerCustomizationForm.controls['configId'].setValue('-1', { onlySelf: true });
    // this.addIssuerCustomizationForm.controls['rangeID'].setValue('0', { onlySelf: true });
  }

  ngOnInit() {
    this.fetchIssuers();
  }
  setDefaultValues() {
    this.addIssuerCustomizationForm = this.formBuilder.group({
      bankName: [''],
      configId: [''],
      rangeID: [''],
      text_locale: [''],
      updateType: [''],
      userAgent: [''],
      UpgradedUI: [''],
      folder: [''],
      acsURL: [''],
    });
  }
  doCancel() {
    this.router.navigate(['/index']);
  }

  fetchIssuers() {
    this.service.getIssuers().subscribe((data) => {
      this.issuers = data.response;
    }, (error) => {
      this.errorMessage = error.error.errorList[0].errorMsg;
    }
    );
  }
  issuerChange(value: any) {
    this.bankId = this.addIssuerCustomizationForm.get('bankName').value;
    this.fetchRangeGroup(this.bankId);
  }

  fetchRangeGroup(bankId) {
    this.service.getRangeGroup(bankId).subscribe((data) => {
      this.rangeGroups = data;
    }, (error) => {
      this.errorMessage = error.error.errorList[0].errorMsg;
    });
  }

  rangeGroupChange(value: any) {
    this.displayCardRange = false;
    this.rangeGroupId = this.addIssuerCustomizationForm.get('configId').value;
    this.fetchCardRanges(this.bankId, this.rangeGroupId);
    this.fetchDeviceLocale(this.bankId);
  }

  fetchCardRanges(bankId, rangeGroupId) {
    this.service.getCardRanges(bankId, rangeGroupId).subscribe((data) => {
      this.cardRanges = data['cardRange'];
      this._cardRangesKeys = Utils.getKeys(this.cardRanges);
    }, (error) => {
      this.errorMessage = error.error.errorList[0].errorMsg;
      if (this.errorMessage) {
        this.displayCardRange = true;
      } else {
        this.displayCardRange = false;
      }
    }
    );
  }


  cardRangeChange(value: any) {
    if (value == 0) {

    } else {

    }
    this.rangeId = this.addIssuerCustomizationForm.get('rangeID').value;
    // this.fetchDeviceLocale(this.bankId);
  }

  fetchDeviceLocale(bankId) {
    this.service.getDeviceLocale(bankId).subscribe((data) => {
      this.deviceLocale = data['deviceLocaleList'];
    }, (error) => {
      this.errorMessage = error.error.errorList[0].errorMsg;
    }


    );
  }

  localeChange(value: any) {
    if (value == 0) {

    } else {

    }
    this.localeId = this.addIssuerCustomizationForm.get('text_locale').value;
    this.fetchUpdateType(this.bankId);
  }

  fetchUpdateType(bankId) {
    this.showDropdownData = false;
    this.service.getUpdateType(bankId).subscribe((data) => {
      this.updateType = data['updateTypeList'];
      // for (let i = 0; i < 4; i++) {
      //   if (this.updateType[i].selected == -1) {
      //     // this.enabled = false;
      //     // document.getElementById('updatetype').options[i].disabled = true;
      //     document.getElementById('updatetype')[i].setAttribute("disabled","disabled");
      //   } else if (this.updateType[i].selected == 1 || this.updateType[i].selected == 0) {
      //     document.getElementById('updatetype')[i].setAttribute("disabled","disabled");

      //     // this.enabled = true;
      //     // document.getElementById('updatetype').options[i].disabled = false;
      //   }
      //   console.log(this.updateType[i].type + ' : ' + this.enabled);
      // }
      if (this.updateType[0].selected == -1) {
        this.enabled = true;
      } else {
        this.enabled = false;
      }
      // for (let i = 0; i < 4; i++) {
      //   debugger;
      //   if (this.updateType[i].selected == -1) {
      //     this.flag = 0;
      //   } else {
      //     this.flag = 1;
      //   }
      // }
      // if (this.flag == 1) {
      //   this.enabled = true;
      // } else{
      //   this.enabled = false;
      // }
      // console.log(this.enabled);
    }, (error) => {
      this.errorMessage = error.error.errorList[0].errorMsg;
    }

    );
  }

  updateTypeChange(value: any) {
    if (value == 2 || value == 4) {
      this.showDropdownData = true;
    } else {
      this.showDropdownData = false;
    }
    if (value == 1) {
      this.userAgentDisable = true;
    } else {
      this.userAgentDisable = false;
    }
    this.updateTypeId = this.addIssuerCustomizationForm.get('updateType').value;
    this.fetchFolderName(this.bankId, this.updateTypeId, this.localeId, this.rangeId);
  }

  fetchFolderName(bankId, updateTypeId, localeId, rangeId) {
    this.service.getFolderName(bankId, updateTypeId, localeId, rangeId).subscribe((data) => {
      this.userAgentData = data['userAgents'];
      this._userAgentKey = Utils.getKeys(this.userAgentData);
      this.defaultMessage = this.userAgentData[this._userAgentKey[0]];
      this.addIssuerCustomizationForm.controls['userAgent'].setValue(this.defaultMessage, { onlySelf: true });
      this.folderData = data['folderNameList'];
      this._folderDataKey = Utils.getKeys(this.folderData);
      this.folderMessage = this.folderData[this._folderDataKey[0]];
      this.addIssuerCustomizationForm.patchValue({
        folder: this.folderMessage
      });
    }, (error) => {
      this.errorMessage = error.error.errorList[0].errorMsg;
    }

    );
  }

  userAgentChange(value: any) {
    if (value == 0 || value == 1) {
      this.showFolderData = false;
    } else {
      this.showFolderData = true;
    }
    this.userAgentId = this.addIssuerCustomizationForm.get('userAgent').value;
    this.folderMessage = ' ';
    this.folderMessage = this.folderData[this.userAgentId];
    this.addIssuerCustomizationForm.patchValue({
      folder: this.folderMessage
    });
  }

  addIssuerCustomization() {
    this.service.addIssuerCustomizations(this.addIssuerCustomizationForm.value).subscribe((data) => {
      this.errorMessage = data.message;
    }, (error) => {
      this.errorMessage = error.error.errorList[0].errorMsg;
    }
    );
  }
}
