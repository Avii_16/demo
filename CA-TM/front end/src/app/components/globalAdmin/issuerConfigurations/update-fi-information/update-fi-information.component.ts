import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UpdateFiInformationService } from '../../../../services/issuer_configuration_service/update_FI_information_service/update-fi-information.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-fi-information',
  templateUrl: './update-fi-information.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class UpdateFiInformationComponent implements OnInit {

  updateFIinformationForm: FormGroup;
  fileinbytes: any = [];
  mcKey: any = [];
  cardTypelist: any = [];
  panLength: any = [];
  cardRangesGroup: any = [];
  signingKeyModule: any = [];
  authKeyModule: any = [];
  chipKeyModule: any = [];
  aAVAlgorithm: any = [];
  authPriority: any = [];
  enrollOptions: any = [];
  errorMessage: any = [];
  cardRange: any = [];
  rangeId: any = [];
  issuers: any;
  cardRanges: any = [];
  brandInfo: any = [];
  show = false;
  issuerdata: any = [];
  // tslint:disable-next-line: no-inferrable-types
  authSelected: boolean = false;
  // tslint:disable-next-line: no-inferrable-types
  authFallback1: boolean = false;
   key = ['S/W', 'nfast', 'cca'];
   chipkey = ['-1' , 'nfast', 'zaxus'];
   authkey = ['S/W', 'spp', 'zaxus', 'ibm4758'];
   aavakey = ['-1', '0', '8'];
   authprioritykey = ['', '2;1', '1', '2', '4;1', '4;2;1'];
   cardkey = ['13', '0', '15', '3', '17', '45', '53', '55', '68'];
   enrollkey = ['0', '1', '7', '2', '70', '20'];
  defaultRangeGroup: any;
  constructor( private formBuilder: FormBuilder, private service: UpdateFiInformationService, private router: Router ) {
    this.updateFIinformationForm = this.formBuilder.group({
      bankName : [''],
      CardRangeName : [''],
      cardRangeGroup : [''],
      fiBusinessID : [''],
      fiBIN : [''],
      cardType : [''],
      panLength : [''],
      beginRange : [''],
      endRange : [''],
      termPolicyVersion1 : [''],
      termPolicyVersion2 : [''],
      mobileEnabled : [''],
      signingKeyModule : [''],
      authResultKeyModule : [''],
      chipKeyModule : [''],
      AAVAlgorithm : [''],
      mcKeyID : [''],
      mcKeyAlias : [''],
      disablerange: [''],
      brandingURL1 : [''],
      brandingURL2 : [''],
      acsURL1 : [''],
      acsURL2 : [''],
      acsURL3 : [''],
      acsURL4 : [''],
      acsURL5 : [''],
      CAPURLParameters : [''],
      signingCertFile : [''],
      ahsReceiptURL : [''],
      authSelect : [true],
      authFallback : [''],
      maxAuthTries : ['3'],
      authPriority : [''],
      nStrikesAcrossSessions : [false],
      maxAuthTriesForFYP : [''],
      authMethod : [''],
      pluginURL : [''],
      eAccess : [''],
      pluginName : [''],
      hsmVariant : [''],
      pluginVersion : [''],
      cvvKeyA : [''],
      cvvKeyB : [''],
      CVVKEYIND : [''],
      passwordChangeVelocity : ['0'],
      enrollOption : [''],
      ActualEnrollOption : ['0'],
      ActualMaxWelcome : ['0'],
      ActualMaxDecline : ['0'],
      backupattempts : [''],
      maxDeclines : [''],
      velogging : [''],
      maxWelcome : [''],
      EnableAttemptsAfterMaxDeclines : [''],
      arcotOtpEnabled : [''],
      amdsCustomerID: [''],
      amdsProfilePassword: [''],
      amdsProfileEmail: [''],
      amdsAPINAME: [''],
      amdsFromEmail: [''],
      amdsFromNumber: [''],
      amdsProviderName: [''],
      trustedBeneficiary: [''],
      enableSCA: [''],
      cardRange: ['']
    });
    this.updateFIinformationForm.controls['bankName'].setValue('0', {
      onlySelf: true
    });
    this.updateFIinformationForm.controls['maxAuthTries'].setValue('3', {
      onlySelf: true
    });
    this.updateFIinformationForm.controls['mobileEnabled'].setValue('1', {
      onlySelf: true
    });
    this.updateFIinformationForm.controls['termPolicyVersion1'].setValue('1', {
      onlySelf: true
    });
    this.updateFIinformationForm.controls['termPolicyVersion2'].setValue('0', {
      onlySelf: true
    });
    this.updateFIinformationForm.controls['maxAuthTriesForFYP'].setValue('0', {
      onlySelf: true
    });
    this.updateFIinformationForm.controls['cardRange'].setValue('-1', {
      onlySelf: true
    });
  }

  ngOnInit() {
    this.fetchAllIssuers();
    this.getBankInfo();
  }
  doCancel() {
    this.router.navigate(['/index']);
  }
  fetchAllIssuers() {
    this.service.getIssuers().subscribe((data) => {
      console.log(data);
      this.issuers = data.enableCardRangesForm.authorizedBanks;
      console.log(this.issuers);
    });
  }

  getCardRangeForIssuer() {
    this.cardRanges = [];
    this.updateFIinformationForm.controls['cardRange'].setValue('-1', {
      onlySelf: true
    });
    const issuerId = this.updateFIinformationForm.get('bankName').value;
    this.service.getCardRangeForIssuer(issuerId).subscribe((data) => {
      console.log(data);

      this.cardRange = data.vecCardRange;
      this.rangeId = data.vecRangeId;
      this.errorMessage = data.message;
      console.log(this.issuers);
      for (let i = 0; i < this.rangeId.length; i++) {
        this.cardRanges.push({
          rangeId: this.rangeId[i],
          cardRange: this.cardRange[i]
        });

        this.defaultRangeGroup = this.rangeId[0];
      }
    }, (error) => {
      this.errorMessage = error.error.errorList[0].errorMsg;
      console.log(this.errorMessage);
     const body = {
      bankName: ' ',
      enrollOption: ' ',
      CardRangeName: ' ',
      cardRangeGroup: ' ',
      fiBusinessID: ' ',
      fiBIN: ' ',
      cardType: ' ',
      panLength: ' ',
      beginRange: ' ',
      endRange: ' ',
      termPolicyVersion1: ' ',
      termPolicyVersion2: ' ',
      mobileEnabled: ' ',
      signingKeyModule: ' ',
      authResultKeyModule: ' ',
      chipKeyModule: ' ',
      AAVAlgorithm: ' ',
      mcKeyID: ' ',
      mcKeyAlias: ' ',
      brandingURL1: ' ',
      brandingURL2: ' ',
      acsURL1: ' ',
      passwordChangeVelocity : '0' ,
      ActualEnrollOption : '0',
      ActualMaxWelcome : '0',
      ActualMaxDecline : '0'
      };
      this.updateFIinformationForm.patchValue(body);
    }
    );
  }
  getBankInfo() {

    let sigingmodule = [];
    let authmodule = [];
    let chipmodule = [];
    let aavalgorithm = [];
    let authpriority = [];
    let cardgroup = [];
    let enroll = [];
    this.service.getBankList().subscribe((data) => {
      console.log(data);
      this.issuerdata = data;
      this.mcKey = data.mcKey;
      authpriority = data.authPriority;
      enroll = data.enrollOptions;
      aavalgorithm = data.aAVAlgorithm;
      authmodule = data.authKeyModule;
      chipmodule = data.chipKeyModule;
      cardgroup = data.cardRangesGroup;
      this.cardTypelist = data.cardTypelist;
      sigingmodule = data.signingKeyModule;
      this.panLength = data.panLength;
      for (let i = 0; i < cardgroup.length; i++) {
        this.cardRangesGroup.push({
          value: cardgroup[i],
          key: this.cardkey[i]
        });
        if (i == cardgroup.length - 1) {
          this.cardRangesGroup.push({
            value: 'None',
            key: '-1'
          });
        }
      }
      for (let i = 0; i < sigingmodule.length; i++) {
        if (sigingmodule[i] == '') {
          this.signingKeyModule.push({
            value: '-- --',
            key: this.key[i]
          });
        } else {
          this.signingKeyModule.push({
            value: sigingmodule[i],
            key: this.key[i]
          });
        }
      }
      for (let i = 0; i < authmodule.length; i++) {
        this.authKeyModule.push({
          value: authmodule[i],
          key: this.authkey[i]
        });
      }
      for (let i = 0; i < chipmodule.length; i++) {
        this.chipKeyModule.push({
          value: chipmodule[i],
          key: this.chipkey[i]
        });
      }
      for (let i = 0; i < aavalgorithm.length; i++) {
        this.aAVAlgorithm.push({
          value: aavalgorithm[i],
          key: this.aavakey[i]
        });
      }
      for (let i = 0; i < authpriority.length; i++) {
        this.authPriority.push({
          value: authpriority[i],
          key: this.authprioritykey[i]
        });
      }
      for (let i = 0; i < enroll.length; i++) {
        this.enrollOptions.push({
          value: enroll[i],
          key: this.enrollkey[i]
        });
      }
      this.updateFIinformationForm.controls['cardRangeGroup'].setValue('0', {
        onlySelf: true
      });
      this.updateFIinformationForm.controls['cardType'].setValue('1', {
        onlySelf: true
      });
      this.updateFIinformationForm.controls['panLength'].setValue('16', {
        onlySelf: true
      });
      this.updateFIinformationForm.controls['signingKeyModule'].setValue('S/W', {
        onlySelf: true
      });
      this.updateFIinformationForm.controls['authResultKeyModule'].setValue('S/W', {
        onlySelf: true
      });
      this.updateFIinformationForm.controls['chipKeyModule'].setValue('-1', {
        onlySelf: true
      });
      this.updateFIinformationForm.controls['AAVAlgorithm'].setValue('-1', {
        onlySelf: true
      });
      this.updateFIinformationForm.controls['mcKeyID'].setValue('0', {
        onlySelf: true
      });
      this.updateFIinformationForm.controls['authPriority'].setValue('', {
        onlySelf: true
      });
      this.updateFIinformationForm.controls['enrollOption'].setValue('0', {
        onlySelf: true
      });

    });
  }
  // get issuer details from range
  getIssuerDetailsFromRange() {

    this.service.getIssuerDetailsFromRange(this.updateFIinformationForm.get('bankName').value, this.updateFIinformationForm.get('cardRange').value).subscribe((data) => {
      let  nstrikes ;
      let  attempts ;
        this.brandInfo = data.brandInfo;
        this.show = true;
        if (data.authSelect == 1) {
          this.authSelected = true;
        } else {
          this.authSelected = false;
        }
         if (data.authFallback == 1) {
          this.authFallback1 = true;
         } else {
          this.authFallback1 = false;
        }
          if (data.brandInfo.nStrikesAcrossSessions == false) {
            nstrikes = '1';
          } else {
            nstrikes = '0';
          }
          if (data.brandInfo.backUpAttempts == false) {
            attempts = '1' ;
          } else {
            attempts = '0';
          }
        this.defaultfileConvert(data.brandInfo.signingCertFileData);
        this.settingDefaultValues();

        if (data.cardRangesGroup == null) {
        this.updateFIinformationForm.controls['cardRangeGroup'].setValue(-1, {
          onlySelf: true
        });
       } else {
        this.updateFIinformationForm.controls['cardRangeGroup'].setValue(data.cardRangeGroup, {
          onlySelf: true
        });
      }
        this.updateFIinformationForm.controls['cardType'].setValue(data.brandInfo.cardType, {
          onlySelf: true
        });
        this.updateFIinformationForm.controls['signingKeyModule'].setValue(data.brandInfo.signingKeyModule, {
          onlySelf: true
        });
        this.updateFIinformationForm.controls['authResultKeyModule'].setValue(data.brandInfo.authResultKeyModule, {
          onlySelf: true
        });

        this.updateFIinformationForm.controls['chipKeyModule'].setValue(data.brandInfo.chipKeyModule, {
          onlySelf: true
        });
        this.updateFIinformationForm.controls['AAVAlgorithm'].setValue(data.brandInfo.aavalgorithm.toString() , {
          onlySelf: true
        });
        this.updateFIinformationForm.controls['mcKeyID'].setValue(data.brandInfo.mckeyID.toString(), {
          onlySelf: true
        });
        this.updateFIinformationForm.controls['authPriority'].setValue(data.brandInfo.authPriority, {
          onlySelf: true
        });
        this.updateFIinformationForm.controls['enrollOption'].setValue(data.brandInfo.enrollOption, {
          onlySelf: true
        });

    });
  }
  settingDefaultValues() {
    console.log(this.brandInfo);
    this.updateFIinformationForm.patchValue({
      beginRange : this.brandInfo.beginRange,
      endRange : this.brandInfo.endRange,
      cardType : this.brandInfo.cardType,
      CardRangeName : this.brandInfo.CardRangeName,
      fiBusinessID : this.brandInfo.businessID,
      fiBIN : this.brandInfo.fibin,
      panLength : this.brandInfo.panLength.toString(),
      termPolicyVersion1 : this.brandInfo.termPolicyVersion1 ,
      termPolicyVersion2 : this.brandInfo.termPolicyVersion2 ,
      mobileEnabled : this.brandInfo.mobileEnabled.toString(),
      signingKeyModule : this.brandInfo.signingKeyModule,
      chipKeyModule : this.brandInfo.chipKeyModule,
      AAVAlgorithm : this.brandInfo.aavalgorithm.toString() ,
      mcKeyID : this.brandInfo.mckeyID.toString() ,
      mcKeyAlias : this.brandInfo.mckeyLabel,
      brandingURL1 : this.brandInfo.brandingURL1 ,
      brandingURL2 : this.brandInfo.brandingURL2,
      acsURL1 : this.brandInfo.acsurl1 ,
      ahsReceiptURL : this.brandInfo.ahsreceiptURL,
      signingCertFile : this.brandInfo.signingCertFileData,
      authSelect : this.brandInfo.authSelect.toString(),
      authFallback : this.brandInfo.authFallback.toString(),
      maxAuthTries : this.brandInfo.maxAuthTries.toString(),
      authPriority : this.brandInfo.authPriority,
      nStrikesAcrossSessions : this.brandInfo.nstrikes,
      maxAuthTriesForFYP : this.brandInfo.maxAuthTriesForFYP.toString(),
      pluginURL : this.brandInfo.pluginURL,
      enrollOption : this.brandInfo.enrollOption,
      maxDeclines : this.brandInfo.maxDeclines,
      velogging : this.brandInfo.velogging.toString(),
      maxWelcome : this.brandInfo.maxWelcome,
      amdsCustomerID: this.brandInfo.amdsCustomerId,
      amdsFromNumber: this.brandInfo.fromNumber,
      amdsProviderName: this.brandInfo.providername,
      authResultKeyModule: this.brandInfo.authResultKeyModule
      });
      console.log(this.updateFIinformationForm.value());
  }
  fileConversiontobyteArray(event) {
    const fileinbytearray = [];
    const thisObj = this;
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      console.log(file);
      const reader = new FileReader();
      reader.onload = function () {
        const obj: ArrayBuffer = reader.result as ArrayBuffer;
        const intArray = new Int8Array(obj);
        for (let i = 0; i < intArray.length; i++) {
          fileinbytearray.push(intArray[i]);
        }
        thisObj.fileinbytes = fileinbytearray;
        console.log(fileinbytearray);
      };
      reader.readAsArrayBuffer(file);
    }
  }


  defaultfileConvert(fileinfo) {
    const fileConvert = [];
    const thisObj = this;
    fileConvert.push(fileinfo);
    const blob = new Blob(fileConvert);
    const reader = new FileReader();
    const fileinbytearray = [];
    reader.readAsArrayBuffer(blob);
    reader.onload = function () {
      const obj: ArrayBuffer = reader.result as ArrayBuffer;
      const intArray = new Int8Array(obj);
      for (let i = 0; i < intArray.length; i++) {
        fileinbytearray.push(intArray[i]);
      }
      thisObj.fileinbytes = fileinbytearray;
    };
  }

  updateFIinformation() {
    this.service.updateFIinformation(this.updateFIinformationForm.value, this.fileinbytes).subscribe((data) => {
      console.log(data);
      this.errorMessage = data.message;
    }, (error) => {
      console.log(error);
      this.errorMessage = error.error.errorList[0].errorMsg;
    });
  }
}


