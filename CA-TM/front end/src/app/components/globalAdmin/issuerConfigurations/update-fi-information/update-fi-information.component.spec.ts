import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFiInformationComponent } from './update-fi-information.component';

describe('UpdateFiInformationComponent', () => {
  let component: UpdateFiInformationComponent;
  let fixture: ComponentFixture<UpdateFiInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFiInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFiInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
