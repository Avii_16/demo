import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UpdateIssuerService } from '../../../../services/issuer_configuration_service/update_issuer_service/update-issuer.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { GlobalAdminService } from '../../../../services/global_admin_service/create_global_admin/global-admin.service';

@Component({
  selector: 'app-update-issuer',
  templateUrl: './update-issuer.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class UpdateIssuerComponent implements OnInit {

  updateIssuerForm: FormGroup;
  issuers: any;
  countryName: string;
  _values2: any;
  adminId: boolean;
  checked: boolean;
  selectedAdmins: any = [];
  selectedRemoveAdmins: any = [];
  errorMessage: string;
  users: any;
  $: any;
  defaultLocale: string;
  encryptionKey: string;
  sltAdmin = [];
  bankDirname : string;
  _values1 = [
    { id: 0, name: 'Do not collect' },
    { id: 1, name: 'Only Email Address' },
    { id: 2, name: 'Only Phone Number' },
    { id: 3, name: 'Both Email Address and Phone Number' }
  ];

  constructor(private service: UpdateIssuerService, private fetchDataService: GlobalAdminService , private formBuilder: FormBuilder, private router: Router) {
    this.updateIssuerForm = this.formBuilder.group({
      bankName : [''],
      country : [''],
      localeID : [''],
      localTimeZone : [''],
      customerId : [''],
      bankDirName : [''],
      issuerDisplayName : [''],
      encryptionKey : [''],
      uploadKey : [''],
      uploadKeyHidden : [''],
      retypeUploadKey : [''],
      userEncoding : [''],
      cvvKeyA : [''],
      cvvKeyB : [''],
      bankKeyModule : [''],
      arKeyModule : [''],
      processorName : [''],
      subProcessorName : [''],
      processorData : [''],
      processorInfo : [''],
      CVVKEYIND : [''],
      DoNotPromptBehavior : [''],
      enrollmentLocales : [''],
      userIdEnabled : [''],
      twoStepLoginEnabled : [''],
      dualAuthRequiredAtCommit : [''],
      collectAdminContactDetails : [''],
      twoFactorDeliveryChannelType : [''],
      slt_Admins : [''],
      slt_selAdmins : [''],
    });
    this.updateIssuerForm.controls['bankName'].setValue('0', { onlySelf: true });
    this.updateIssuerForm.controls['country'].setValue('0', { onlySelf: true });
    this.updateIssuerForm.controls['localTimeZone'].setValue('GMT-08:00', { onlySelf: true });
    this.updateIssuerForm.controls['userEncoding'].setValue('ISO-8859-1', { onlySelf: true });
    this.updateIssuerForm.controls['bankKeyModule'].setValue('S/W', { onlySelf: true });
    this.updateIssuerForm.controls['arKeyModule'].setValue('S/W', { onlySelf: true });
    this.updateIssuerForm.controls['CVVKEYIND'].setValue('00', { onlySelf: true });
    this.updateIssuerForm.controls['DoNotPromptBehavior'].setValue('1', { onlySelf: true });
    this.updateIssuerForm.controls['collectAdminContactDetails'].setValue('0', { onlySelf: true });
   }

  ngOnInit() {
    this.fetchAllAdmins();
    this.getAllUserIds();
  }

  doCancel() {
    this.router.navigate(['/index']);
  }

  changeAdmin(value: any) {
    const obj = this._values1[value];
    if (obj.id == 0) {
      this.adminId = false;
    } else {
      this.adminId = true;
    }
  }

  firstDropDownChanged(val: any) {
    const obj = this._values1[val];
    if (!obj) { return; }
    if (obj.id == 1) {
      this._values2 = [
        { id: 2, name: 'EMAIL' }
      ];
    this.updateIssuerForm.controls['twoFactorDeliveryChannelType'].setValue('2', { onlySelf: true });
    } else if (obj.id == 2) {
      this._values2 = [
        { id: 1, name: 'SMS' },
        { id: 3, name: 'VOICE' }
      ];
    this.updateIssuerForm.controls['twoFactorDeliveryChannelType'].setValue('1', { onlySelf: true });
    } else if (obj.id == 3) {
      this._values2 = [
        { id: 4, name: 'EMAIL and SMS' },
        { id: 1, name: 'SMS' },
        { id: 2, name: 'EMAIL' },
        { id: 3, name: 'VOICE' },
        { id: 5, name: 'EMAIL AND VOICE' }
      ];
    this.updateIssuerForm.controls['twoFactorDeliveryChannelType'].setValue('4', { onlySelf: true });
    } else {
      this._values2 = [];
    }
  }

  fetchIssuerDetails() {
    this.service.getIssuerDetails(this.updateIssuerForm.get('bankName').value).subscribe((response) => {
      this.countryName = response.country;
      this.defaultLocale = response.locale;
      this.encryptionKey = response.strEncryptionKey;
      this.selectedAdmins = response.selAdmins;
      this.bankDirname = response.bankDirName;
    // this.country = response.strIssuerDisplayName;
    this.updateIssuerForm.patchValue({ issuerDisplayName : response.strIssuerDisplayName });
    this.updateIssuerForm.patchValue({ processorName : response.strProcessorName });
    this.updateIssuerForm.patchValue({ subProcessorName : response.strSubProcessorName });
    this.updateIssuerForm.patchValue({ processorData : response.strProcessorData });
    this.updateIssuerForm.patchValue({ processorInfo : response.strProcessorInfo });
    this.updateIssuerForm.patchValue({ cvvKeyB : response.strCvvKeyB });
    this.updateIssuerForm.patchValue({ cvvKeyA : response.strCvvKeyA });
    // this.updateIssuerForm.patchValue({ CVVKEYIND : response.strCVVKEYIND });
    this.updateIssuerForm.patchValue({ customerId : response.strCustomerId });
    this.updateIssuerForm.patchValue({ uploadKey : response.strUploadKey });
    this.updateIssuerForm.patchValue({ retypeUploadKey : response.strRetypeUploadKey });
    this.updateIssuerForm.patchValue({ slt_selAdmins : response.selAdmins });
    this.updateIssuerForm.patchValue({ bankDirName : response.bankDirName})
    this.updateIssuerForm.patchValue({encryptionKey : response.strEncryptionKey})
    this.updateIssuerForm.patchValue({ country : response.countryId })
    this.updateIssuerForm.patchValue({ localeID : response.localeID })
      if (response.strCVVKEYIND == ""){
        response.strCVVKEYIND = "00"
      }
    this.updateIssuerForm. controls['CVVKEYIND'].setValue(response.strCVVKEYIND, { onlySelf: true })
    this.updateIssuerForm.controls['localTimeZone'].setValue(response.strTimeZoneLocal, { onlySelf: true });
    this.updateIssuerForm.controls['customerId'].setValue(response.strCustomerId, { onlySelf: true });
    this.updateIssuerForm.controls['twoFactorDeliveryChannelType'].setValue(response.deliveryChannel, { onlySelf: true });
    this.updateIssuerForm.controls['collectAdminContactDetails'].setValue('0', { onlySelf: true });
    this.updateIssuerForm.controls['DoNotPromptBehavior'].setValue(response.strDoNotPromptBehavior, { onlySelf: true });
    this.updateIssuerForm.controls['bankKeyModule'].setValue(response.strBankKeyModule, { onlySelf: true });
    this.updateIssuerForm.controls['arKeyModule'].setValue(response.strArKeyModule, { onlySelf: true });
    this.updateIssuerForm.controls['enrollmentLocales'].setValue(response.vecEnrollmentLocales, { onlySelf: true });
      {
        if (response.strUserEncoding == null) {
          this.updateIssuerForm.controls['userEncoding'].setValue('ISO-8859-1', { onlySelf: true });
        } else {
          this.updateIssuerForm.controls['userEncoding'].setValue(response.strUserEncoding, { onlySelf: true });
        }
      }
      {
        if (response.strUserIdEnabled == 0) {
          this.checked = false;
        } else {
          this.checked = true;
        }
      }

      {
        if (response.strTwoStepLoginEnabled == 0) {
          this.checked = false;
        } else {
          this.checked = true;
        }
      }

      {
        if (response.strDualAuthRequiredAtCommit == 0) {
          this.checked = false;
        } else {
          this.checked = true;
        }
      }

    });
  }

  getUnique(array) {
    const uniqueArray = [];
    for (let i = 0; i < array.length; i++) {
      if (uniqueArray.indexOf(array[i]) === -1) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  }
  addAdmins() {
    for (let i = 0; i < this.updateIssuerForm.get('slt_Admins').value.length; i++) {
      this.selectedAdmins.push(this.updateIssuerForm.get('slt_Admins').value[i]);
    }
    this.selectedAdmins = this.getUnique(this.selectedAdmins);
  }

  removeAdmins() {
    this.selectedRemoveAdmins.length = 0;
    for (let i = 0; i < this.updateIssuerForm.get('slt_selAdmins').value.length; i++) {
      this.selectedRemoveAdmins.push(this.updateIssuerForm.get('slt_selAdmins').value[i]);
    }
    for (let j = 0; j < this.selectedRemoveAdmins.length; j++) {
      this.selectedAdmins.splice($.inArray(this.selectedRemoveAdmins[j], this.selectedAdmins), 1);
    }
  }

  getAllUserIds() {
    this.service.getAllUsers().subscribe((data: any) => {
      this.users = data.response;
    });
  }

  fetchAllAdmins() {
    this.service.getAllIssuers().subscribe((data) => {
      this.issuers = data.response;
      console.log(this.issuers);
    });
  }


  updateIssuer() {
    if (this.updateIssuerForm.get('bankName').value == 0) {
      this.errorMessage = 'Issuer Name required';
    } else {
    this.updateIssuerForm.patchValue({
      slt_selAdmins : this.selectedAdmins
    });
    this.service.updateIssuers(this.updateIssuerForm.value).subscribe((response) => {
      console.log(response);
      this.errorMessage = 'Issuer Updated';
    },
    (error) => {
      console.log(error);
      this.errorMessage = error.error.errorList[0].errorMsg;
      console.log(this.errorMessage);
    });
  }
}
}
