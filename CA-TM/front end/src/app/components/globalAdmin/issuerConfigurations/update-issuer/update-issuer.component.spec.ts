import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateIssuerComponent } from './update-issuer.component';

describe('UpdateIssuerComponent', () => {
  let component: UpdateIssuerComponent;
  let fixture: ComponentFixture<UpdateIssuerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateIssuerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateIssuerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
