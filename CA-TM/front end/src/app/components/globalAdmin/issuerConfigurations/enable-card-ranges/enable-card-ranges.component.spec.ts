import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnableCardRangesComponent } from './enable-card-ranges.component';

describe('EnableCardRangesComponent', () => {
  let component: EnableCardRangesComponent;
  let fixture: ComponentFixture<EnableCardRangesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnableCardRangesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnableCardRangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
