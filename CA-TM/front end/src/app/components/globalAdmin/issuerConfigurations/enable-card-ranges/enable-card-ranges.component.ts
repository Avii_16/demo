import { Component, OnInit } from '@angular/core';
import { EnableCardRangesService } from '../../../../services/issuer_configuration_service/enable_card_ranges_service/enable-card-ranges.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-enable-card-ranges',
  templateUrl: './enable-card-ranges.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class EnableCardRangesComponent implements OnInit {
  issuers: any = [];
  errorMessages: string;
  issuerSelected: boolean;
  enableCardRangesForm: FormGroup;
  bankcode: any;
  ranges: any;
  constructor(private service: EnableCardRangesService, private formbuilder: FormBuilder, private router: Router) {
    this.enableCardRangesForm = this.formbuilder.group({
      issuer: [''],
      rangeIds: ['']
    });
    this.enableCardRangesForm.controls['issuer'].setValue('0',
      { onlySelf: true });
  }

  ngOnInit() {
    this.fetchAllIssuers();
  }
  
  doCancel() {
    this.router.navigate(['/index']);
  }

  fetchAllIssuers() {
    this.service.getIssuers().subscribe((data) => {
      this.issuers = data.enableCardRangesForm.authorizedBanks;
      console.log(this.issuers);
    });
  }

  onChangeIssuer(val: any) {
    if (val == 0) {
      // this.issuerSelected = false;
      // this.admindetails = false;
      // this.errorMessage = "Invalid admin selected."
    } else {
      this.bankcode = this.enableCardRangesForm.get('issuer').value;
      this.getRangeForSelectedBank(this.bankcode);
    }
  }

  getRangeForSelectedBank(bankcode) {
    this.service.getRangeForSelectedIssuer(bankcode).subscribe((data) => {
      console.log(data);
      this.ranges = data.enableCardRangesForm.rangesForBank;
      console.log(this.ranges);

      if (data.errorMessage == 'No card ranges are available to be enabled for the selected issuer.') {
        this.errorMessages = 'No card ranges are available to be enabled for the selected issuer.';
      }
    });
  }

  enableCardRanges() {
    this.service.enableCardRange(this.enableCardRangesForm.value, this.bankcode).subscribe((data) => {
      console.log(data);
      if (data.successMsg == 'Successfully enabled the card ranges.') {
        this.errorMessages = 'Successfully enabled the card ranges.';
      } else {
        this.errorMessages = data.errorMessage;
      }

    });
  }
}
