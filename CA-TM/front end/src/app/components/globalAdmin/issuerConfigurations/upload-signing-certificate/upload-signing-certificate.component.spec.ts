import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadSigningCertificateComponent } from './upload-signing-certificate.component';

describe('UploadSigningCertificateComponent', () => {
  let component: UploadSigningCertificateComponent;
  let fixture: ComponentFixture<UploadSigningCertificateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadSigningCertificateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadSigningCertificateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
