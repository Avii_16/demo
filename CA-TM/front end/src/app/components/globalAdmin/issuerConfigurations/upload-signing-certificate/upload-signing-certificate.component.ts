import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UploadSigningCertificateService } from '../../../../services/issuer_configuration_service/upload_signing_certificate_service/upload-signing-certificate.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-upload-signing-certificate',
  templateUrl: './upload-signing-certificate.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class UploadSigningCertificateComponent implements OnInit {

  ranges: any = [];
  issuers: any = [];
  bankcode: any;
  uploadSigningCertificateForm: FormGroup;
  errorMessages: string;
  fileinbytes: any = [];
  constructor(private service: UploadSigningCertificateService, private formbuilder: FormBuilder,private router: Router) {

    this.uploadSigningCertificateForm = this.formbuilder.group({
      bankId: [''],
      rangeIds: ['']
    });

    this.uploadSigningCertificateForm.controls['bankId'].setValue('-1000', { onlySelf: true });
    this.uploadSigningCertificateForm.controls['rangeIds'].setValue('-1000', { onlySelf: true });
  }

  ngOnInit() {
    this.fetchAllIssuers();
  }
  doCancel() {
    this.router.navigate(['/index']);
  }

  fetchAllIssuers() {
    this.service.getIssuers().subscribe((data) => {
      console.log(data);
      this.issuers = data.enableCardRangesForm.authorizedBanks;
      console.log(this.issuers);
    });
  }

  onChangeIssuer(val: any) {
    this.bankcode = '';
    const settingDefaultValues = {
      rangeIds : ''
    };
    this.uploadSigningCertificateForm.patchValue(settingDefaultValues);

    if (val == 0) {
      // this.issuerSelected = false;
      // this.admindetails = false;
      // this.errorMessage = "Invalid admin selected."
    } else {
      this.bankcode = this.uploadSigningCertificateForm.get('bankId').value;
      this.getRangeForSelectedBank(this.bankcode);
    }
  }

  getRangeForSelectedBank(bankcode) {
    this.service.getRangeForSelectedIssuer(bankcode).subscribe((data) => {
      this.ranges = data.signingCertificateUploadForm.rangesForBank;
      console.log(this.ranges);
      this.errorMessages = '';
    });
  }
  uploadSigningCertificate(file) {
    if ( this.uploadSigningCertificateForm.get('rangeIds').value != '' ) {
    console.log(this.uploadSigningCertificateForm.get('rangeIds').value);
    this.service.uploadSigningCertificate(this.uploadSigningCertificateForm.value, this.bankcode, this.fileinbytes).subscribe((data) => {
      console.log(data);
      if (data.errorList != null) {
        if (data.errorList[0].errorCode == 500 ) {
          this.errorMessages = data.errorList[0].errorMsg;
        }
      } else {
          this.errorMessages = data.message;
        }
    });
  } else {
    this.errorMessages = 'Issuer/Range has not been selected.';
  }
  }

  uploadFile(event) {
    this.fileinbytes = [];
    const fileinbytearray = [];
    const thisObj = this;
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      console.log(file);
      const reader = new FileReader();
      reader.onload = function () {
        const obj: ArrayBuffer = reader.result as ArrayBuffer;
        const intArray = new Int8Array(obj);
        for (let i = 0; i < intArray.length; i++) {
          fileinbytearray.push(intArray[i]);
        }
        thisObj.fileinbytes = fileinbytearray;
        console.log(fileinbytearray);
      };
      reader.readAsArrayBuffer(file);
    }
  }

}
