import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FiInformationService } from '../../../../services/issuer_configuration_service/FI_information_services/fi-information.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-fi-information',
  templateUrl: './add-fi-information.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class AddFIInformationComponent implements OnInit {

  addFIinformationForm: FormGroup;

  errorMessage: string;
  issuers: any = [];
  mcKey: any = [];
  enrollOptions: any = [];
  authPriority: any = [];
  authKeyModule: any = [];
  aAVAlgorithm: any = [];
  chipKeyModule: any = [];
  cardTypelist: any = [];
  cardRangesGroup: any = [];
  signingKeyModule: any = [];
  panLength: any = [];
  defaultvalue = 0;
  authselected = true;
  fileinbytes: any = [];

  constructor(private formBuilder: FormBuilder, private service: FiInformationService, private router: Router ) {
    this.addFIinformationForm = this.formBuilder.group({
      bankName: [''],
      enrollOption: [''],
      CardRangeName: [''],
      cardRangeGroup: [''],
      fiBusinessID: [''],
      fiBIN: [''],
      cardType: [''],
      panLength: [''],
      beginRange: [''],
      endRange: [''],
      termPolicyVersion1: [''],
      termPolicyVersion2: [''],
      mobileEnabled: [''],
      signingKeyModule: [''],
      authResultKeyModule: [''],
      chipKeyModule: [''],
      AAVAlgorithm: [''],
      mcKeyID: [''],
      mcKeyAlias: [''],
      brandingURL1: [''],
      brandingURL2: [''],
      acsURL1: [''],
      acsURL2: [''],
      acsURL3: [''],
      acsURL4: [''],
      acsURL5: [''],
      CAPURLParameters: [''],
      signingCertFile: [''],
      ahsReceiptURL: [''],
      authSelect: [true],
      authFallback: [''],
      maxAuthTries: [''],
      authPriority: [''],
      nStrikesAcrossSessions: [false],
      maxAuthTriesForFYP: [''],
      authMethod: [''],
      pluginURL: [''],
      eAccess: [''],
      pluginName: [''],
      hsmVariant: [''],
      pluginVersion: [''],
      cvvKeyA: [''],
      cvvKeyB: [''],
      CVVKEYIND: [''],
      passwordChangeVelocity: ['0'],
      ActualEnrollOption: ['0'],
      ActualMaxWelcome: ['0'],
      ActualMaxDecline: ['0'],
      backupattempts: [''],
      maxDeclines: [''],
      velogging: [''],
      maxWelcome: [''],
      EnableAttemptsAfterMaxDeclines: [''],
      amdsCustomerID: [''],
      amdsProfilePassword: [''],
      amdsProfileEmail: [''],
      amdsAPINAME: [''],
      amdsFromEmail: [''],
      amdsFromNumber: [''],
      amdsProviderName: [''],
      trustedBeneficiary: [''],
      enableSCA: ['']
    });
    this.addFIinformationForm.controls['bankName'].setValue('0', {
      onlySelf: true
    });
    this.addFIinformationForm.controls['maxAuthTries'].setValue('3', {
      onlySelf: true
    });
    this.addFIinformationForm.controls['mobileEnabled'].setValue('1', {
      onlySelf: true
    });
    this.addFIinformationForm.controls['termPolicyVersion1'].setValue('1', {
      onlySelf: true
    });
    this.addFIinformationForm.controls['termPolicyVersion2'].setValue('0', {
      onlySelf: true
    });
    this.addFIinformationForm.controls['maxAuthTriesForFYP'].setValue('0', {
      onlySelf: true
    });
  }

  ngOnInit() {
    this.getBankInfo();
    this.fetchAllIssuers();
  }

  doCancel() {
    this.router.navigate(['/index']);
  }
  fetchAllIssuers() {
    this.service.getIssuers().subscribe((data) => {
      this.issuers = data.enableCardRangesForm.authorizedBanks;
      console.log(this.issuers);
    });
  }
  getBankInfo() {

    let sigingmodule = [];
    let authmodule = [];
    let chipmodule = [];
    let aavalgorithm = [];
    let authpriority = [];
    let cardgroup = [];
    let enroll = [];
    const key = ['S/W', 'nfast', 'cca'];
    const chipkey = ['nfast', 'zaxus', '-1'];
    const authkey = ['S/W', 'spp', 'zaxus', 'ibm4758'];
    const aavakey = ['-1', '0', '8'];
    const authprioritykey = ['', '2;1', '1', '2', '4;1', '4;2;1'];
    const cardkey = ['13', '0', '15', '3', '17', '45', '53', '55', '68'];
    const enrollkey = ['0', '1', '7', '2', '70', '20'];
    this.service.getBankList().subscribe((data) => {
      this.mcKey = data.mcKey;
      authpriority = data.authPriority;
      enroll = data.enrollOptions;
      aavalgorithm = data.aAVAlgorithm;
      authmodule = data.authKeyModule;
      chipmodule = data.chipKeyModule;
      cardgroup = data.cardRangesGroup;
      this.cardTypelist = data.cardTypelist;
      sigingmodule = data.signingKeyModule;
      this.panLength = data.panLength;
      for (let i = 0; i < cardgroup.length; i++) {
        this.cardRangesGroup.push({
          value: cardgroup[i],
          key: cardkey[i]
        });
        if (i == cardgroup.length - 1) {
          this.cardRangesGroup.push({
            value: 'None',
            key: '-1'
          });
        }
      }
      for (let i = 0; i < sigingmodule.length; i++) {
        if (sigingmodule[i] == '') {
          this.signingKeyModule.push({
            value: '-- --',
            key: key[i]
          });
        } else {
          this.signingKeyModule.push({
            value: sigingmodule[i],
            key: key[i]
          });
        }
      }
      for (let i = 0; i < authmodule.length; i++) {
        this.authKeyModule.push({
          value: authmodule[i],
          key: authkey[i]
        });
      }
      for (let i = 0; i < chipmodule.length; i++) {
        this.chipKeyModule.push({
          value: chipmodule[i],
          key: chipkey[i]
        });
        if (i == chipmodule.length - 1) {
          this.chipKeyModule.push({
            value: 'None',
            key: '-1'
          });
        }
      }
      for (let i = 0; i < aavalgorithm.length; i++) {
        this.aAVAlgorithm.push({
          value: aavalgorithm[i],
          key: aavakey[i]
        });
      }
      for (let i = 0; i < authpriority.length; i++) {
        this.authPriority.push({
          value: authpriority[i],
          key: authprioritykey[i]
        });
      }
      for (let i = 0; i < enroll.length; i++) {
        this.enrollOptions.push({
          value: enroll[i],
          key: enrollkey[i]
        });
      }
      this.addFIinformationForm.controls['cardRangeGroup'].setValue('0', {
        onlySelf: true
      });
      this.addFIinformationForm.controls['cardType'].setValue('1', {
        onlySelf: true
      });
      this.addFIinformationForm.controls['panLength'].setValue('16', {
        onlySelf: true
      });
      this.addFIinformationForm.controls['signingKeyModule'].setValue('S/W', {
        onlySelf: true
      });
      this.addFIinformationForm.controls['authResultKeyModule'].setValue('S/W', {
        onlySelf: true
      });
      this.addFIinformationForm.controls['chipKeyModule'].setValue('-1', {
        onlySelf: true
      });
      this.addFIinformationForm.controls['AAVAlgorithm'].setValue('-1', {
        onlySelf: true
      });
      this.addFIinformationForm.controls['mcKeyID'].setValue('0', {
        onlySelf: true
      });
      this.addFIinformationForm.controls['authPriority'].setValue('-1', {
        onlySelf: true
      });

      this.addFIinformationForm.controls['enrollOption'].setValue('0', {
        onlySelf: true
      });
    });
  }

  fileConversiontobyteArray(event) {
    const fileinbytearray = [];
    const thisObj = this;
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      console.log(file);
      const reader = new FileReader();
      reader.onload = function () {
        const obj: ArrayBuffer = reader.result as ArrayBuffer;
        const intArray = new Int8Array(obj);
        for (let i = 0; i < intArray.length; i++) {
          fileinbytearray.push(intArray[i]);
        }
        thisObj.fileinbytes = fileinbytearray;
        console.log(fileinbytearray);
      };
      reader.readAsArrayBuffer(file);
    }
  }
  addFIInformation() {
    // this.service.addFiInformation(this.addFIinformationForm.value);
    console.log(this.addFIinformationForm.value);
    this.service.addFiInformation(this.addFIinformationForm.value, this.fileinbytes).subscribe((data) => {
      this.errorMessage = data.message;
    }, (error) => {
      this.errorMessage = error.error.errorList[0].errorMsg;
      console.log(this.errorMessage);
    }
    );
  }

}
