import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFIInformationComponent } from './add-fi-information.component';

describe('AddFIInformationComponent', () => {
  let component: AddFIInformationComponent;
  let fixture: ComponentFixture<AddFIInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFIInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFIInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
