import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureIssuerParametersComponent } from './configure-issuer-parameters.component';

describe('ConfigureIssuerParametersComponent', () => {
  let component: ConfigureIssuerParametersComponent;
  let fixture: ComponentFixture<ConfigureIssuerParametersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureIssuerParametersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureIssuerParametersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
