import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UpdateIssuerService } from '../../../../services/issuer_configuration_service/update_issuer_service/update-issuer.service';
// tslint:disable-next-line: max-line-length
import { ConfigureIssuerParametersService } from '../../../../services/issuer_configuration_service/configure_issuer_parameters_services/configure-issuer-parameters.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-configure-issuer-parameters',
  templateUrl: './configure-issuer-parameters.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class ConfigureIssuerParametersComponent implements OnInit {

  showParametersForm = false;
  configureissuerParameterForm: FormGroup;
  IssuerForm: FormGroup;
  issuers: any;
  checked: boolean;
  errorMessage: string;
  iSupportUpgradedCAPUI: boolean;
  sendTempPasswordToCH: boolean;
  iEnableAbridgedADS: boolean;
  iPasswordUsagePolicy: boolean;
  iSimultaneousCHLogin: boolean;
  iEnrollmentOfLockedCard: boolean;
  iEnableRiskFort: boolean;
  iRFIntermediatePage: boolean;
  iEnableArgus: boolean;
  iCollectMCDDNA: boolean;
  iVELOG: boolean;
  iLogVEReqPAReqTime: boolean;
  iLogPreviousTxnDetails: boolean;
  iLogRFStatus: boolean;
  iEnableCaseAutoAssignment: boolean;
  iHAWithBackupServers: boolean;
  maxTransactionsWithoutAOTP: boolean;
  suppressCHContactDetailsInput: boolean;
  iUACheckEnabled: boolean;
  callOutValues: any = [];
  riskEnabled = false;
  fieldsEnable = false;
  caseEnabled = false;
  authFields = false;
  priority: boolean;
  // tslint:disable-next-line: max-line-length
  constructor(private formBuilder: FormBuilder, private router: Router, private getService: UpdateIssuerService, private service: ConfigureIssuerParametersService) {
    this.IssuerForm = this.formBuilder.group({
      bankName: [''],
      formattedDate: [''],
      dateOrder: [''],
      timeStampFormat: [''],
      recordsPerPage: [''],
      symbolLink: [''],
      symbolDisplay: [''],
      passPhrase: [''],
      retypeDUPassPhrase: [''],
      dateFormat: [''],
      dateSeparator: [''],
      SupportUpgradedCAPUI: [''],
      tempPwdDuration: [''],
      sendTempPasswordToCH: [''],
      EnableAbridgedADS: [''],
      enableAbridgedFyp: [''],
      pwdUsagePolicy: [''],
      enrollmentLockingPolicy: [''],
      maxValForLastPwdList: [''],
      promptNPasswordCharacters: [''],
      simultaneousCHLogin: [''],
      enrollmentOfLockedCard: [''],
      pareqWaitTimeBuffer: [''],
      enableRiskFort: [''],
      raDownHandleAction: [''],
      RFIntermediatePage: [''],
      ENABLEARGUS: [''],
      BrandRFIntermediatePage: [''],
      cookieType: [''],
      CollectMCDDNA: [''],
      VELOG: [''],
      LogVEReqPAReqTime: [''],
      LogPreviousTxnDetails: [''],
      LogRFStatus: [''],
      EnableCaseAutoAssignment: [''],
      autoAssignmentForPriority: [''],
      caseGroupingForPriority: [''],
      includeClosedCases: [''],
      emailPIIType: [''],
      pamPIIType: [''],
      issuerAnsPIIType: [''],
      haWithBackupServers: [''],
      arcotOtpEnabled: [''],
      authMinderOrgName: [''],
      arcotOtpImagesRelativePath: [''],
      arcotOTPProfileName: [''],
      arcotEMVAccountType: [''],
      authminderFailOverOptions: [''],
      maxNonAOTPTxns: [''],
      maxTransactionsWithoutAOTPTemp: [''],
      maxTransactionsWithoutAOTP: [''],
      displayOptionalInfo: [''],
      suppressCHContactDetailsInput: [''],
      uaCheckEnabled: [''],
      maxChangePwdTries: [''],
      passwordStorageAlgorithm: [''],
      calloutAuthMethod: [''],
      ENABLESCA: [''],
      TRUSTEDBENEFICIARY: [''],
      amdsProviderName: [''],
      amdsFromNumber: [''],
      amdsFromEmail: [''],
      amdsAPINAME: [''],
      amdsProfileEmail: [''],
      amdsProfilePassword: [''],
      amdsCustomerID: [''],
    });
    // this.configureissuerParameterForm.controls['bankName'].setValue('0', { onlySelf: true });
  }
  ngOnInit() {
    this.fetchAllAdmins();
  }
  doCancel() {
    this.router.navigate(['/index']);
  }

  fetchAllAdmins() {
    this.service.getAllIssuers().subscribe((data) => {
      this.issuers = data.response;
      console.log(this.issuers);
    });
  }

  onIssuerSelect() {
    // tslint:disable-next-line: triple-equals
    if (this.IssuerForm.get('bankName').value == 0) {
      this.showParametersForm = false;
    } else {
      this.showParametersForm = true;
      this.service.getIssuerParams(this.IssuerForm.get('bankName').value).subscribe((response) => {
        for (let index = 0 ; index < response.calloutAMValues.length; index++) {
          this.callOutValues.push(
            {
              'id' : response.calloutAMValues[index],
              'value' : response.calloutAMParams[index]
            }
          );
        }
        this.getDateSeperator(response.strDateFormat);
        console.log(this.callOutValues);
        this.IssuerForm.patchValue({
          passPhrase: response.strPassPhrase,
          retypeDUPassPhrase: response.strRetypeDUPassPhrase,
          tempPwdDuration: response.iTempPasswordDuration,
          pareqWaitTimeBuffer: response.iPAReqWaitTimeBuffer,
          amdsProviderName: response.amdsProviderName,
          amdsFromNumber: response.amdsFromNumber,
          amdsFromEmail: response.amdsFromEmail,
          amdsAPINAME: response.amdsAPINAME,
          amdsProfileEmail: response.amdsProfileEmail,
          amdsProfilePassword: response.amdsProfilePassword,
          amdsCustomerID: response.amdsCustomerID,
          authMinderOrgName: response.authMinderOrgName,
          arcotOtpImagesRelativePath: response.arcotOtpImagesRelativePath,
          arcotOTPProfileName: response.arcotOTPProfileName,
          arcotEMVAccountType: response.arcotEMVAccountType,
          maxTransactionsWithoutAOTPTemp: response.maxTransactionsWithoutAOTPtxt,
          // checkboxes
          // SupportUpgradedCAPUI: response.iSupportUpgradedCAPUI,
          // sendTempPasswordToCH: response.sendTempPasswordToCH,
          // EnableAbridgedADS: response.iEnableAbridgedADS,
          // simultaneousCHLogin: response.iSimultaneousCHLogin,
          // enrollmentOfLockedCard: response.iEnrollmentOfLockedCard,
          // enableRiskFort: response.iEnableRiskFort,
          // RFIntermediatePage: response.iRFIntermediatePage,
          // ENABLEARGUS: response.iEnableArgus,
          // CollectMCDDNA: response.iCollectMCDDNA,
          // VELOG: response.iVELOG,
          // LogVEReqPAReqTime: response.iLogVEReqPAReqTime,
          // LogPreviousTxnDetails: response.iLogPreviousTxnDetails,
          // LogRFStatus: response.iLogRFStatus,
          // EnableCaseAutoAssignment: response.iEnableCaseAutoAssignment,
          // haWithBackupServers: response.iHAWithBackupServers,
          // maxTransactionsWithoutAOTP: response.maxTransactionsWithoutAOTP,
          // suppressCHContactDetailsInput: response.suppressCHContactDetailsInput,
          // uaCheckEnabled: response.iUACheckEnabled,
          // pwdUsagePolicy: response.iPasswordUsagePolicy
        });
        this.IssuerForm.controls['dateOrder'].setValue(response.iDateOrder, { onlySelf: true });
        this.IssuerForm.controls['timeStampFormat'].setValue(response.strTimeStampFormat, { onlySelf: true });
        this.IssuerForm.controls['recordsPerPage'].setValue(response.iRecordsPerPage, { onlySelf: true });
        this.IssuerForm.controls['symbolLink'].setValue(response.iSymbolLink, { onlySelf: true });
        this.IssuerForm.controls['symbolDisplay'].setValue(response.iSymbolDisplay, { onlySelf: true });
        this.IssuerForm.controls['dateFormat'].setValue(response.strDateFormat, { onlySelf: true });
        // this.IssuerForm.controls['dateSeparator'].setValue(response.iSymbolDisplay, { onlySelf: true });
        this.IssuerForm.controls['enrollmentLockingPolicy'].setValue(response.iEnrollmentLockingPolicy, { onlySelf: true });
        this.IssuerForm.controls['maxValForLastPwdList'].setValue(response.iMaxValForLastPwdList, { onlySelf: true });
        this.IssuerForm.controls['promptNPasswordCharacters'].setValue(response.iMaxCharsToPrompt, { onlySelf: true });
        this.IssuerForm.controls['raDownHandleAction'].setValue(response.iRADownHandleAction, { onlySelf: true });
        this.IssuerForm.controls['BrandRFIntermediatePage'].setValue(response.iBrandRFIntermediatePage, { onlySelf: true });
        this.IssuerForm.controls['cookieType'].setValue(response.iCookieType, { onlySelf: true });
        this.IssuerForm.controls['emailPIIType'].setValue(response.emailPIIType, { onlySelf: true });
        this.IssuerForm.controls['pamPIIType'].setValue(response.pamPIIType, { onlySelf: true });
        this.IssuerForm.controls['issuerAnsPIIType'].setValue(response.issuerAnsPIIType, { onlySelf: true });
        this.IssuerForm.controls['arcotOtpEnabled'].setValue(response.iArcotOtpEnabled, { onlySelf: true });
        this.IssuerForm.controls['authminderFailOverOptions'].setValue(response.authminderFailOverOptions, { onlySelf: true });
        this.IssuerForm.controls['displayOptionalInfo'].setValue(response.displayOptionalInfo, { onlySelf: true });
        this.IssuerForm.controls['maxChangePwdTries'].setValue(response.iMAXCHANGEPWDFAILURES, { onlySelf: true });
        this.IssuerForm.controls['passwordStorageAlgorithm'].setValue(response.iPasswordStorageType, { onlySelf: true });
        this.IssuerForm.controls['calloutAuthMethod'].setValue(response.selCalloutAuthMethod, { onlySelf: true });
        this.IssuerForm.controls['ENABLESCA'].setValue(response.iEnableSCA, { onlySelf: true });
        this.IssuerForm.controls['TRUSTEDBENEFICIARY'].setValue(response.iEnableTrustedBeneficiary, { onlySelf: true });
        {
          if (response.iArcotOtpEnabled != 0) {
            this.authFields = true;
          } else {
            this.authFields = false;
          }
        }
        {
          if (response.iSupportUpgradedCAPUI == 0) {
            this.iSupportUpgradedCAPUI = false;
          } else {
            this.iSupportUpgradedCAPUI = true;
          }
        }

        {
          if (response.sendTempPasswordToCH == 0) {
            this.sendTempPasswordToCH = false;
          } else {
            this.sendTempPasswordToCH = true;
          }
        }

        {
          if (response.iEnableAbridgedADS == 0) {
            this.iEnableAbridgedADS = false;
          } else {
            this.iEnableAbridgedADS = true;
          }
        }

        {
          if (response.iPasswordUsagePolicy == 0) {
            this.iPasswordUsagePolicy = false;
          } else {
            this.iPasswordUsagePolicy = true;
          }
        }

        {
          if (response.iSimultaneousCHLogin == 0) {
            this.iSimultaneousCHLogin = false;
          } else {
            this.iSimultaneousCHLogin = true;
          }
        }

        {
          if (response.iEnrollmentOfLockedCard == 0) {
            this.iEnrollmentOfLockedCard = false;
          } else {
            this.iEnrollmentOfLockedCard = true;
          }
        }

        {
          if (response.iEnableRiskFort == 0) {
            this.iEnableRiskFort = false;
            this.riskEnabled = false;
          } else {
            this.iEnableRiskFort = true;
            this.riskEnabled = true;
          }
        }

        {
          if (response.iRFIntermediatePage == 0) {
            this.iRFIntermediatePage = false;
            this.fieldsEnable = false;
          } else {
            this.iRFIntermediatePage = true;
            this.fieldsEnable = true;
          }
        }

        {
          if (response.iEnableArgus == 0) {
            this.iEnableArgus = false;
          } else {
            this.iEnableArgus = true;
          }
        }

        {
          if (response.iCollectMCDDNA == 0) {
            this.iCollectMCDDNA = false;
          } else {
            this.iCollectMCDDNA = true;
          }
        }

        {
          if (response.iVELOG == 0) {
            this.iVELOG = false;
          } else {
            this.iVELOG = true;
          }
        }

        {
          if (response.iLogVEReqPAReqTime == 0) {
            this.iLogVEReqPAReqTime = false;
          } else {
            this.iLogVEReqPAReqTime = true;
          }
        }

        {
          if (response.iLogPreviousTxnDetails == 0) {
            this.iLogPreviousTxnDetails = false;
          } else {
            this.iLogPreviousTxnDetails = true;
          }
        }

        {
          if (response.iLogRFStatus == 0) {
            this.iLogRFStatus = false;
          } else {
            this.iLogRFStatus = true;
          }
        }

        {
          if (response.iEnableCaseAutoAssignment == 0) {
            this.iEnableCaseAutoAssignment = false;
          } else {
            this.iEnableCaseAutoAssignment = true;
          }
        }

        {
          if (response.iHAWithBackupServers == 0) {
            this.iHAWithBackupServers = false;
          } else {
            this.iHAWithBackupServers = true;
          }
        }

        {
          if (response.maxTransactionsWithoutAOTP == 0) {
            this.maxTransactionsWithoutAOTP = false;
          } else {
            this.maxTransactionsWithoutAOTP = true;
          }
        }

        {
          if (response.suppressCHContactDetailsInput == 0) {
            this.suppressCHContactDetailsInput = false;
          } else {
            this.suppressCHContactDetailsInput = true;
          }
        }

        {
          if (response.iUACheckEnabled == 0) {
            this.iUACheckEnabled = false;
          } else {
            this.iUACheckEnabled = true;
          }
        }

        {
          if (response.enableAbidegedFYP == 0) {
            this.checked = false;
          } else {
            this.checked = true;
          }
        }
        {
          if (response.casePrioritization == 0) {
            this.priority = false;
            this.caseEnabled = false;
          } else {
            this.priority = true;
            this.caseEnabled = true;
          }
        }
        console.log(response);
      });
    }
  }

  getDateSeperator(dateFormat) {
    const dateSeperator = dateFormat.charAt(2);
    // if (dateSeperator == )
    this.IssuerForm.controls['dateSeparator'].setValue(dateSeperator, { onlySelf: true });
  }

  enableFields(value: any) {
    console.log(value);
    if (value == true) {
      this.riskEnabled = true;
    } else {
      this.riskEnabled = false;
    }
  }

  activateFields(value: any) {
    if (value == true) {
      this.fieldsEnable = true;
    } else {
      this.fieldsEnable = false;
    }
  }

  enableCases(value: any) {
    if (value == true) {
      this.caseEnabled = true;
    } else {
      this.caseEnabled = false;
    }
  }

  EnableAuthMinderFields(value: any) {
    if (window.confirm('Warning: Ideally Mobile OTP Type should not be changed. These changes for \"Mobile OTP Type\" will be applicable to all ranges.') == true) {
      if (value == 0) {
        this.authFields = false;
      } else {
        this.authFields = true;
      }
    }
  }

  configureParams() {
    // this.service.submitEditParams(this.IssuerForm.value);
    // console.log(this.IssuerForm.value);
    this.service.submitEditParams(this.IssuerForm.value).subscribe((response) => {
      console.log(response);
      this.errorMessage = 'Issuer Configuration Parameters updated';
    },
      (error) => {
        if (error.error.errorList[0].errorMsg == '') {
          this.errorMessage = 'Error in updating Issuer Parameters';
        } else {
        this.errorMessage = error.error.errorList[0].errorMsg;
      }
    });
  }
}
