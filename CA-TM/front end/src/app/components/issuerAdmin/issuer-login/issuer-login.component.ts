import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthServiceService } from '../../../services/auth_service/auth-service.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-issuer-login',
  templateUrl: './issuer-login.component.html',
  styleUrls: ['./issuer-login.component.css']
})
export class IssuerLoginComponent implements OnInit {

  loginForm: FormGroup;
  errorMessage: string;
  showPopup = true;
  sessionData: any;
  bank: string;
  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private service: AuthServiceService, private routes: Router) {
    this.loginForm = this.formBuilder.group({
      adminName: [''],
      password: ['']
    });
  }
  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.bank = params['bank'];
    });
  }

  doLogin() {
    localStorage.setItem('password', this.loginForm.get('password').value);
    localStorage.setItem('username', this.loginForm.get('adminName').value);
    this.service.doIssuerLogin(this.loginForm.value, this.bank).subscribe((response) => {
      if (response.statusCode == 1) {
        this.sessionData = response;
        localStorage.setItem('token', response.token);
        localStorage.setItem('entered', 'true');
        localStorage.setItem('path', window.location.href);
        localStorage.setItem('sessionData', JSON.stringify(this.sessionData));
        window.location.href = '/index';
      } else if (response.statusCode == 2) {
        this.sessionData = response;
        localStorage.setItem('token', response.token);
        localStorage.setItem('entered', 'true');
        localStorage.setItem('path', window.location.href);
        localStorage.setItem('sessionData', JSON.stringify(this.sessionData));
        window.location.href = '/update_profile/first_login';
      } else if (response.statusCode == 3) {
        this.sessionData = response;
        localStorage.setItem('token', response.token);
        localStorage.setItem('entered', 'true');
        localStorage.setItem('path', window.location.href);
        localStorage.setItem('sessionData', JSON.stringify(this.sessionData));
        window.location.href = '/index';
      } else if (response.statusCode == 4) {
        this.sessionData = response;
        localStorage.setItem('token', response.token);
        localStorage.setItem('entered', 'true');
        localStorage.setItem('path', window.location.href);
        localStorage.setItem('firstlogin', 'true');
        localStorage.setItem('sessionData', JSON.stringify(this.sessionData));
        window.location.href = '/change_password';
      } else if (response.statusCode == -3) {
        this.errorMessage = 'We are sorry, the Database Service is currently not available, please try after some time';
      } else {
        this.errorMessage = 'Invalid Credentials supplied or account is locked';
      }
    });
  }
  hidePopup() {
    this.showPopup = false;
  }

}
