// Imports
import { Component, OnInit } from '@angular/core';

// Component Declaration
@Component({
  selector:     'app-root',
  templateUrl:  './app.component.html',
  styleUrls:    ['./app.component.css']
})
export class AppComponent implements OnInit {

  loggedIn = false;
  token: string = localStorage.getItem('token');
  firstPasswordLogin:boolean=localStorage.getItem('firstlogin') == 'true' ? true : false;
  // default constructor
  constructor() {
  }

  // lifecycle method ngOnInit()
  ngOnInit() {

    if (window.location.href === localStorage.getItem('path')) {
      localStorage.setItem('entered', 'false');
      localStorage.removeItem('token');
      localStorage.removeItem('path');
    }

    // check login status and set
    if (this.token != null && localStorage.getItem('entered') === 'true') {
      if (localStorage.getItem('tempPasswordDuration') == '-3' || localStorage.getItem('tempPasswordDuration') == '0'  && this.firstPasswordLogin == true) {
        this.loggedIn = false;
      } else if (localStorage.getItem('firstlogin') == 'true') {
        this.loggedIn = false;
      } else if (localStorage.getItem('update') == 'updatePage') {
        this.loggedIn = false;
      } else if (localStorage.getItem('updated') == 'updated') {
        this.loggedIn = true;
      } else {
        this.loggedIn = true;
      }
    } else {
      this.loggedIn = false;
    }
  }
}
