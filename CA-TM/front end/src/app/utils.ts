export default class Utils {
  /**
   * @param data is an array containing the get params to be sent
   * @returns a string representing HTTP get params
   */
  static encodeQueryData(data) {
    const ret = [];
    for (const d in data) {
      if (d) {
        ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
      }
    }
    return ret.join('&');
  }

  /**
   * @param map is a JS object containing key value pairs
   * @returns an array of the keys of the JS object sent
   */
  static getKeys(map: any) {
    const keysArray = Object.keys(map);
    return keysArray;
  }
}
