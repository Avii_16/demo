// Angular imports
import { NgModule }                               from '@angular/core';
import { Routes, RouterModule }                   from '@angular/router';

// Component imports 

// Auth components
import { LoginpageComponent }                     from './components/globalAdmin/loginpage/loginpage.component';
import { MasterAdminLoginComponent }              from './components/masterAdmin/master-admin-login/master-admin-login.component';

// Common components
import { IndexComponent }                         from './components/commonFiles/index/index.component';
import { ChangePasswordComponent }                from './components/commonFiles/change-password/change-password.component';
import { UpdateProfileComponent }                 from './components/commonFiles/update-profile/update-profile.component';
import { HelpComponent }                          from './components/commonFiles/help/help.component';
import { FormInfoComponent }                      from './components/commonFiles/form-info/form-info.component';

// Issuer Configuration components
import { CreateIssuerComponent }                  from './components/globalAdmin/issuerConfigurations/create-issuer/create-issuer.component';
import { AddFIInformationComponent }              from './components/globalAdmin/issuerConfigurations/add-fi-information/add-fi-information.component';
import { UpdateFiInformationComponent }           from './components/globalAdmin/issuerConfigurations/update-fi-information/update-fi-information.component';
import { AddIssuerCustomizationComponent }        from './components/globalAdmin/issuerConfigurations/add-issuer-customization/add-issuer-customization.component';
import { UpdateIssuerComponent }                  from './components/globalAdmin/issuerConfigurations/update-issuer/update-issuer.component';
import { UploadSigningCertificateComponent }      from './components/globalAdmin/issuerConfigurations/upload-signing-certificate/upload-signing-certificate.component';
import { EnableCardRangesComponent }              from './components/globalAdmin/issuerConfigurations/enable-card-ranges/enable-card-ranges.component';
import { ConfigureIssuerParametersComponent }     from './components/globalAdmin/issuerConfigurations/configure-issuer-parameters/configure-issuer-parameters.component';

// GlobalAdmin components
import { CreateGlobalAdminComponent }             from './components/globalAdmin/globalAdminConfigurations/create-global-admin/create-global-admin.component';
import { ModifyGlobalAdminComponent }             from './components/globalAdmin/globalAdminConfigurations/modify-global-admin/modify-global-admin.component';
import { ResetGlobalAdminComponent }              from './components/globalAdmin/globalAdminConfigurations/reset-global-admin/reset-global-admin.component';
import { UpdateGlobalAdminComponent }             from './components/globalAdmin/globalAdminConfigurations/update-global-admin/update-global-admin.component';

// Issuer Admin components
import { CreateIssuerAdminComponent }             from './components/globalAdmin/issuerAdminConfigurations/create-issuer-admin/create-issuer-admin.component';
import { ConfigureIssuerAdminPolicyComponent }    from './components/globalAdmin/issuerAdminConfigurations/configure-issuer-admin-policy/configure-issuer-admin-policy.component';
import { ModifyIssuerAdminAccountComponent }      from './components/globalAdmin/issuerAdminConfigurations/modify-issuer-admin-account/modify-issuer-admin-account.component';
import { ResetIssuerAdminPasswordComponent }      from './components/globalAdmin/issuerAdminConfigurations/reset-issuer-admin-password/reset-issuer-admin-password.component';
import { UpdateIssuerAdminPrevilegesComponent }   from './components/globalAdmin/issuerAdminConfigurations/update-issuer-admin-previleges/update-issuer-admin-previleges.component';
import { ConfigureIssuerAdminPrevilegesComponent }from './components/globalAdmin/issuerAdminConfigurations/configure-issuer-admin-previleges/configure-issuer-admin-previleges.component';

//Issuer Admin
import { IssuerLoginComponent }                   from './components/issuerAdmin/issuer-login/issuer-login.component';



// All components packed into routes
const routes: Routes = [
  { path: '',                                       component: LoginpageComponent  },
  { path: 'masterAdminLogin',                       component: MasterAdminLoginComponent },
  { path: 'issuerLogin',                            component: IssuerLoginComponent },
  
  { path: 'index',                                  component: IndexComponent },
  { path: 'change_password',                        component: ChangePasswordComponent },
  { path: 'update_profile',                         component: UpdateProfileComponent },
  { path: 'update_profile/first_login',             component: UpdateProfileComponent },
  { path: 'help',                                   component: HelpComponent },
  { path: 'formInfo',                               component: FormInfoComponent },

  { path: 'isco/create_issuer',                     component: CreateIssuerComponent },
  { path: 'isco/add_FI_information',                component: AddFIInformationComponent },
  { path: 'isco/update_FI_information',             component: UpdateFiInformationComponent },
  { path: 'isco/add_issuer_customization',          component: AddIssuerCustomizationComponent },
  { path: 'isco/configure_issuer_parameters',       component: ConfigureIssuerParametersComponent },
  { path: 'isco/update_issuer',                     component: UpdateIssuerComponent },
  { path: 'isco/upload_signing_certificate',        component: UploadSigningCertificateComponent },
  { path: 'isco/enable_card_ranges',                component: EnableCardRangesComponent },

  { path: 'gaco/global_admin/create',               component: CreateGlobalAdminComponent },
  { path: 'gaco/global_admin/modify',               component: ModifyGlobalAdminComponent },
  { path: 'gaco/global_admin/reset',                component: ResetGlobalAdminComponent },
  { path: 'gaco/global_admin/update',               component: UpdateGlobalAdminComponent },

  { path: 'iaco/issuer_admin/create',               component: CreateIssuerAdminComponent },
  { path: 'iaco/issuer_admin/configure_policy',     component: ConfigureIssuerAdminPolicyComponent },
  { path: 'iaco/issuer_admin/modify',               component: ModifyIssuerAdminAccountComponent },
  { path: 'iaco/issuer_admin/reset_password',       component: ResetIssuerAdminPasswordComponent },
  { path: 'iaco/issuer_admin/update_privilages',    component: UpdateIssuerAdminPrevilegesComponent },
  { path: 'iaco/issuer_admin/configure_privilages', component: ConfigureIssuerAdminPrevilegesComponent }
];

// Initializing the router
@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [ RouterModule ]
})

// exporting the SPA routing module
export class AppRoutingModule {
}
