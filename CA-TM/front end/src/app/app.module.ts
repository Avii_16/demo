import { BrowserModule }                            from '@angular/platform-browser';
import { NgModule }                                 from '@angular/core';
import { ReactiveFormsModule }                      from '@angular/forms';
import { HttpClientModule }                         from '@angular/common/http';

import { AppRoutingModule }                         from './app-routing.module';
import { AppComponent }                             from './app.component';

import { SidenavComponent }                         from './components/commonFiles/sidenav/sidenav.component';
import { HeaderComponent }                          from './components/commonFiles/header/header.component';
import { IndexComponent }                           from './components/commonFiles/index/index.component';
import { ChangePasswordComponent }                  from './components/commonFiles/change-password/change-password.component';
import { HelpComponent }                            from './components/commonFiles/help/help.component';
import { UpdateProfileComponent }                   from './components/commonFiles/update-profile/update-profile.component';
import { FormInfoComponent }                        from './components/commonFiles/form-info/form-info.component';

import { LoginpageComponent }                       from './components/globalAdmin/loginpage/loginpage.component';

import { CreateIssuerComponent }                    from './components/globalAdmin/issuerConfigurations/create-issuer/create-issuer.component';
import { AddFIInformationComponent }                from './components/globalAdmin/issuerConfigurations/add-fi-information/add-fi-information.component';
import { UpdateFiInformationComponent }             from './components/globalAdmin/issuerConfigurations/update-fi-information/update-fi-information.component';
import { AddIssuerCustomizationComponent }          from './components/globalAdmin/issuerConfigurations/add-issuer-customization/add-issuer-customization.component';
import { UpdateIssuerComponent }                    from './components/globalAdmin/issuerConfigurations/update-issuer/update-issuer.component';
import { UploadSigningCertificateComponent }        from './components/globalAdmin/issuerConfigurations/upload-signing-certificate/upload-signing-certificate.component';
import { EnableCardRangesComponent }                from './components/globalAdmin/issuerConfigurations/enable-card-ranges/enable-card-ranges.component';

import { CreateGlobalAdminComponent }               from './components/globalAdmin/globalAdminConfigurations/create-global-admin/create-global-admin.component';
import { ModifyGlobalAdminComponent }               from './components/globalAdmin/globalAdminConfigurations/modify-global-admin/modify-global-admin.component';
import { ResetGlobalAdminComponent }                from './components/globalAdmin/globalAdminConfigurations/reset-global-admin/reset-global-admin.component';
import { UpdateGlobalAdminComponent }               from './components/globalAdmin/globalAdminConfigurations/update-global-admin/update-global-admin.component';

import { CreateIssuerAdminComponent }               from './components/globalAdmin/issuerAdminConfigurations/create-issuer-admin/create-issuer-admin.component';
import { ModifyIssuerAdminAccountComponent }        from './components/globalAdmin/issuerAdminConfigurations/modify-issuer-admin-account/modify-issuer-admin-account.component';
import { ResetIssuerAdminPasswordComponent }        from './components/globalAdmin/issuerAdminConfigurations/reset-issuer-admin-password/reset-issuer-admin-password.component';
import { UpdateIssuerAdminPrevilegesComponent }     from './components/globalAdmin/issuerAdminConfigurations/update-issuer-admin-previleges/update-issuer-admin-previleges.component';
import { ConfigureIssuerAdminPrevilegesComponent }  from './components/globalAdmin/issuerAdminConfigurations/configure-issuer-admin-previleges/configure-issuer-admin-previleges.component';
import { ConfigureIssuerAdminPolicyComponent }      from './components/globalAdmin/issuerAdminConfigurations/configure-issuer-admin-policy/configure-issuer-admin-policy.component';
import { ConfigureIssuerParametersComponent }       from './components/globalAdmin/issuerConfigurations/configure-issuer-parameters/configure-issuer-parameters.component';

import { MasterAdminLoginComponent }                from './components/masterAdmin/master-admin-login/master-admin-login.component';
import { ConfigureGlobalAdminPolicyComponent }      from './components/masterAdmin/configure-global-admin-policy/configure-global-admin-policy.component';
import { CreateGlobalAdmin1Component }              from './components/masterAdmin/create-global-admin1/create-global-admin1.component';

import { AuthServiceService }                       from './services/auth_service/auth-service.service';
import { UpdateProfileService }                     from './services/update_profile_service/update-profile.service';
import { ErrorMessagesService }                     from './services/service_for_errorMessages/error-messages.service';

import { ModifyGlobalAdminService }                 from './services/global_admin_service/modify_global_admin/modify-global-admin.service';
import { ResetGlobalAdminPasswordService }          from './services/global_admin_service/reset_global_admin_password/reset-global-admin-password.service';
import { UpdateGlobalAdminPrivilegesService }       from './services/global_admin_service/update_global_admin_privileges/update-global-admin-privileges.service';
import { GlobalAdminService }                       from './services/global_admin_service/create_global_admin/global-admin.service';

import { ConfigureIssuerAdminPolicyService }        from './services/issuer_admin_configuration_service/configure_issuer_admin_policy/configure-issuer-admin-policy.service';
import { ConfigureIssuerAdminPrivilegesService }    from './services/issuer_admin_configuration_service/configure_issuer_admin_privileges/configure-issuer-admin-privileges.service';
import { CreateIssuerAdminService }                 from './services/issuer_admin_configuration_service/create_issuer_admin/create-issuer-admin.service';
import { ModifyIssuerAdminService }                 from './services/issuer_admin_configuration_service/modify_issuer_admin/modify-issuer-admin.service';
import { ResetIssuerAdminPasswordService }          from './services/issuer_admin_configuration_service/reset_issuer_admin_password/reset-issuer-admin-password.service';
import { UpdateIssuerAdminPrivilegesService }       from './services/issuer_admin_configuration_service/update_issuer_admin_privileges/update-issuer-admin-privileges.service';

import { CreateIssuerService }                      from './services/issuer_configuration_service/create_issuer/create-issuer.service';
import { AddIssuerCustomizationService }            from './services/issuer_configuration_service/add_issuer_customization_service/add-issuer-customization.service';
import { ConfigureIssuerParametersService }         from './services/issuer_configuration_service/configure_issuer_parameters_services/configure-issuer-parameters.service';
import { EnableCardRangesService }                  from './services/issuer_configuration_service/enable_card_ranges_service/enable-card-ranges.service';
import { UpdateFiInformationService }               from './services/issuer_configuration_service/update_FI_information_service/update-fi-information.service';
import { UpdateIssuerService }                      from './services/issuer_configuration_service/update_issuer_service/update-issuer.service';
import { UploadSigningCertificateService }          from './services/issuer_configuration_service/upload_signing_certificate_service/upload-signing-certificate.service';
import { FiInformationService } from './services/issuer_configuration_service/FI_information_services/fi-information.service';
import { IssuerLoginComponent } from './components/issuerAdmin/issuer-login/issuer-login.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginpageComponent,
    SidenavComponent,
    HeaderComponent,
    IndexComponent,
    CreateIssuerComponent,
    CreateGlobalAdminComponent,
    ModifyGlobalAdminComponent,
    ResetGlobalAdminComponent,
    UpdateGlobalAdminComponent,
    AddFIInformationComponent,
    UpdateFiInformationComponent,
    AddIssuerCustomizationComponent,
    MasterAdminLoginComponent,
    ConfigureGlobalAdminPolicyComponent,
    CreateGlobalAdmin1Component,
    UpdateIssuerComponent,
    UploadSigningCertificateComponent,
    EnableCardRangesComponent,
    ChangePasswordComponent,
    HelpComponent,
    UpdateProfileComponent,
    CreateIssuerAdminComponent,
    ModifyIssuerAdminAccountComponent,
    ResetIssuerAdminPasswordComponent,
    UpdateIssuerAdminPrevilegesComponent,
    ConfigureIssuerAdminPrevilegesComponent,
    ConfigureIssuerAdminPolicyComponent,
    ConfigureIssuerParametersComponent,
    FormInfoComponent,
    IssuerLoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    AuthServiceService,
    ErrorMessagesService,
    ModifyGlobalAdminService,
    ResetGlobalAdminPasswordService,
    UpdateGlobalAdminPrivilegesService,
    GlobalAdminService,
    ConfigureIssuerAdminPolicyService,
    ConfigureIssuerAdminPrivilegesService,
    CreateIssuerAdminService,
    ModifyIssuerAdminService,
    ResetIssuerAdminPasswordService,
    UpdateIssuerAdminPrivilegesService,
    CreateIssuerService,
    FiInformationService,
    UpdateProfileService,
    AddIssuerCustomizationService,
    ConfigureIssuerParametersService,
    EnableCardRangesService,
    UpdateFiInformationService,
    UpdateIssuerService,
    UploadSigningCertificateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
